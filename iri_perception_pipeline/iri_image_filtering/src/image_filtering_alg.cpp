#include "image_filtering_alg.h"

#include <opencv2/imgproc/imgproc.hpp>
#include <cv_bridge/cv_bridge.h>

sensor_msgs::ImagePtr ImageFilteringAlgorithm::filterImage(
    sensor_msgs::ImageConstPtr input)
{
  /* Convert image to OpenCv format. */
  cv_bridge::CvImagePtr image_ptr;
  if (sensor_msgs::image_encodings::isColor(input->encoding) or
      sensor_msgs::image_encodings::isBayer(input->encoding))
    image_ptr = cv_bridge::toCvCopy(input,"bgr8");
  else
    image_ptr = cv_bridge::toCvCopy(input,"mono8");

  image_ptr->image = averager_(image_ptr->image);

  /* Some filters may not work in-place, so create a new cv::Mat. */
  cv::Mat output;

  /* Apply filter according to config_.type */
  switch(config_.type)
  {
    case iri_image_filtering::ImageFiltering_None:
      output = image_ptr->image;
      break;
    case iri_image_filtering::ImageFiltering_Box:
      cv::blur(image_ptr->image,output,
          cv::Size(config_.box_ksize_2*2+1,config_.box_ksize_2*2+1));
      break;
    case iri_image_filtering::ImageFiltering_Gaussian:
      cv::GaussianBlur(image_ptr->image,output,
          cv::Size(0,0), /* Let the kernel size be computed from the*/
          config_.sigma,config_.sigma);
      break;
    case iri_image_filtering::ImageFiltering_Bilateral:
      cv::bilateralFilter(image_ptr->image,output,-1,config_.sigma_color,
          config_.sigma_space);
      break;
    case iri_image_filtering::ImageFiltering_Median:
      cv::medianBlur(image_ptr->image,output,config_.median_ksize_2*2+1);
      break;
    default:
      ROS_ASSERT(false and "Non-implemented filter");
  }

  image_ptr->image = output;

  return image_ptr->toImageMsg();
}

ImageFilteringAlgorithm::ImageFilteringAlgorithm(void)
{
  pthread_mutex_init(&this->access_,NULL);
}

ImageFilteringAlgorithm::~ImageFilteringAlgorithm(void)
{
  pthread_mutex_destroy(&this->access_);
}

void ImageFilteringAlgorithm::config_update(Config& config, uint32_t level)
{
  this->lock();

  if (config_.type != config.type)
    ROS_INFO("New filter type: %i", config.type);

  // save the current configuration
  this->config_=config;

  averager_.set_memory(config.memory);

  this->unlock();
}

// ImageFilteringAlgorithm Public API
