#include "image_filtering_alg_node.h"

ImageFilteringAlgNode::ImageFilteringAlgNode(void) :
  algorithm_base::IriBaseAlgorithm<ImageFilteringAlgorithm>(),
  imgout_camera_manager(ros::NodeHandle("imgout")),
  it(this->public_node_handle_)
{
  //init class attributes if necessary
  this->loop_rate_ = 1; //in [Hz]

  // [init publishers]
  this->imgout_publisher_ = this->it.advertiseCamera(
      "imgout/image_raw", 3);
  // uncomment the following lines to load the calibration file for the camera
  // Change <cal_file_param> for the correct parameter name holding the configuration filename
  //std::string imgout_cal_file;
  //public_node_handle_.param<std::string>("<cal_file_param>",imgout_cal_file,"");
  //if(this->imgout_camera_manager.validateURL(imgout_cal_file))
  //{
  //  if(!this->imgout_camera_manager.loadCameraInfo(imgout_cal_file))
  //    ROS_INFO("Invalid calibration file");
  //}
  //else
  //  ROS_INFO("Invalid calibration file");

  // [init subscribers]
  this->imgin_subscriber_ = this->it.subscribeCamera("imgin/image_raw",
      3, &ImageFilteringAlgNode::imgin_callback, this);
  pthread_mutex_init(&this->imgin_mutex_,NULL);

  
  // [init services]
  
  // [init clients]
  
  // [init action servers]
  
  // [init action clients]
}

ImageFilteringAlgNode::~ImageFilteringAlgNode(void)
{
  // [free dynamic memory]
  pthread_mutex_destroy(&this->imgin_mutex_);
}

void ImageFilteringAlgNode::mainNodeThread(void)
{
  ROS_INFO("%i publishers to %s",imgin_subscriber_.getNumPublishers(),
      imgin_subscriber_.getTopic().c_str());
  ROS_INFO("%i subscribers to %s",imgout_publisher_.getNumSubscribers(),
      imgout_publisher_.getTopic().c_str());
  ROS_INFO("%i filtering operations so far.\n---",count_smoothed_images_);

  // [fill msg structures]
  // Initialize the topic message structure
  //this->imgout_Image_msg_.data = my_var;

  // Uncomment the following lines two initialize the camera info structure
  //sensor_msgs::CameraInfo imgout_camera_info=this->imgout_camera_manager.getCameraInfo();
  //imgout_camera_info.header.stamp = <time_stamp>;
  //imgout_camera_info.header.frame_id = <frame_id>;

  
  // [fill srv structure and make request to the server]
  
  // [fill action structure and make request to the action server]

  // [publish messages]
  // Uncomment the following line to convert an OpenCV image to a ROS image message
  //this->imgout_Image_msg_=*this->cv_image_->toImageMsg();
  // Uncomment the following line to publish the image together with the camera information
  //this->imgout_publisher_.publish(this->imgout_Image_msg_,imgout_camera_info);

}

/*  [subscriber callbacks] */
void ImageFilteringAlgNode::imgin_callback(
    const sensor_msgs::Image::ConstPtr& msg,
    const sensor_msgs::CameraInfoConstPtr& info)
{
  ROS_DEBUG("ImageFilteringAlgNode::imgin_callback: New Message Received");

  /* No need to do anything if there are not subscribers */
  if (imgout_publisher_.getNumSubscribers() > 0)
  {
    //use appropiate mutex to shared variables if necessary
    this->alg_.lock();
    
    ROS_DEBUG("Filtering and publishing smoothed image...");
    imgout_publisher_.publish(alg_.filterImage(msg),info);
    ++count_smoothed_images_;

    //unlock previously blocked shared variables
    this->alg_.unlock();
  }
  else
  {
    ROS_DEBUG("There are not subscribers. Not doing anything.");
  }
}

void ImageFilteringAlgNode::imgin_mutex_enter(void)
{
  pthread_mutex_lock(&this->imgin_mutex_);
}

void ImageFilteringAlgNode::imgin_mutex_exit(void)
{
  pthread_mutex_unlock(&this->imgin_mutex_);
}


/*  [service callbacks] */

/*  [action callbacks] */

/*  [action requests] */

void ImageFilteringAlgNode::node_config_update(Config &config, uint32_t level)
{
  this->alg_.lock();
  this->config_=config;
  this->alg_.unlock();
}

void ImageFilteringAlgNode::addNodeDiagnostics(void)
{
}

/* main function */
int main(int argc,char *argv[])
{
  return algorithm_base::main<ImageFilteringAlgNode>(argc, argv,
      "image_filtering_alg_node");
}
