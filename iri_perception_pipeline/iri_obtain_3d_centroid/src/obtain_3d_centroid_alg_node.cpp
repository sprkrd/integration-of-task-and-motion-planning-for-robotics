#include "obtain_3d_centroid_alg_node.h"

Obtain3dCentroidAlgNode::Obtain3dCentroidAlgNode(void) :
  algorithm_base::IriBaseAlgorithm<Obtain3dCentroidAlgorithm>()
{
  //init class attributes if necessary
  this->loop_rate_ = 1;//in [Hz]

  // [init publishers]
  
  // [init subscribers]
  this->points_subscriber_ = this->public_node_handle_.subscribe("points", 1,
      &Obtain3dCentroidAlgNode::points_callback, this);
  pthread_mutex_init(&this->points_mutex_,NULL);

  
  // [init services]
  this->obtain3DPoint_server_ = this->public_node_handle_.advertiseService(
      "Obtain3DPoint", &Obtain3dCentroidAlgNode::obtain3DPointCallback, this);
  pthread_mutex_init(&this->obtain3DPoint_mutex_,NULL);

  this->obtain3DCentroid_server_ = this->public_node_handle_.advertiseService(
      "Obtain3DCentroid", &Obtain3dCentroidAlgNode::obtain3DCentroidCallback,
      this);
  pthread_mutex_init(&this->obtain3DCentroid_mutex_,NULL);

  
  // [init clients]
  
  // [init action servers]
  
  // [init action clients]
}

Obtain3dCentroidAlgNode::~Obtain3dCentroidAlgNode(void)
{
  // [free dynamic memory]
  pthread_mutex_destroy(&this->obtain3DPoint_mutex_);
  pthread_mutex_destroy(&this->points_mutex_);
  pthread_mutex_destroy(&this->obtain3DCentroid_mutex_);
}

void Obtain3dCentroidAlgNode::mainNodeThread(void)
{
  // [fill msg structures]
  
  // [fill srv structure and make request to the server]
  
  // [fill action structure and make request to the action server]

  // [publish messages]
}

/*  [subscriber callbacks] */
void Obtain3dCentroidAlgNode::points_callback(
    const sensor_msgs::PointCloud2::ConstPtr& msg)
{
  ROS_INFO("Obtain3dCentroidAlgNode::points_callback: New Message Received");

  //use appropiate mutex to shared variables if necessary
  //this->alg_.lock();
  //this->points_mutex_enter();

  last_cloud_ptr_ = msg;

  //this->alg_.unlock();
  //this->points_mutex_exit();
}

void Obtain3dCentroidAlgNode::points_mutex_enter(void)
{
  pthread_mutex_lock(&this->points_mutex_);
}

void Obtain3dCentroidAlgNode::points_mutex_exit(void)
{
  pthread_mutex_unlock(&this->points_mutex_);
}


/*  [service callbacks] */
bool Obtain3dCentroidAlgNode::obtain3DPointCallback(
    iri_perception_pipeline_msgs::Obtain3DPoint::Request &req, 
    iri_perception_pipeline_msgs::Obtain3DPoint::Response &res)
{
  ROS_INFO("Obtain3dCentroidAlgNode::obtain3DPointCallback: New Request Received!");

  //use appropiate mutex to shared variables if necessary
  //this->alg_.lock();
  this->obtain3DPoint_mutex_enter();

  if (last_cloud_ptr_)
    alg_.get3dPoint(last_cloud_ptr_,req.point2d,res.point3d,
        req.zmax,req.patch_size);
  else ROS_ERROR("There is not any point cloud available.");

  //unlock previously blocked shared variables
  this->obtain3DPoint_mutex_exit();
  //this->alg_.unlock();

  return true;
}

void Obtain3dCentroidAlgNode::obtain3DPoint_mutex_enter(void)
{
  pthread_mutex_lock(&this->obtain3DPoint_mutex_);
}

void Obtain3dCentroidAlgNode::obtain3DPoint_mutex_exit(void)
{
  pthread_mutex_unlock(&this->obtain3DPoint_mutex_);
}

bool Obtain3dCentroidAlgNode::obtain3DCentroidCallback(
    iri_perception_pipeline_msgs::Obtain3DCentroid::Request &req,
    iri_perception_pipeline_msgs::Obtain3DCentroid::Response &res)
{
  ROS_INFO("Obtain3dCentroidAlgNode::obtain3DCentroidCallback: New Request Received!");

  //use appropiate mutex to shared variables if necessary
  //this->alg_.lock();
  this->obtain3DCentroid_mutex_enter();

  if (last_cloud_ptr_)
    alg_.get3dCentroid(last_cloud_ptr_,req.contour,res.point3d,req.zmax,
        req.patch_size);
  else ROS_ERROR("There is not any point cloud available.");

  //unlock previously blocked shared variables
  this->obtain3DCentroid_mutex_exit();
  //this->alg_.unlock();

  return true;
}

void Obtain3dCentroidAlgNode::obtain3DCentroid_mutex_enter(void)
{
  pthread_mutex_lock(&this->obtain3DCentroid_mutex_);
}

void Obtain3dCentroidAlgNode::obtain3DCentroid_mutex_exit(void)
{
  pthread_mutex_unlock(&this->obtain3DCentroid_mutex_);
}


/*  [action callbacks] */

/*  [action requests] */

void Obtain3dCentroidAlgNode::node_config_update(Config &config, uint32_t level)
{
  this->alg_.lock();
  this->config_=config;
  this->alg_.unlock();
}

void Obtain3dCentroidAlgNode::addNodeDiagnostics(void)
{
}

/* main function */
int main(int argc,char *argv[])
{
  return algorithm_base::main<Obtain3dCentroidAlgNode>(argc, argv,
      "obtain_3d_centroid_alg_node");
}
