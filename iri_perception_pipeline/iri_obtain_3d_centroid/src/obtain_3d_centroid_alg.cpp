#include "obtain_3d_centroid_alg.h"
#include <pcl_ros/point_cloud.h>
#include <pcl/common/centroid.h>
#include <ros/assert.h>
#include <ros/time.h>

namespace
{

inline void operator+=(pcl::PointXYZ& dst, const pcl::PointXYZ& point)
{
  dst.x += point.x;
  dst.y += point.y;
  dst.z += point.z;
}

template<typename N>
inline void operator/=(pcl::PointXYZ& dst, N q)
{
  dst.x /= q;
  dst.y /= q;
  dst.z /= q;
}

}

Obtain3dCentroidAlgorithm::Obtain3dCentroidAlgorithm(void)
{
  pthread_mutex_init(&this->access_,NULL);
}

Obtain3dCentroidAlgorithm::~Obtain3dCentroidAlgorithm(void)
{
  pthread_mutex_destroy(&this->access_);
}

void Obtain3dCentroidAlgorithm::get3dPoint(
    sensor_msgs::PointCloud2::ConstPtr cloud_ptr,
    const iri_perception_pipeline_msgs::Point2D& point2d,
    geometry_msgs::PointStamped& point3d, double zmax, int patch_size) const
{
  point3d.header = cloud_ptr->header;

  /* First check if the cloud if organized. */
  if (cloud_ptr->height == 1 and not config_.force_aspect_ratio)
  {
    point3d.point.x = point3d.point.y = point3d.point.z = -1;
    ROS_ERROR("Point cloud is not organized");
    return;
  }

  ROS_INFO("Computing 3d centroid of image point (%i,%i)",point2d.x,point2d.y);

  /* Convert PointCloud2 from ROS message */
  pcl::PointCloud<pcl::PointXYZ> cloud;
  pcl::fromROSMsg(*cloud_ptr,cloud);

  if (config_.force_aspect_ratio)
  {
    cloud.width = config_.width;
    cloud.height = config_.height;
  }

  /* Compute centroid */
  int n = 0;
  pcl::PointXYZ centroid(0,0,0);
  int xmin = std::max((int)0,             (int)point2d.x-patch_size);
  int xmax = std::min((int)cloud.width-1, (int)point2d.x+patch_size);
  int ymin = std::max((int)0,             (int)point2d.y-patch_size);
  int ymax = std::min((int)cloud.height-1,(int)point2d.y+patch_size);
  for (int x = xmin; x <= xmax; ++x)
  {
    for (int y = ymin; y <= ymax; ++y)
    {
      /* Only consider point if its coordinates are not nan nor inf. */
      if (pcl::isFinite(cloud(x,y)) and cloud(x,y).z < zmax)
      {
        centroid += cloud(x,y);
        ++n;
      }
    }
  }

  if (n)
  {
    centroid /= n;
    /* Copy to ROS msg. */
    point3d.point.x = centroid.x;
    point3d.point.y = centroid.y;
    point3d.point.z = centroid.z;
    ROS_INFO("3D centroid: (%f,%f,%f)",centroid.x,centroid.y,centroid.z);
  }
  else
  {
    /* There is not any point with finite coordinates. */
    point3d.point.x = point3d.point.y = point3d.point.z = -1;
    ROS_ERROR("Could not find any valid point");
  }
}

void Obtain3dCentroidAlgorithm::get3dCentroid(
    sensor_msgs::PointCloud2::ConstPtr cloud_ptr,
    Contour& contour,
    geometry_msgs::PointStamped& point3d,
    double zmax,
    int patch_size) const
{
  /* Calculate coordinates' centroid. */
  ROS_ASSERT(contour.size() > 0);
  iri_perception_pipeline_msgs::Point2D point2d;
  point2d.x = point2d.y = 0;
  for (int i = 0; i < contour.size(); ++i)
  {
    point2d.x += contour[i].x;
    point2d.y += contour[i].y;
  }
  point2d.x /= contour.size();
  point2d.y /= contour.size();
  /* Obtain 3d point. */
  get3dPoint(cloud_ptr,point2d,point3d,zmax,patch_size);
}

void Obtain3dCentroidAlgorithm::config_update(Config& config, uint32_t level)
{
  this->lock();

  // save the current configuration
  this->config_=config;
  
  this->unlock();
}

// Obtain3dCentroidAlgorithm Public API
