// Copyright (C) 2010-2011 Institut de Robotica i Informatica Industrial, CSIC-UPC.
// Author 
// All rights reserved.
//
// This file is part of iri-ros-pkg
// iri-ros-pkg is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
// 
// IMPORTANT NOTE: This code has been generated through a script from the 
// iri_ros_scripts. Please do NOT delete any comments to guarantee the correctness
// of the scripts. ROS topics can be easly add by using those scripts. Please
// refer to the IRI wiki page for more information:
// http://wikiri.upc.es/index.php/Robotics_Lab

#ifndef _obtain_3d_centroid_alg_h_
#define _obtain_3d_centroid_alg_h_

#include <iri_obtain_3d_centroid/Obtain3dCentroidConfig.h>
#include <iri_perception_pipeline_msgs/Point2D.h>
#include <geometry_msgs/PointStamped.h>
#include <sensor_msgs/PointCloud2.h>

//include obtain_3d_centroid_alg main library

/**
 * \brief IRI ROS Specific Driver Class
 *
 *
 */
class Obtain3dCentroidAlgorithm
{
  protected:
   /**
    * \brief define config type
    *
    * Define a Config type with the Obtain3dCentroidConfig. All driver implementations
    * will then use the same variable type Config.
    */
    pthread_mutex_t access_;    

    // private attributes and methods

  public:
   /**
    * \brief define config type
    *
    * Define a Config type with the Obtain3dCentroidConfig. All driver implementations
    * will then use the same variable type Config.
    */
    typedef iri_obtain_3d_centroid::Obtain3dCentroidConfig Config;

    typedef std::vector<iri_perception_pipeline_msgs::Point2D> Contour;

   /**
    * \brief config variable
    *
    * This variable has all the driver parameters defined in the cfg config file.
    * Is updated everytime function config_update() is called.
    */
    Config config_;

   /**
    * \brief constructor
    *
    * In this constructor parameters related to the specific driver can be
    * initalized. Those parameters can be also set in the openDriver() function.
    * Attributes from the main node driver class IriBaseDriver such as loop_rate,
    * may be also overload here.
    */
    Obtain3dCentroidAlgorithm(void);

    /**
     * \brief Obtains 3d point from a 2d image point.
     *
     * Obtains the 3D point that corresponds to certain 2D coordinates (that
     * may come from an image). It searches in a cloud of 3D points. The
     * cloud may be non-dense, so the method uses the mean of a few neighbor
     * points in order to calculate an approximation of the desired 3D point.
     *
     * \param cloud_ptr Organized cloud message of 3D points.
     * \param point2d 2D coordinates of the desired point (in the original
     * image).
     * \param point3d Output argument. The method will store here the
     * 3D point (or an approximation).
     * \param patch_size The larger, the more the quantity of points that will
     * be considered to calculate an estimation of the desired 3D point.
     * \param zmax Maximum distance from the camera. The objective of this
     * parameter is to average only the points that are near the camera.
     */
    void get3dPoint(sensor_msgs::PointCloud2::ConstPtr cloud_ptr,
        const iri_perception_pipeline_msgs::Point2D& point2d,
        geometry_msgs::PointStamped& point3d, double zmax,
        int patch_size=0) const;

    /**
     * \brief Obtains 3d centroid of a contour of 2d image points.
     *
     * Obtains the 3D point that corresponds to certain 2D coordinates (that
     * may come from an image). It searches in a cloud of 3D points. The
     * cloud may be non-dense, so the method uses the mean of a few neighbor
     * points in order to calculate an approximation of the desired 3D point.
     *
     * \param cloud_ptr Organized cloud message of 3D points.
     * \param point2d 2D coordinates of the desired point (in the original
     * image).
     * \param point3d Output argument. The method will store here the
     * 3D point (or an approximation).
     * \param patch_size The larger, the more the quantity of points that will
     * be considered to calculate an estimation of the desired 3D point.
     * \param zmax Maximum distance from the camera. The objective of this
     * parameter is to average only the points that are near the camera.
     */
    void get3dCentroid(sensor_msgs::PointCloud2::ConstPtr cloud_ptr,
        Contour& contour, geometry_msgs::PointStamped& point3d,
        double zmax, int patch_size=0) const;

   /**
    * \brief Lock Algorithm
    *
    * Locks access to the Algorithm class
    */
    void lock(void) { pthread_mutex_lock(&this->access_); };

   /**
    * \brief Unlock Algorithm
    *
    * Unlocks access to the Algorithm class
    */
    void unlock(void) { pthread_mutex_unlock(&this->access_); };

   /**
    * \brief Tries Access to Algorithm
    *
    * Tries access to Algorithm
    * 
    * \return true if the lock was adquired, false otherwise
    */
    bool try_enter(void) 
    { 
      if(pthread_mutex_trylock(&this->access_)==0)
        return true;
      else
        return false;
    };

   /**
    * \brief config update
    *
    * In this function the driver parameters must be updated with the input
    * config variable. Then the new configuration state will be stored in the 
    * Config attribute.
    *
    * \param new_cfg the new driver configuration state
    *
    * \param level level in which the update is taken place
    */
    void config_update(Config& config, uint32_t level=0);

    // here define all obtain_3d_centroid_alg interface methods to retrieve and set
    // the driver parameters

   /**
    * \brief Destructor
    *
    * This destructor is called when the object is about to be destroyed.
    *
    */
    ~Obtain3dCentroidAlgorithm(void);
};

#endif
