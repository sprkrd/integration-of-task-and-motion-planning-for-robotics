#include "shape_matcher_alg.h"

#include <string>
#include <sstream>
#include <fstream>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <ros/package.h>
#include <ros/assert.h>


namespace
{

void copy(const std::vector<iri_perception_pipeline_msgs::Point2D>& msg_contour,
    std::vector<cv::Point>& cv_contour)
{
  cv_contour.resize(msg_contour.size());
  for (int i = 0; i < msg_contour.size(); ++i)
  {
    cv_contour[i].x = msg_contour[i].x;
    cv_contour[i].y = msg_contour[i].y;
  }
}

} /* end unnamed namespace */

int ShapeMatcherAlgorithm::indexOfMatcher(const std::string& shape) const
{
  int idx = 0;
  bool found = false;
  while (idx < available_matchers_.size() and not found)
  {
    if (available_matchers_[idx].name == shape) found = true;
    else ++idx;
  }
  return found? idx : -1;
}

ShapeMatcherAlgorithm::ShapeMatcherAlgorithm(void)
{
  pthread_mutex_init(&this->access_,NULL);

  /* The remaining lines of the constructor method are dedicated to load
   * and parse the templates.cfg file, from the cfg subfolder. */

  /* Load templates.cfg from cfg subfolder. */
  std::string pkg_path = ros::package::getPath("iri_shape_matcher");
  std::string cnf_path = pkg_path + "/cfg/templates.cfg";
  ROS_INFO_STREAM("Reading " << pkg_path);

  /* Remove comments. */
  std::ifstream fin(cnf_path.c_str(),std::fstream::in);
  std::ostringstream content;
  std::string line;
  while (std::getline(fin,line))
    if (not line.empty() and line[0] != '#') content << line << '\n';

  std::istringstream str_in(content.str());

  int label;
  while (str_in >> label)
  {
    /* Read number of pieces in category. */
    int n;
    str_in >> n;
    for (int i = 0; i < n; ++i)
    {
      /* Read name, filename, angle limit and simplification error factor. */
      std::string name, template_path;
      double angle_limit, simplify_error_factor;
      str_in >> name >> template_path >> angle_limit >> simplify_error_factor;
      template_path = pkg_path + "/templates/" + template_path;
      ROS_INFO_STREAM("Piece: " << name << ", path: " << template_path
          << "label: " << label << ", angle_limit: " << angle_limit
          << ", simplification error factor: " << simplify_error_factor);

      /* Extract contour from image */
      std::vector<std::vector<cv::Point> > contours;
      cv::Mat temp = cv::imread(template_path,CV_LOAD_IMAGE_GRAYSCALE);
      cv::findContours(temp,contours,cv::noArray(),CV_RETR_EXTERNAL,
          CV_CHAIN_APPROX_SIMPLE);
      ROS_ASSERT(contours.size() == 1);

      /* Add matcher with metainformation to the available_matchers_ vector. */
      MatcherMeta matcher = {label,name,cv_extra::ShapeMatcher(
          contours[0],angle_limit,25,simplify_error_factor)};
      available_matchers_.push_back(matcher);
    }
  }

  ROS_INFO_STREAM(available_matchers_.size() << " matchers loaded.");
}

void ShapeMatcherAlgorithm::compare(
    const iri_perception_pipeline_msgs::Blob& blob,
    const std::vector<std::string>& available_shapes, Matches& matches,
    bool ignore_labels) const
{
  /* Convert to OpenCV contour. */
  std::vector<cv::Point> contour;
  copy(blob.contour,contour);
  /* Fill matches vector. */
  matches.resize(available_shapes.size());
  for (int i = 0; i < available_shapes.size(); ++i)
  {
    int idx_matcher = indexOfMatcher(available_shapes[i]);
    ROS_ASSERT(idx_matcher >= 0);
    const MatcherMeta& relevant = available_matchers_[idx_matcher];
    /* Fill ROS message. */
    matches[i].shape = available_shapes[i];
    if (blob.label == relevant.label or ignore_labels)
    {
      /* Comparison is possible. Store result in ROS message. */
      cv_extra::ShapeMatcher::Match match = relevant.matcher(contour);
      matches[i].optimum_angle = match.optimum_angle;
      matches[i].similitude = match.similitude;
    }
    else matches[i].similitude = 0;
  }
}

ShapeMatcherAlgorithm::~ShapeMatcherAlgorithm(void)
{
  pthread_mutex_destroy(&this->access_);
}

void ShapeMatcherAlgorithm::config_update(Config& config, uint32_t level)
{
  this->lock();

  // save the current configuration
  this->config_=config;
  
  this->unlock();
}

// ShapeMatcherAlgorithm Public API
