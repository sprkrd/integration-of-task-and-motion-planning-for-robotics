#include "shape_matcher_alg_node.h"

#include <cv_bridge/cv_bridge.h>

ShapeMatcherAlgNode::ShapeMatcherAlgNode(void) :
  algorithm_base::IriBaseAlgorithm<ShapeMatcherAlgorithm>()
{
  //init class attributes if necessary
  //this->loop_rate_ = 2;//in [Hz]

  // [init publishers]
  
  // [init subscribers]
  
  // [init services]
  this->compare_server_ = this->public_node_handle_.advertiseService("Compare",
      &ShapeMatcherAlgNode::compareCallback, this);
  pthread_mutex_init(&this->compare_mutex_,NULL);

  
  // [init clients]
  
  // [init action servers]
  
  // [init action clients]
}

ShapeMatcherAlgNode::~ShapeMatcherAlgNode(void)
{
  // [free dynamic memory]
  pthread_mutex_destroy(&this->compare_mutex_);
}

void ShapeMatcherAlgNode::mainNodeThread(void)
{
  // [fill msg structures]
  
  // [fill srv structure and make request to the server]
  
  // [fill action structure and make request to the action server]

  // [publish messages]
}

/*  [subscriber callbacks] */

/*  [service callbacks] */
bool ShapeMatcherAlgNode::compareCallback(
    iri_perception_pipeline_msgs::Compare::Request &req,
    iri_perception_pipeline_msgs::Compare::Response &res)
{
  ROS_INFO("ShapeMatcherAlgNode::compareCallback: New Request Received!");

  //use appropiate mutex to shared variables if necessary
  this->alg_.lock();
  //this->compare_mutex_enter();
  
  alg_.compare(req.blob,req.available_shapes,res.matches,req.ignore_label);

  //unlock previously blocked shared variables
  //this->compare_mutex_exit();
  this->alg_.unlock();

  return true;
}

void ShapeMatcherAlgNode::compare_mutex_enter(void)
{
  pthread_mutex_lock(&this->compare_mutex_);
}

void ShapeMatcherAlgNode::compare_mutex_exit(void)
{
  pthread_mutex_unlock(&this->compare_mutex_);
}


/*  [action callbacks] */

/*  [action requests] */

void ShapeMatcherAlgNode::node_config_update(Config &config, uint32_t level)
{
  this->alg_.lock();
  this->config_=config;
  this->alg_.unlock();
}

void ShapeMatcherAlgNode::addNodeDiagnostics(void)
{
}

/* main function */
int main(int argc,char *argv[])
{
  return algorithm_base::main<ShapeMatcherAlgNode>(argc, argv,
      "shape_matcher_alg_node");
}
