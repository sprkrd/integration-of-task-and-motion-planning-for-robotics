// Copyright (C) 2010-2011 Institut de Robotica i Informatica Industrial, CSIC-UPC.
// Author 
// All rights reserved.
//
// This file is part of iri-ros-pkg
// iri-ros-pkg is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
// 
// IMPORTANT NOTE: This code has been generated through a script from the 
// iri_ros_scripts. Please do NOT delete any comments to guarantee the correctness
// of the scripts. ROS topics can be easly add by using those scripts. Please
// refer to the IRI wiki page for more information:
// http://wikiri.upc.es/index.php/Robotics_Lab

#ifndef _shape_matcher_alg_h_
#define _shape_matcher_alg_h_

#include <iri_shape_matcher/ShapeMatcherConfig.h>
#include <iri_perception_pipeline_msgs/Blob.h>
#include <iri_perception_pipeline_msgs/Match.h>
#include <shapematcher.h>
#include <string>

//include shape_matcher_alg main library

/**
 * \brief IRI ROS Specific Driver Class
 *
 *
 */
class ShapeMatcherAlgorithm
{
  protected:
   /**
    * \brief define config type
    *
    * Define a Config type with the ShapeMatcherConfig. All driver implementations
    * will then use the same variable type Config.
    */
    pthread_mutex_t access_;    

  private:
    // private attributes and methods
    struct MatcherMeta
    {
      int label;
      std::string name;
      cv_extra::ShapeMatcher matcher;
    };

    std::vector<MatcherMeta> available_matchers_;

    void subset(std::vector<const MatcherMeta*>& sset,
        const std::vector<std::string>& names) const;

    /** \brief Finds matcher
     *  \param shape The method finds matcher with name==shape
     *  \return Index of the matcher. Negative value if it does not exist.
     */
    int indexOfMatcher(const std::string& shape) const;

  public:
   /**
    * \brief define config type
    *
    * Define a Config type with the ShapeMatcherConfig. All driver implementations
    * will then use the same variable type Config.
    */
    typedef iri_shape_matcher::ShapeMatcherConfig Config;

    /* \brief define Matches type.
     *
     * Shorter name for a vector of matches.
     */
    typedef std::vector<iri_perception_pipeline_msgs::Match> Matches;

   /**
    * \brief config variable
    *
    * This variable has all the driver parameters defined in the cfg config file.
    * Is updated everytime function config_update() is called.
    */
    Config config_;

   /**
    * \brief constructor
    *
    * In this constructor parameters related to the specific driver can be
    * initalized. Those parameters can be also set in the openDriver() function.
    * Attributes from the main node driver class IriBaseDriver such as loop_rate,
    * may be also overload here.
    */
    ShapeMatcherAlgorithm(void);

    /*
     * \brief Comparison method
     *
     * Compares a certain blob with a set of shapes.
     *
     * \param blob Input blob message.
     * \param available_shapes Vector of with the names of the relevant shapes.
     * \param matches Output argument. The Match messages shall be stored
     * here.
     * \param ignore_labels If true, the method will compare a blob and
     * a shape even if they do not share label.
     */
    void compare(const iri_perception_pipeline_msgs::Blob& blob,
        const std::vector<std::string>& available_shapes, Matches& matches,
        bool ignore_labels=false) const;

   /**
    * \brief Lock Algorithm
    *
    * Locks access to the Algorithm class
    */
    void lock(void) { pthread_mutex_lock(&this->access_); };

   /**
    * \brief Unlock Algorithm
    *
    * Unlocks access to the Algorithm class
    */
    void unlock(void) { pthread_mutex_unlock(&this->access_); };

   /**
    * \brief Tries Access to Algorithm
    *
    * Tries access to Algorithm
    * 
    * \return true if the lock was adquired, false otherwise
    */
    bool try_enter(void) 
    { 
      if(pthread_mutex_trylock(&this->access_)==0)
        return true;
      else
        return false;
    };

   /**
    * \brief config update
    *
    * In this function the driver parameters must be updated with the input
    * config variable. Then the new configuration state will be stored in the 
    * Config attribute.
    *
    * \param new_cfg the new driver configuration state
    *
    * \param level level in which the update is taken place
    */
    void config_update(Config& config, uint32_t level=0);

    // here define all shape_matcher_alg interface methods to retrieve and set
    // the driver parameters

   /**
    * \brief Destructor
    *
    * This destructor is called when the object is about to be destroyed.
    *
    */
    ~ShapeMatcherAlgorithm(void);
};

#endif
