// Copyright (C) 2010-2011 Institut de Robotica i Informatica Industrial, CSIC-UPC.
// Author 
// All rights reserved.
//
// This file is part of iri-ros-pkg
// iri-ros-pkg is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
// 
// IMPORTANT NOTE: This code has been generated through a script from the 
// iri_ros_scripts. Please do NOT delete any comments to guarantee the correctness
// of the scripts. ROS topics can be easly add by using those scripts. Please
// refer to the IRI wiki page for more information:
// http://wikiri.upc.es/index.php/Robotics_Lab

#ifndef _piece_detector_alg_node_h_
#define _piece_detector_alg_node_h_

#include <iri_base_algorithm/iri_base_algorithm.h>
#include "piece_detector_alg.h"

// [publisher subscriber headers]

// [service client headers]
#include <iri_perception_pipeline_msgs/Obtain3DCentroid.h>
#include <iri_perception_pipeline_msgs/Compare.h>
#include <iri_perception_pipeline_msgs/Segmentate.h>

// [action server client headers]
#include <iri_perception_pipeline_msgs/DetectPiecesAction.h>
#include <iri_perception_pipeline_msgs/DetectCavityAction.h>
#include <iri_action_server/iri_action_server.h>
#include <iri_perception_pipeline_msgs/DetectPiecesOnTableAction.h>

/**
 * \brief IRI ROS Specific Algorithm Class
 *
 */
class PieceDetectorAlgNode : public algorithm_base::IriBaseAlgorithm<PieceDetectorAlgorithm>
{
  private:
    // [publisher attributes]

    // [subscriber attributes]

    // [service attributes]

    // [client attributes]
    ros::ServiceClient obtain3DCentroid_client_;
    iri_perception_pipeline_msgs::Obtain3DCentroid obtain3DCentroid_srv_;

    ros::ServiceClient compare_client_;
    iri_perception_pipeline_msgs::Compare compare_srv_;

    /* 0: for pieces on table, 1: for cavities, 2: for grabbed pieces */
    ros::ServiceClient segmentate_client_[3];
    iri_perception_pipeline_msgs::Segmentate segmentate_srv_;


    // [action server attributes]
    IriActionServer<iri_perception_pipeline_msgs::DetectPiecesAction>
      detectPieces_aserver_;
    void detectPiecesStartCallback(
        const iri_perception_pipeline_msgs::DetectPiecesGoalConstPtr& goal);
    void detectPiecesStopCallback(void);
    bool detectPiecesIsFinishedCallback(void);
    bool detectPiecesHasSucceededCallback(void);
    void detectPiecesGetResultCallback(
        iri_perception_pipeline_msgs::DetectPiecesResultPtr& result);
    void detectPiecesGetFeedbackCallback(
        iri_perception_pipeline_msgs::DetectPiecesFeedbackPtr& feedback);

    static const int PIECES_ON_TABLE = 0;
    static const int CAVITY = 1;
    static const int PIECE_IN_HAND = 2;
    int detectPieces_detection_type_;

    /* Control variables */
    static const int READY = 0;
    static const int SEGMENTATION = 1;
    static const int OBTAINING_CENTROIDS = 2;
    static const int COMPARING = 3;
    static const int END = 4;
    int detectPieces_step_;
    bool detectPieces_finished_;
    bool detectPieces_ok_;

    std::vector<std::string> detectPieces_available_shapes_;
    std::vector<iri_perception_pipeline_msgs::Piece>
      detectPieces_detected_pieces_;

    bool segmentate(int type);
    bool obtain3DCentroid(int patch_size=0, double zmax=1000);
    bool compare(bool ignore_labels=false, bool mirror=false);

    /**
     * \brief Takes care of executing the steps of DetectPiecesOnTable
     */
    void handleDetectPiecesOnTable();

    /**
     * \brief Takes care of executing the steps of DetectCavity
     */
    void handleDetectCavity();

    /**
     * \brief Take care of executing the steps of DetectPieceInHand
     */

    void handleDetectPieceInHand();

    // [action client attributes]

   /**
    * \brief config variable
    *
    * This variable has all the driver parameters defined in the cfg config file.
    * Is updated everytime function config_update() is called.
    */
    Config config_;
  public:
   /**
    * \brief Constructor
    * 
    * This constructor initializes specific class attributes and all ROS
    * communications variables to enable message exchange.
    */
    PieceDetectorAlgNode(void);

   /**
    * \brief Destructor
    * 
    * This destructor frees all necessary dynamic memory allocated within this
    * this class.
    */
    ~PieceDetectorAlgNode(void);

  protected:
   /**
    * \brief main node thread
    *
    * This is the main thread node function. Code written here will be executed
    * in every node loop while the algorithm is on running state. Loop frequency 
    * can be tuned by modifying loop_rate attribute.
    *
    * Here data related to the process loop or to ROS topics (mainly data structs
    * related to the MSG and SRV files) must be updated. ROS publisher objects 
    * must publish their data in this process. ROS client servers may also
    * request data to the corresponding server topics.
    */
    void mainNodeThread(void);

   /**
    * \brief dynamic reconfigure server callback
    * 
    * This method is called whenever a new configuration is received through
    * the dynamic reconfigure. The derivated generic algorithm class must 
    * implement it.
    *
    * \param config an object with new configuration from all algorithm 
    *               parameters defined in the config file.
    * \param level  integer referring the level in which the configuration
    *               has been changed.
    */
    void node_config_update(Config &config, uint32_t level);

   /**
    * \brief node add diagnostics
    *
    * In this abstract function additional ROS diagnostics applied to the 
    * specific algorithms may be added.
    */
    void addNodeDiagnostics(void);

    // [diagnostic functions]
    
    // [test functions]
};

#endif
