#include "piece_detector_alg.h"

PieceDetectorAlgorithm::PieceDetectorAlgorithm(void)
{
  pthread_mutex_init(&this->access_,NULL);
}

PieceDetectorAlgorithm::~PieceDetectorAlgorithm(void)
{
  pthread_mutex_destroy(&this->access_);
}

void PieceDetectorAlgorithm::config_update(Config& config, uint32_t level)
{
  this->lock();

  // save the current configuration
  this->config_=config;
  
  this->unlock();
}

// PieceDetectorAlgorithm Public API
