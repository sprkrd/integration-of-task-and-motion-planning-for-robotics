#include "piece_detector_alg_node.h"

#include <ros/assert.h>

PieceDetectorAlgNode::PieceDetectorAlgNode(void) :
  algorithm_base::IriBaseAlgorithm<PieceDetectorAlgorithm>(),
  detectPieces_aserver_(public_node_handle_, "DetectPieces")
{
  //init class attributes if necessary
  this->loop_rate_ = 50; //in [Hz]

  // [init publishers]
  
  // [init subscribers]
  
  // [init services]
  
  // [init clients]
  obtain3DCentroid_client_ = this->public_node_handle_.
    serviceClient<iri_perception_pipeline_msgs::Obtain3DCentroid>("Obtain3DCentroid");

  compare_client_ = this->public_node_handle_.
    serviceClient<iri_perception_pipeline_msgs::Compare>("Compare");

  segmentate_client_[PIECES_ON_TABLE] = this->public_node_handle_.
    serviceClient<iri_perception_pipeline_msgs::Segmentate>("Segmentate");

  segmentate_client_[CAVITY] = this->public_node_handle_.
    serviceClient<iri_perception_pipeline_msgs::Segmentate>("SegmentateCavity");

  segmentate_client_[PIECE_IN_HAND] = this->public_node_handle_.
    serviceClient<iri_perception_pipeline_msgs::Segmentate>("SegmentateRegion");

  
  // [init action servers]
  
  /***********************************************************************
   *************************** DetectPieces ******************************
   ***********************************************************************/

  detectPieces_aserver_.registerStartCallback(boost::bind(
        &PieceDetectorAlgNode::detectPiecesStartCallback, this, _1));
  detectPieces_aserver_.registerStopCallback(boost::bind(
        &PieceDetectorAlgNode::detectPiecesStopCallback, this));
  detectPieces_aserver_.registerIsFinishedCallback(boost::bind(
        &PieceDetectorAlgNode::detectPiecesIsFinishedCallback,this));
  detectPieces_aserver_.registerHasSucceedCallback(boost::bind(
        &PieceDetectorAlgNode::detectPiecesHasSucceededCallback,this));
  detectPieces_aserver_.registerGetResultCallback(boost::bind(
        &PieceDetectorAlgNode::detectPiecesGetResultCallback,this, _1));
  detectPieces_aserver_.registerGetFeedbackCallback(boost::bind(
        &PieceDetectorAlgNode::detectPiecesGetFeedbackCallback,this, _1));
  detectPieces_aserver_.start();

  detectPieces_step_ = READY;
  detectPieces_finished_ = false;
  detectPieces_ok_ = false;
  
  // [init action clients]
}

PieceDetectorAlgNode::~PieceDetectorAlgNode(void)
{
  // [free dynamic memory]
}

void PieceDetectorAlgNode::mainNodeThread(void)
{
  // [fill msg structures]
  
  // [fill srv structure and make request to the server]
  
  if (detectPieces_step_ == SEGMENTATION)
  {
    detectPieces_ok_ = segmentate(detectPieces_detection_type_);
    if (detectPieces_ok_) detectPieces_step_ =
      detectPieces_detection_type_ == PIECE_IN_HAND? COMPARING :
        OBTAINING_CENTROIDS;
    else detectPieces_step_ = END;
  }
  else if (detectPieces_step_ == OBTAINING_CENTROIDS)
  {
    if (detectPieces_detection_type_ == PIECES_ON_TABLE)
      detectPieces_ok_ = obtain3DCentroid();
    else if (detectPieces_detection_type_ == CAVITY)
      detectPieces_ok_ = obtain3DCentroid(
          config_.patch_size_cavity,config_.zmax_cavity);
    else ROS_BREAK();
    if (detectPieces_ok_) detectPieces_step_ = COMPARING;
    else detectPieces_step_ = END;
  }
  else if (detectPieces_step_ == COMPARING)
  {
    if (detectPieces_detection_type_ == PIECES_ON_TABLE)
      detectPieces_ok_ = compare();
    else if (detectPieces_detection_type_ == CAVITY)
      detectPieces_ok_ = compare(true);
    else if (detectPieces_detection_type_ == PIECE_IN_HAND)
      detectPieces_ok_ = compare(false,true);
    else ROS_BREAK();
    detectPieces_step_ = END;
  }
  else if (detectPieces_step_ == END)
  {
    detectPieces_step_ = READY;
    detectPieces_available_shapes_.clear();
    detectPieces_detection_type_ = -1;
    if (detectPieces_ok_) ROS_INFO("Action completed successfully");
    else ROS_ERROR("Error executing action");
    detectPieces_finished_ = true;
  }

  // [fill action structure and make request to the action server]

  // [publish messages]
}

bool PieceDetectorAlgNode::segmentate(int type)
{
  bool success = true;
  /* Call Segmentate client */
  if (segmentate_client_[type].call(segmentate_srv_))
  {
    /* Copy labels */
    const std::vector<iri_perception_pipeline_msgs::Blob>& blobs =
      segmentate_srv_.response.blobs;
    detectPieces_detected_pieces_.resize(blobs.size());
    for (int i = 0; i < blobs.size(); ++i)
      detectPieces_detected_pieces_[i].label = blobs[i].label;
    ROS_INFO("%i blobs detected", (int)segmentate_srv_.response.blobs.size());
  }
  else
  {
    ROS_ERROR("Segmentation error");
    success = false;
  }
  return success;
}

bool PieceDetectorAlgNode::obtain3DCentroid(int patch_size, double zmax)
{
  bool success = true;
  ROS_INFO("Obtaining centroids...");
  const std::vector<iri_perception_pipeline_msgs::Blob>& blobs =
    segmentate_srv_.response.blobs;
  /* Call Obtain3DCentroid client once per Blob */
  int idx = 0;
  while (idx < blobs.size() and success)
  {
    obtain3DCentroid_srv_.request.contour = blobs[idx].contour;
    obtain3DCentroid_srv_.request.patch_size = patch_size;
    obtain3DCentroid_srv_.request.zmax = zmax;
    if (obtain3DCentroid_client_.call(obtain3DCentroid_srv_))
    {
      detectPieces_detected_pieces_[idx].centroid =
        obtain3DCentroid_srv_.response.point3d;
      ++idx;
    }
    else success = false;
  }
  if (success) ROS_INFO("%i centroids obtained successfully",idx);
  else ROS_ERROR("The centroid %i could not be retrieved.",idx);
  return success;
}

bool PieceDetectorAlgNode::compare(bool ignore_labels, bool mirror)
{ 
  bool success = true;
  ROS_INFO("Comparing contours...");
  const std::vector<iri_perception_pipeline_msgs::Blob>& blobs =
    segmentate_srv_.response.blobs;
  /* Call Compare client once per blob. */
  int idx = 0;
  while (idx < blobs.size() and success)
  {
    compare_srv_.request.blob = blobs[idx];
    if (mirror)
    {
      /* Reverse contour vertically. Before doing the comparison. This is
       * useful when the robot is */
      for (int i = 0; i < compare_srv_.request.blob.contour.size(); ++i)
        compare_srv_.request.blob.contour[i].y *= -1;
    }
    compare_srv_.request.ignore_label = ignore_labels;
    compare_srv_.request.available_shapes = detectPieces_available_shapes_;
    if (compare_client_.call(compare_srv_))
    {
      detectPieces_detected_pieces_[idx].matches =
        compare_srv_.response.matches;
      ++idx;
    }
    else success = false;
  }
  if (success) ROS_INFO("Success in the comparison");
  else ROS_ERROR("Failure in the comparison.");
  return success;
}

/*  [subscriber callbacks] */

/*  [service callbacks] */

/*  [action callbacks] */


/***********************************************************************
 *************************** DetectPieces ******************************
 ***********************************************************************/

void PieceDetectorAlgNode::detectPiecesStartCallback(
    const iri_perception_pipeline_msgs::DetectPiecesGoalConstPtr& goal)
{
  this->alg_.lock();
  //check goal
  if (detectPieces_step_ == READY)
  {
    /* OK from the begining. Set to false if error occurs. */
    detectPieces_detection_type_ = goal->detection_type;
    detectPieces_available_shapes_ = goal->available_shapes;
    detectPieces_ok_ = true;
    detectPieces_step_ = SEGMENTATION;
    detectPieces_finished_ = false;
  }
  else /* Just one action at a time */
  {
    detectPieces_ok_ = false;
    detectPieces_step_ = END;
    detectPieces_finished_ = true;
  }
  //execute goal
  this->alg_.unlock();
}

void PieceDetectorAlgNode::detectPiecesStopCallback(void)
{
  this->alg_.lock();
  //stop action
  detectPieces_step_ = READY;
  this->alg_.unlock();
}

bool PieceDetectorAlgNode::detectPiecesIsFinishedCallback(void)
{
  bool ret = false;

  this->alg_.lock();
  //if action has finish for any reason
  ret = detectPieces_finished_;
  this->alg_.unlock();

  return ret;
}

bool PieceDetectorAlgNode::detectPiecesHasSucceededCallback(void)
{
  bool ret = false;

  this->alg_.lock();
  //if goal was accomplished
  ret = detectPieces_ok_;
  this->alg_.unlock();

  return ret;
}

void PieceDetectorAlgNode::detectPiecesGetResultCallback(
    iri_perception_pipeline_msgs::DetectPiecesResultPtr& result)
{
  this->alg_.lock();
  //update result data to be sent to client
  result->status = detectPieces_step_;
  result->detected_pieces = detectPieces_detected_pieces_;
  detectPieces_detected_pieces_.clear();
  this->alg_.unlock();
}

void PieceDetectorAlgNode::detectPiecesGetFeedbackCallback(
    iri_perception_pipeline_msgs::DetectPiecesFeedbackPtr& feedback)
{
  this->alg_.lock();
  //update feedback data to be sent to client
  feedback->status = detectPieces_step_;
  this->alg_.unlock();
}

/*  [action requests] */

void PieceDetectorAlgNode::node_config_update(Config &config, uint32_t level)
{
  this->alg_.lock();
  this->config_=config;
  this->alg_.unlock();
}

void PieceDetectorAlgNode::addNodeDiagnostics(void)
{
}

/* main function */
int main(int argc,char *argv[])
{
  return algorithm_base::main<PieceDetectorAlgNode>(argc, argv, "piece_detector_alg_node");
}
