#! /usr/bin/env python
#*  All rights reserved.
#*
#*  Redistribution and use in source and binary forms, with or without
#*  modification, are permitted provided that the following conditions
#*  are met:
#*
#*   * Redistributions of source code must retain the above copyright
#*     notice, this list of conditions and the following disclaimer.
#*   * Redistributions in binary form must reproduce the above
#*     copyright notice, this list of conditions and the following
#*     disclaimer in the documentation and/or other materials provided
#*     with the distribution.
#*   * Neither the name of the Willow Garage nor the names of its
#*     contributors may be used to endorse or promote products derived
#*     from this software without specific prior written permission.
#*
#*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
#*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
#*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
#*  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
#*  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
#*  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
#*  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
#*  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
#*  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
#*  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
#*  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
#*  POSSIBILITY OF SUCH DAMAGE.
#***********************************************************

# Author: 

PACKAGE='iri_piece_segmenter'

from dynamic_reconfigure.parameter_generator_catkin import *

gen = ParameterGenerator()

# We have tried to keep a reasonably small number of parameters

# Primary segmenter

cropping = gen.add_group("cropping")
cropping.add("crop_bottom",int_t,0,"Crop from bottom border",0,0,225)
cropping.add("crop_top",int_t,0,"Crop from top border",0,0,225)
cropping.add("crop_left",int_t,0,"Crop from left border",0,0,300)
cropping.add("crop_right",int_t,0,"Crop from right border",0,0,300)

tolerances = gen.add_group("tolerance")
tolerances.add("h_tolerance",int_t,0,"Hue tolerance",0,0,180)
tolerances.add("s_tolerance",int_t,0,"Saturation tolerance",0,0,255)
tolerances.add("v_tolerance",int_t,0,"Value tolerance",0,0,255)

segmenters = ["red","blue","green","yellow"]
for seg in segmenters:
    group = gen.add_group(seg+"_central_color")
    group.add(seg+"_h",int_t,0,"Central %s hue"%seg,0,0,180)
    group.add(seg+"_s",int_t,0,"Central %s saturation"%seg,0,0,255)
    group.add(seg+"_v",int_t,0,"Central %s value"%seg,0,0,255)

# Secondary segmenter

region_watershed_segmenter = gen.add_group("region_watershed_segmenter")
region_watershed_segmenter.add("center_x",int_t,0,"Region central x",320,0,640)
region_watershed_segmenter.add("center_y",int_t,0,"Region central y",240,0,480)
region_watershed_segmenter.add("exclusive_radius",int_t,0,"Pixels farther than this radio will be initially marked as background",20,11,200)
region_watershed_segmenter.add("inclusive_radius",int_t,0,"Pixels inside this radio will be initially marked as foreground",4,1,10)

# Terciary segmenter

cavity_watershed_segmenter = gen.add_group("cavity_watershed_segmenter")
cavity_watershed_segmenter.add("cavity_x",int_t,0,"cavity central x",320,0,640)
cavity_watershed_segmenter.add("cavity_y",int_t,0,"cavity central y",240,0,480)
cavity_watershed_segmenter.add("cavity_ex_radius",int_t,0,"Pixels farther than this radio will be initially marked as background",20,11,200)
cavity_watershed_segmenter.add("cavity_in_radius",int_t,0,"Pixels inside this radio will be initially marked as foreground",4,1,10)

# Common

gen.add("min_blob_area",double_t,0,"Minimum area of detected blob",20,0,100)

exit(gen.generate(PACKAGE, "PieceSegmenterAlgorithm", "PieceSegmenter"))
