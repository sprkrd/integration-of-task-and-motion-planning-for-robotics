// Copyright (C) 2010-2011 Institut de Robotica i Informatica Industrial, CSIC-UPC.
// Author 
// All rights reserved.
//
// This file is part of iri-ros-pkg
// iri-ros-pkg is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
// 
// IMPORTANT NOTE: This code has been generated through a script from the 
// iri_ros_scripts. Please do NOT delete any comments to guarantee the correctness
// of the scripts. ROS topics can be easly add by using those scripts. Please
// refer to the IRI wiki page for more information:
// http://wikiri.upc.es/index.php/Robotics_Lab

#ifndef _piece_segmenter_alg_h_
#define _piece_segmenter_alg_h_

#include <iri_piece_segmenter/PieceSegmenterConfig.h>
#include <iri_perception_pipeline_msgs/Blob.h>
#include <sensor_msgs/Image.h>
#include <cv_extra.h>

//include piece_segmenter_alg main library

/**
 * \brief IRI ROS Specific Driver Class
 *
 *
 */
class PieceSegmenterAlgorithm
{
  protected:
   /**
    * \brief define config type
    *
    * Define a Config type with the PieceSegmenterConfig. All driver implementations
    * will then use the same variable type Config.
    */
    pthread_mutex_t access_;    

  private:
    // private attributes and methods

    /* Primary segmenter. Aimed at detecting the pieces that are on the table.*/
    cv_extra::HSVSegmenter segmenter_;

    /* Secondary segmenter. Aimed at detecting pieces that are already grabbed
     * by the WAM*/
    cv_extra::RegionSegmenter region_segmenter_;

    /* Terciary segmenter. Aimed at detecting the cavities of the sphere. */
    cv_extra::RegionSegmenter cavity_segmenter_;

    /* One color per label */
    cv::Vec3b color_tab_[6];

    /**
     * \brief Compute ROI for *Primary segmenter*.
     */
    cv::Rect get_roi_rect(const cv::Size& size) const;

  public:

    const static int EDGE_LABEL = 0;
    const static int BG_LABEL = 1;
    const static int RED_LABEL = 2;
    const static int YELLOW_LABEL = 3;
    const static int GREEN_LABEL = 4;
    const static int BLUE_LABEL = 5;

   /**
    * \brief define config type
    *
    * Define a Config type with the PieceSegmenterConfig. All driver implementations
    * will then use the same variable type Config.
    */
    typedef iri_piece_segmenter::PieceSegmenterConfig Config;

    typedef std::vector<iri_perception_pipeline_msgs::Blob> Blobs;

   /**
    * \brief config variable
    *
    * This variable has all the driver parameters defined in the cfg config file.
    * Is updated everytime function config_update() is called.
    */
    Config config_;

   /**
    * \brief constructor
    *
    * In this constructor parameters related to the specific driver can be
    * initalized. Those parameters can be also set in the openDriver() function.
    * Attributes from the main node driver class IriBaseDriver such as loop_rate,
    * may be also overload here.
    */
    PieceSegmenterAlgorithm(void);

    /**
     *  \brief Segmentates the given input.
     *
     *  Labels the areas associated to certains colors. If the algorithm
     *  configuration is correct. these labels will correspond to the
     *  areas where the pieces are located. The method gives the labels
     *  of the detected blobs and their contours.
     *  The labels follow this legend:
     *    + 2: red pieces
     *    + 3: yellow pieces
     *    + 4: green pieces
     *    + 5: blue pieces
     *
     *  \param image Input image.
     *  \param labels The labels of the detected areas shall be stored here.
     *  \param contours The contours of the detected areas shall be stored
     *  here
     */
    void segmentate(const cv::Mat& image, std::vector<int>& labels,
        std::vector<std::vector<cv::Point> >& contours) const;

    /**
     *  \brief Segments the given input
     *
     *  Idem to segmentate(input,labels,contours), but with an additional
     *  feedback parameter.
     *
     *  \param image Input image.
     *  \param labels The labels of the detected areas shall be stored here.
     *  \param contours The contours of the detected areas shall be stored
     *  here
     *  \param feedback Output argument. This image is useful for fine tuning
     *  the central colors of the segmenter and the tolerances. This
     *  image shows each contour in the position where they were detected in
     *  the image. The contours are colorized according to their label.
     *  The edges (cropped area) are shown in gray and the background is shown
     *  in black.
     */
    void segmentate(const cv::Mat& image, std::vector<int>& labels,
        std::vector<std::vector<cv::Point> >& contours,
        cv::Mat& feedback) const;

    /**
     *  \brief Segmentates the given input
     *
     *  Convenience method. Idem to segmentate(input,labels,contours).
     *  However, it already stores the results in a vector of Blob messages.
     *
     *  \param image_ptr Input image ROS message constant pointer.
     *  \param blobs Vector of Blob messages. A blob has the following
     *  structure:
     *    int32 label
     *    Point2D[] contour
     *      (Each Point2D)
     *      int32 x
     *      int32 y
     */
    void segmentate(sensor_msgs::ImageConstPtr image_ptr, Blobs& blobs) const;

    /**
     *  \brief Segmentates the given input
     *
     *  Convenience method. Idem to segmentate(input,blobs).
     *  However, it additionally provides a feedback image message.
     *
     *  \param image_ptr Input image ROS message constant pointer.
     *  \param blobs Vector of Blob messages.
     *  \param feedback Output argument. Feedback image message.
     */
    void segmentate(sensor_msgs::ImageConstPtr image_ptr, Blobs& blobs,
        sensor_msgs::Image& feedback) const;

    /**
     *  \brief Segmentates the given input around a certain region.
     *
     *  It segmentates a local region in an image using the RegionSegmenter
     *  from cv_extra. The parameters of the segmenter are extracted from the
     *  node configuration. The label is infered from the mean of the detected
     *  pixels.
     *
     *  \param image Input image.
     *  \param labels The label of the detected contour shall be stored here
     *  \param contour The contour of the detected shape shall be stored
     *  here (it should be only one contour).
     */
    void segmentateRegion(const cv::Mat& image, std::vector<int>& labels,
        std::vector<std::vector<cv::Point> >& contour) const;

    /**
     *  \brief Segmentates the given input
     *
     *  Idem to segmentateRegion(input,contour), but with an additional
     *  feedback parameter.
     *
     *  \param image Input image.
     *  \param labels The label of the detected contour shall be stored here.
     *  \param contour The contours of the detected shape shall be stored
     *  here
     *  \param feedback Output argument. This image is useful for fine tuning
     *  the location of the segmenter's local region, the inclusive radius and
     *  the exclusive radius.
     */
    void segmentateRegion(const cv::Mat& image, std::vector<int>& labels,
        std::vector<std::vector<cv::Point> >& contour,
        cv::Mat& feedback) const;

    /**
     *  \brief Segmentates the given input
     *
     *  Convenience method. Idem to segmentateRegion(input,contour).
     *  However, it already stores the results in a vector of Blob messages.
     *
     *  \param image_ptr Input image ROS message constant pointer.
     *  \param blobs Vector of Blob messages. After calling this method, it
     *  should contain only one Blob.
     */
    void segmentateRegion(sensor_msgs::ImageConstPtr image_ptr,
        Blobs& blobs) const;

    /**
     *  \brief Segmentates the given input
     *
     *  Convenience method. Idem to segmentateRegion(input,blobs).
     *  However, it additionally provides a feedback image message.
     *
     *  \param image_ptr Input image ROS message constant pointer.
     *  \param blobs Vector of Blob messages.
     *  \param feedback Output argument. Feedback image message.
     */
    void segmentateRegion(sensor_msgs::ImageConstPtr image_ptr,
        Blobs& blobs, sensor_msgs::Image& feedback) const;

    /**
     *  \brief Segmentates the given input around a certain region.
     *
     *  Quite similar to segmentateRegion. However, this one is aimed at
     *  detecting the cavities of the toy sphere. Therefore, it does not
     *  try to infere the color and does not care about the labels.
     *
     *  \param image Input image.
     *  \param contour The contour of the detected shape shall be stored
     *  here (it should be only one contour).
     */
    void segmentateCavity(const cv::Mat& image,
        std::vector<std::vector<cv::Point> >& contour) const;

    /**
     *  \brief Segmentates the given input. For cavities.
     *
     *  Idem to segmentateCavity(input,contour), but with an additional
     *  feedback parameter.
     *
     *  \param image Input image.
     *  \param contour The contour of the detected shape shall be stored
     *  here
     *  \param feedback Output argument. This image is useful for fine tuning
     *  the location of the segmenter's local region, the inclusive radius and
     *  the exclusive radius.
     */
    void segmentateCavity(const cv::Mat& image,
        std::vector<std::vector<cv::Point> >& contour,
        cv::Mat& feedback) const;

    /**
     *  \brief Segmentates the given input
     *
     *  Convenience method. Idem to segmentateCavity(input,contour).
     *  However, it already stores the results in a vector of Blob messages.
     *
     *  \param image_ptr Input image ROS message constant pointer.
     *  \param blobs Vector of Blob messages. After calling this method, it
     *  should contain only one Blob. The label is not set to any relevant
     *  value.
     */
    void segmentateCavity(sensor_msgs::ImageConstPtr image_ptr,
        Blobs& blobs) const;

    /**
     *  \brief Segmentates the given input
     *
     *  Convenience method. Idem to segmentateCavity(input,blobs).
     *  However, it additionally provides a feedback image message.
     *
     *  \param image_ptr Input image ROS message constant pointer.
     *  \param blobs Vector of Blob messages. The label of the only blob is
     *  not filled with any relevant value.
     *  \param feedback Output argument. Feedback image message.
     */
    void segmentateCavity(sensor_msgs::ImageConstPtr image_ptr,
        Blobs& blobs, sensor_msgs::Image& feedback) const;
   /**
    * \brief Lock Algorithm
    *
    * Locks access to the Algorithm class
    */
    void lock(void) { pthread_mutex_lock(&this->access_); };

   /**
    * \brief Unlock Algorithm
    *
    * Unlocks access to the Algorithm class
    */
    void unlock(void) { pthread_mutex_unlock(&this->access_); };

   /**
    * \brief Tries Access to Algorithm
    *
    * Tries access to Algorithm
    * 
    * \return true if the lock was adquired, false otherwise
    */
    bool try_enter(void) 
    { 
      if(pthread_mutex_trylock(&this->access_)==0)
        return true;
      else
        return false;
    };

   /**
    * \brief config update
    *
    * In this function the driver parameters must be updated with the input
    * config variable. Then the new configuration state will be stored in the 
    * Config attribute.
    *
    * \param new_cfg the new driver configuration state
    *
    * \param level level in which the update is taken place
    */
    void config_update(Config& config, uint32_t level=0);

    // here define all piece_segmenter_alg interface methods to retrieve and set
    // the driver parameters

   /**
    * \brief Destructor
    *
    * This destructor is called when the object is about to be destroyed.
    *
    */
    ~PieceSegmenterAlgorithm(void);
};

#endif
