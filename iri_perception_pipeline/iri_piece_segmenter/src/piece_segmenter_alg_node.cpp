#include "piece_segmenter_alg_node.h"

PieceSegmenterAlgNode::PieceSegmenterAlgNode(void) :
  algorithm_base::IriBaseAlgorithm<PieceSegmenterAlgorithm>(),
  feedback_cavity_segmenter_camera_manager(
      ros::NodeHandle("~feedback_cavity_segmenter")),
  feedback_region_segmenter_camera_manager(
      ros::NodeHandle("~feedback_region_segmenter")),
  feedback_camera_manager(ros::NodeHandle("~feedback")),
  it(this->public_node_handle_)
{
  //init class attributes if necessary
  this->loop_rate_ = 1;//in [Hz]

  segmentate_calls_ = 0;
  segmentate_region_calls_ = 0;
  segmentate_cavity_calls_ = 0;

  // [init publishers]
  this->feedback_cavity_segmenter_publisher_ = this->it.advertiseCamera(
      "feedback_cavity_segmenter/image_raw", 1);

  this->feedback_region_segmenter_publisher_ = this->it.advertiseCamera(
      "feedback_region_segmenter/image_raw", 1);

  this->feedback_publisher_ = this->it.advertiseCamera(
      "feedback/image_raw",1);
  
  // [init subscribers]
  this->imgin_subscriber_ = this->it.subscribeCamera(
      "imgin/image_raw", 1, &PieceSegmenterAlgNode::imgin_callback, this);
  pthread_mutex_init(&this->imgin_mutex_,NULL);

  
  // [init services]
  this->segmentateCavity_server_ = this->public_node_handle_.advertiseService(
      "SegmentateCavity", &PieceSegmenterAlgNode::segmentateCavityCallback,
      this);
  pthread_mutex_init(&this->segmentateCavity_mutex_,NULL);

  this->segmentateRegion_server_ = this->public_node_handle_.advertiseService(
      "SegmentateRegion", &PieceSegmenterAlgNode::segmentateRegionCallback,
      this);
  pthread_mutex_init(&this->segmentateRegion_mutex_,NULL);

  this->segmentate_server_ = this->public_node_handle_.advertiseService(
      "Segmentate", &PieceSegmenterAlgNode::segmentateCallback, this);
  pthread_mutex_init(&this->segmentate_mutex_,NULL);

  
  // [init clients]
  
  // [init action servers]
  
  // [init action clients]
}

PieceSegmenterAlgNode::~PieceSegmenterAlgNode(void)
{
  // [free dynamic memory]
  pthread_mutex_destroy(&this->segmentateCavity_mutex_);
  pthread_mutex_destroy(&this->segmentateRegion_mutex_);
  pthread_mutex_destroy(&this->segmentate_mutex_);
  pthread_mutex_destroy(&this->imgin_mutex_);
}

void PieceSegmenterAlgNode::mainNodeThread(void)
{
  ROS_INFO("%i publishers to %s",imgin_subscriber_.getNumPublishers(),
      imgin_subscriber_.getTopic().c_str());
  ROS_INFO("%i subscribers to feedback topic (%s)",
      feedback_publisher_.getNumSubscribers(),
      feedback_publisher_.getTopic().c_str());
  ROS_INFO("%i subscribers to region segmenter feedback topic (%s)",
      feedback_region_segmenter_publisher_.getNumSubscribers(),
      feedback_region_segmenter_publisher_.getTopic().c_str());
  ROS_INFO("%i subscribers to cavity segmenter feedback topic (%s)",
      feedback_cavity_segmenter_publisher_.getNumSubscribers(),
      feedback_cavity_segmenter_publisher_.getTopic().c_str());
  ROS_INFO("%i service (%s) calls so far.\n---",segmentate_calls_,
      segmentate_server_.getService().c_str());
  ROS_INFO("%i service (%s) calls so far.\n---",segmentate_region_calls_,
      segmentateRegion_server_.getService().c_str());
  ROS_INFO("%i service (%s) calls so far.\n---",segmentate_cavity_calls_,
      segmentateCavity_server_.getService().c_str());

  // [fill msg structures]
  
  // [fill srv structure and make request to the server]
  
  // [fill action structure and make request to the action server]

  // [publish messages]

}

/*  [subscriber callbacks] */
void PieceSegmenterAlgNode::imgin_callback(
    const sensor_msgs::Image::ConstPtr& msg,
    const sensor_msgs::CameraInfoConstPtr& info)
{
  ROS_DEBUG("PieceSegmenterAlgNode::imgin_callback: New Message Received");

  last_imgin_ = msg;

  /* There are subscribers for primary segmenter feedback. */
  bool prim_seg = feedback_publisher_.getNumSubscribers() > 0;
  /* There are subscribers for secondary segmenter feedback. */
  bool seco_seg = feedback_region_segmenter_publisher_.getNumSubscribers() > 0;
  /* There are subscribers for terciary segmenter feedback. */
  bool terc_seg = feedback_cavity_segmenter_publisher_.getNumSubscribers() > 0;

  if (prim_seg or seco_seg or terc_seg)
  {
    // use appropiate mutex to shared variables if necessary
    this->alg_.lock();

    /* Dummy variable. Required for calling alg_.segmentate*/
    std::vector<iri_perception_pipeline_msgs::Blob> dummy;

    if (prim_seg)
    {
      ROS_DEBUG("There are subscribers to the feedback topic. Publishing...");
      sensor_msgs::Image feedback;
      alg_.segmentate(msg,dummy,feedback);
      feedback_publisher_.publish(feedback,*info);
    }
    
    if (seco_seg)
    {
      ROS_DEBUG("There are subscribers to the secondary feedback topic. Publishing...");
      sensor_msgs::Image feedback;
      alg_.segmentateRegion(msg,dummy,feedback);
      feedback_region_segmenter_publisher_.publish(feedback,*info);
    }

    if (terc_seg)
    {
      ROS_DEBUG("There are subscribers to the terciary feedback topic. Publishing...");
      sensor_msgs::Image feedback;
      alg_.segmentateCavity(msg,dummy,feedback);
      feedback_cavity_segmenter_publisher_.publish(feedback,*info);
    }

    // unlock previously blocked shared variables
    this->alg_.unlock();
  }
}

void PieceSegmenterAlgNode::imgin_mutex_enter(void)
{
  pthread_mutex_lock(&this->imgin_mutex_);
}

void PieceSegmenterAlgNode::imgin_mutex_exit(void)
{
  pthread_mutex_unlock(&this->imgin_mutex_);
}


/*  [service callbacks] */
bool PieceSegmenterAlgNode::segmentateCavityCallback(iri_perception_pipeline_msgs::Segmentate::Request &req, iri_perception_pipeline_msgs::Segmentate::Response &res)
{
  ROS_DEBUG("PieceSegmenterAlgNode::segmentateCavityCallback: New Request Received!");

  //use appropiate mutex to shared variables if necessary
  this->alg_.lock();
  alg_.segmentateCavity(last_imgin_,res.blobs);
  ++segmentate_cavity_calls_;
  // unlock previously blocked shared variables
  this->alg_.unlock();
  return true;
}

void PieceSegmenterAlgNode::segmentateCavity_mutex_enter(void)
{
  pthread_mutex_lock(&this->segmentateCavity_mutex_);
}

void PieceSegmenterAlgNode::segmentateCavity_mutex_exit(void)
{
  pthread_mutex_unlock(&this->segmentateCavity_mutex_);
}

bool PieceSegmenterAlgNode::segmentateRegionCallback(
    iri_perception_pipeline_msgs::Segmentate::Request &req,
    iri_perception_pipeline_msgs::Segmentate::Response &res)
{
  ROS_DEBUG("PieceSegmenterAlgNode::segmentateRegionCallback: New Request Received!");

  //use appropiate mutex to shared variables if necessary
  this->alg_.lock();
  alg_.segmentateRegion(last_imgin_,res.blobs);
  ++segmentate_region_calls_;
  // unlock previously blocked shared variables
  this->alg_.unlock();
  return true;
}

void PieceSegmenterAlgNode::segmentateRegion_mutex_enter(void)
{
  pthread_mutex_lock(&this->segmentateRegion_mutex_);
}

void PieceSegmenterAlgNode::segmentateRegion_mutex_exit(void)
{
  pthread_mutex_unlock(&this->segmentateRegion_mutex_);
}

bool PieceSegmenterAlgNode::segmentateCallback(
    iri_perception_pipeline_msgs::Segmentate::Request &req,
    iri_perception_pipeline_msgs::Segmentate::Response &res)
{
  ROS_DEBUG("PieceSegmenterAlgNode::segmentateCallback: New Request Received!");

  //use appropiate mutex to shared variables if necessary
  this->alg_.lock();
  alg_.segmentate(last_imgin_,res.blobs);
  ++segmentate_calls_;
  // unlock previously blocked shared variables
  this->alg_.unlock();

  return true;
}

void PieceSegmenterAlgNode::segmentate_mutex_enter(void)
{
  pthread_mutex_lock(&this->segmentate_mutex_);
}

void PieceSegmenterAlgNode::segmentate_mutex_exit(void)
{
  pthread_mutex_unlock(&this->segmentate_mutex_);
}


/*  [action callbacks] */

/*  [action requests] */

void PieceSegmenterAlgNode::node_config_update(Config &config, uint32_t level)
{
  this->alg_.lock();
  this->config_=config;
  this->alg_.unlock();
}

void PieceSegmenterAlgNode::addNodeDiagnostics(void)
{
}

/* main function */
int main(int argc,char *argv[])
{
  return algorithm_base::main<PieceSegmenterAlgNode>(argc, argv,
      "piece_segmenter_alg_node");
}
