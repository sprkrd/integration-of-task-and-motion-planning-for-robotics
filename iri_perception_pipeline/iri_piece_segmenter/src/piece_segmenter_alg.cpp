#include <cv_bridge/cv_bridge.h>
#include <ros/assert.h>
#include "piece_segmenter_alg.h"

/* Unnammed namespace for implementation functions. */
namespace
{

/* \brief contours and labels to Blob messages.
 * 
 * \param contours Vector of OpenCV contours
 * \param labels A label per contour
 * \param blobs A vector of Blob messages
 *
 * Store contours and labels in Blob messages. Blob messages have the following
 * structure:
 *  Blob
 *    int32 label
 *    Point2D[] contour
 *      (Each Point2D)
 *      int32 x
 *      int32 y
 */
void copyTo(const std::vector<std::vector<cv::Point> >& contours,
    const std::vector<int>& labels, PieceSegmenterAlgorithm::Blobs& blobs)
{
  blobs.resize(contours.size());
  for (int i = 0; i < contours.size(); ++i)
  {
    blobs[i].contour.resize(contours[i].size());
    for (int j = 0; j < contours[i].size(); ++j)
    {
      blobs[i].contour[j].x = contours[i][j].x;
      blobs[i].contour[j].y = contours[i][j].y;
    }
    blobs[i].label = labels[i];
  }
}

}

cv::Rect PieceSegmenterAlgorithm::get_roi_rect(const cv::Size& size) const
{
  /* Compute ROI rect */
  int x_corner = config_.crop_left;
  int y_corner = config_.crop_top;
  int width = size.width - config_.crop_left - config_.crop_right;
  int height = size.height - config_.crop_top - config_.crop_bottom;
  return cv::Rect(x_corner,y_corner,width,height);
}

PieceSegmenterAlgorithm::PieceSegmenterAlgorithm(void)
{
  pthread_mutex_init(&this->access_,NULL);
  color_tab_[EDGE_LABEL] = cv::Vec3b(128,128,128);
  color_tab_[BG_LABEL] = cv::Vec3b(0,0,0);
}

PieceSegmenterAlgorithm::~PieceSegmenterAlgorithm(void)
{
  pthread_mutex_destroy(&this->access_);
}

void PieceSegmenterAlgorithm::segmentate(const cv::Mat& image,
    std::vector<int>& labels,
    std::vector<std::vector<cv::Point> >& contours) const
{
  /* Compute ROI rect */
  cv::Rect roi = get_roi_rect(image.size());

  /* Segment pixels inside ROI */
  segmenter_(cv::Mat(image,roi),labels,contours,config_.min_blob_area,
      cv::Point(roi.x,roi.y));
}

void PieceSegmenterAlgorithm::segmentate(const cv::Mat& image,
    std::vector<int>& labels, std::vector<std::vector<cv::Point> >& contours,
    cv::Mat& feedback) const
{
  segmentate(image,labels,contours);

  /* Colorize feedback according to labels */
  feedback.create(image.size(),CV_8UC3);
  feedback = cv::Scalar(color_tab_[EDGE_LABEL]);

  cv::Rect roi = get_roi_rect(image.size());
  cv::rectangle(feedback,roi,cv::Scalar(color_tab_[BG_LABEL]),CV_FILLED);

  for (int i = 0; i < contours.size(); ++i)
  {
    const cv::Vec3b& color = color_tab_[labels[i]];
    cv::drawContours(image,contours,i,cv::Scalar(color),CV_FILLED);
  }

  feedback = 0.5*feedback + 0.5*image;
}

void PieceSegmenterAlgorithm::segmentate(sensor_msgs::ImageConstPtr image_ptr,
    PieceSegmenterAlgorithm::Blobs& blobs) const
{
  cv_bridge::CvImageConstPtr cv_image = cv_bridge::toCvShare(image_ptr,"bgr8");
  const cv::Mat& image = cv_image->image;

  /* Segmentate (obtain contours and a label per contour). */
  std::vector<int> labels;
  std::vector<std::vector<cv::Point> > contours;
  segmentate(image,labels,contours);

  /* Store contours and labels into Blob messages. */
  copyTo(contours,labels,blobs);
}

void PieceSegmenterAlgorithm::segmentate(sensor_msgs::ImageConstPtr image_ptr,
    PieceSegmenterAlgorithm::Blobs& blobs, sensor_msgs::Image& feedback) const
{
  cv_bridge::CvImageConstPtr cv_image = cv_bridge::toCvShare(image_ptr,"bgr8");
  const cv::Mat& image = cv_image->image;
  cv::Mat feedback_mat;

  /* Segmentate (obtain contours and a label per contour). */
  std::vector<int> labels;
  std::vector<std::vector<cv::Point> > contours;
  segmentate(image,labels,contours,feedback_mat);

  /* Store contours and labels into Blob messages. */
  copyTo(contours,labels,blobs);

  /* Convert feedback to ROS message*/
  cv_bridge::CvImage cv_feedback(image_ptr->header,"bgr8",feedback_mat);
  cv_feedback.toImageMsg(feedback);
}

void PieceSegmenterAlgorithm::segmentateRegion(const cv::Mat& image,
    std::vector<int>& labels,
    std::vector<std::vector<cv::Point> >& contour) const
{
  cv::Mat mask = region_segmenter_(image);
  cv::Scalar mean = cv::mean(image,mask);
  /* Obtain most suitable label. */
  labels.resize(1);
  double min_norm = 256;
  for (int i = RED_LABEL; i <= BLUE_LABEL; ++i)
  {
    double norm = cv::norm(mean,cv::Scalar(color_tab_[i]),cv::NORM_INF);
    if (norm < min_norm)
    {
      min_norm = norm;
      labels[0] = i;
    }
  }
  cv::findContours(mask,contour,cv::noArray(),CV_RETR_EXTERNAL,
      CV_CHAIN_APPROX_SIMPLE);
  /* Guarantee that there will be just one contour. cv_extra::RegionSegmenter
   * already provides such guarantee. However, just in case...*/
  ROS_ASSERT(contour.size() == 1);
}

void PieceSegmenterAlgorithm::segmentateRegion(const cv::Mat& image,
    std::vector<int>& labels, std::vector<std::vector<cv::Point> >& contour,
    cv::Mat& feedback) const
{
  /* Obtain contour. */
  segmentateRegion(image,labels,contour);
  /* Create and colorize feedback image. */
  feedback.create(image.size(),CV_8UC3);
  feedback = cv::Scalar(color_tab_[EDGE_LABEL]);
  cv::circle(feedback,region_segmenter_.get_center(),config_.exclusive_radius,
      cv::Scalar(color_tab_[BG_LABEL]),CV_FILLED);
  /* Draw detected contout. */
  cv::drawContours(feedback,contour,0,cv::Scalar(color_tab_[labels[0]]),
      CV_FILLED);
  cv::circle(feedback,region_segmenter_.get_center(),config_.inclusive_radius,
      cv::Scalar(color_tab_[EDGE_LABEL]),CV_FILLED);

  feedback = 0.5*feedback + 0.5*image;
}

void PieceSegmenterAlgorithm::segmentateRegion(
    sensor_msgs::ImageConstPtr image_ptr,
    PieceSegmenterAlgorithm::Blobs& blobs) const
{
  cv_bridge::CvImageConstPtr cv_image = cv_bridge::toCvShare(image_ptr,"bgr8");
  const cv::Mat& image = cv_image->image;

  /* Segmentate (obtain contour). */
  std::vector<int> label;
  std::vector<std::vector<cv::Point> > contour;
  segmentateRegion(image,label,contour);

  /* Store contour and labels into Blob message. */
  copyTo(contour,label,blobs);
}

void PieceSegmenterAlgorithm::segmentateRegion(
    sensor_msgs::ImageConstPtr image_ptr,
    PieceSegmenterAlgorithm::Blobs& blobs,
    sensor_msgs::Image& feedback) const
{
  cv_bridge::CvImageConstPtr cv_image = cv_bridge::toCvShare(image_ptr,"bgr8");
  const cv::Mat& image = cv_image->image;
  cv::Mat feedback_mat;

  /* Segmentate (obtain contours and a label per contour). */
  std::vector<int> label;
  std::vector<std::vector<cv::Point> > contour;
  segmentateRegion(image,label,contour,feedback_mat);

  /* Store contours and labels into Blob messages. */
  copyTo(contour,label,blobs);

  /* Convert feedback to ROS message*/
  cv_bridge::CvImage cv_feedback(image_ptr->header,"bgr8",feedback_mat);
  cv_feedback.toImageMsg(feedback);
}

void PieceSegmenterAlgorithm::segmentateCavity(const cv::Mat& image,
    std::vector<std::vector<cv::Point> >& contour) const
{
  cavity_segmenter_(image,contour);
  /* Guarantee that there will be just one contour. cv_extra::RegionSegmenter
   * already provides such guarantee. However, just in case...*/
  ROS_ASSERT(contour.size() == 1);
}

void PieceSegmenterAlgorithm::segmentateCavity(const cv::Mat& image,
    std::vector<std::vector<cv::Point> >& contour, cv::Mat& feedback) const
{
  /* Obtain contour. */
  segmentateCavity(image,contour);
  /* Create and colorize feedback image. */
  feedback.create(image.size(),CV_8UC3);
  feedback = cv::Scalar(color_tab_[EDGE_LABEL]);
  cv::circle(feedback,cavity_segmenter_.get_center(),config_.cavity_ex_radius,
      cv::Scalar(color_tab_[BG_LABEL]),CV_FILLED);
  /* Draw detected contour. We do not care about the color (we have chosen
   * (0,255,0) as we can have chosen any other). */
  cv::drawContours(feedback,contour,0,cv::Scalar(0,255,0), CV_FILLED);
  cv::circle(feedback,cavity_segmenter_.get_center(),config_.cavity_in_radius,
      cv::Scalar(color_tab_[EDGE_LABEL]),CV_FILLED);

  feedback = 0.5*feedback + 0.5*image;
}

void PieceSegmenterAlgorithm::segmentateCavity(
    sensor_msgs::ImageConstPtr image_ptr,
    PieceSegmenterAlgorithm::Blobs& blobs) const
{
  cv_bridge::CvImageConstPtr cv_image = cv_bridge::toCvShare(image_ptr,"bgr8");
  const cv::Mat& image = cv_image->image;

  /* Segmentate (obtain contour). The label does not matter. Any will do. */
  std::vector<int> label(1,-1);
  std::vector<std::vector<cv::Point> > contour;
  segmentateCavity(image,contour);

  /* Store contour and label into Blob message. */
  copyTo(contour,label,blobs);
}

void PieceSegmenterAlgorithm::segmentateCavity(
    sensor_msgs::ImageConstPtr image_ptr,
    PieceSegmenterAlgorithm::Blobs& blobs,
    sensor_msgs::Image& feedback) const
{
  cv_bridge::CvImageConstPtr cv_image = cv_bridge::toCvShare(image_ptr,"bgr8");
  const cv::Mat& image = cv_image->image;
  cv::Mat feedback_mat;

  /* Segmentate (obtain contour, label does not matter). */
  std::vector<int> label(1,-1);
  std::vector<std::vector<cv::Point> > contour;
  segmentateCavity(image,contour,feedback_mat);

  /* Store contours and labels into Blob messages. */
  copyTo(contour,label,blobs);

  /* Convert feedback to ROS message*/
  cv_bridge::CvImage cv_feedback(image_ptr->header,"bgr8",feedback_mat);
  cv_feedback.toImageMsg(feedback);
}

void PieceSegmenterAlgorithm::config_update(Config& config, uint32_t level)
{
  this->lock();

  /* Configure segmenter. */
  segmenter_[RED_LABEL] =
    cv::Scalar(config.red_h,config.red_s,config.red_v);
  segmenter_[YELLOW_LABEL] =
    cv::Scalar(config.yellow_h,config.yellow_s,config.yellow_v);
  segmenter_[GREEN_LABEL] =
    cv::Scalar(config.green_h,config.green_s,config.green_v);
  segmenter_[BLUE_LABEL] =
    cv::Scalar(config.blue_h,config.blue_s,config.blue_v);

  segmenter_.set_tolerance(
      cv::Scalar(config.h_tolerance,config.s_tolerance,config.v_tolerance));

  /* Configure region segmenter. */
  region_segmenter_.set_center(cv::Point(config.center_x,config.center_y));
  region_segmenter_.set_exclusive_radius(config.exclusive_radius);
  region_segmenter_.set_inclusive_radius(config.inclusive_radius);

  /* Configure cavity segmenter. */
  cavity_segmenter_.set_center(cv::Point(config.cavity_x,config.cavity_y));
  cavity_segmenter_.set_exclusive_radius(config.cavity_ex_radius);
  cavity_segmenter_.set_inclusive_radius(config.cavity_in_radius);

  /* Compute color_tabs for feedback */
  color_tab_[RED_LABEL] = cv_extra::cvtColor(
      cv::Vec3b(config.red_h,config.red_s,config.red_v),CV_HSV2BGR);
  color_tab_[YELLOW_LABEL] = cv_extra::cvtColor(
      cv::Vec3b(config.yellow_h,config.yellow_s,config.yellow_v),CV_HSV2BGR);
  color_tab_[GREEN_LABEL] = cv_extra::cvtColor(
      cv::Vec3b(config.green_h,config.green_s,config.green_v),CV_HSV2BGR);
  color_tab_[BLUE_LABEL] = cv_extra::cvtColor(
      cv::Vec3b(config.blue_h,config.blue_s,config.blue_v),CV_HSV2BGR);

  // save the current configuration
  this->config_=config;
  
  this->unlock();
}

// PieceSegmenterAlgorithm Public API
