#!/usr/bin/python

import sys
import rospy
import moveit_commander
import time
import moveit_msgs.msg

from geometry_msgs.msg import PoseStamped


print "=== Starting IRI WAM move group_picker example"

moveit_commander.roscpp_initialize(sys.argv)
rospy.init_node('move_group_catcher_python_interface_tutorial', anonymous=True)

robot = moveit_commander.RobotCommander()

scene = moveit_commander.PlanningSceneInterface()

group_catcher = robot.get_group("catcher")
group_catcher.set_planner_id("RRTConnectkConfigDefault")

display_trajectory_publisher = rospy.Publisher("move_group_catcher/display_planned_path",
        moveit_msgs.msg.DisplayTrajectory)

print "============ Reference frame: %s" % group_catcher.get_planning_frame()
print "============ Reference frame: %s" % group_catcher.get_end_effector_link()

print "============ Robot group_catchers:"
print robot.get_group_names()

print "============ Printing robot state"
print robot.get_current_state()
print "============"

pose_target = PoseStamped()
pose_target.header.frame_id = "iri_wam_catcher_link_base"
pose_target.pose.position.x = 0.5
pose_target.pose.position.y = 0.0
pose_target.pose.position.z = 0.5
pose_target.pose.orientation.w = 1.0

group_catcher.set_joint_value_target(pose_target, "iri_wam_catcher_sphere_link")
#group_picker.set_pose_target(pose_target, "iri_wam_catcher_link_tc")

plan = group_catcher.plan()

if plan is not None:
    rospy.loginfo("Plan to pose_target found. Executing...")
    group_catcher.execute(plan)
else:
    rospy.loginfo("Plan to pose_target not found.")

# Compute plan from arbitratry start configuration and move in joints

group_picker = moveit_commander.MoveGroupCommander("picker") 
group_picker.set_planner_id("RRTConnectkConfigDefault")

start = robot.get_current_state()
start.joint_state.position = list(start.joint_state.position)
start.joint_state.position[0] = 1.57

target = group_picker.get_current_joint_values()
print type(target), target

group_picker.set_start_state(start)
group_picker.set_joint_value_target(target)

plan = group_picker.plan()

group_picker.execute(plan)

# Plan for both groups

group_both = moveit_commander.MoveGroupCommander("both")
group_both.set_planner_id("RRTConnectkConfigDefault")


pose1 = PoseStamped()
pose1.header.frame_id = "iri_wam_picker_link_base"
pose1.pose.position.x = 0.5
pose1.pose.position.y = 0.0
pose1.pose.position.z = 0.5
pose1.pose.orientation.w = 1.0

pose2 = PoseStamped()
pose2.header.frame_id = "iri_wam_catcher_link_base"
pose2.pose.position.x = 0.0
pose2.pose.position.y = -0.5
pose2.pose.position.z = 0.5
pose2.pose.orientation.w = 1.0

group_both.set_pose_target(pose1, "iri_wam_picker_link_7")
group_both.set_pose_target(pose2, "iri_wam_catcher_link_7")

plan = group_both.plan()

group_both.execute(plan)

print "Done"

moveit_commander.roscpp_shutdown()
