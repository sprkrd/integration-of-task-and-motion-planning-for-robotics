#include <ros/ros.h>
#include <geometry_msgs/Pose.h>

// MoveIt!
#include <moveit/move_group_interface/move_group.h>
#include <moveit/planning_scene_interface/planning_scene_interface.h>

#include <moveit_msgs/DisplayRobotState.h>
#include <moveit_msgs/DisplayTrajectory.h>

#include <moveit_msgs/AttachedCollisionObject.h>
#include <moveit_msgs/CollisionObject.h>

#include <cmath>

#include <sys/time.h>
#include <time.h>

// struct timeval start, end;
// 
// void tic()
// {
//   gettimeofday(&start, NULL);
// }
// 
// void toc()
// {
//   gettimeofday(&end, NULL);
//   time_t sec = end.tv_sec - start.tv_sec;
//   suseconds_t usec = end.tv_usec - start.tv_usec;
//   ROS_INFO("Elapsed %lds %ldu", sec, usec);
// }

clock_t start;

inline void tic()
{
  start = clock();
}

inline double toc()
{
  clock_t end = clock();
  return ((double) end - start)/CLOCKS_PER_SEC;
}

int main(int argc, char* argv[])
{
  ros::init(argc, argv, "iri_wam_move_group");
  ros::AsyncSpinner spinner(1);
  spinner.start();

  ros::NodeHandle nh;

  //sleep(5.0);

  moveit::planning_interface::MoveGroup group("catcher");

  robot_state::RobotState state(*group.getCurrentState());
  const robot_state::JointModelGroup* j_model_group = state.getJointModelGroup(group.getName());

  const int N = 10000;
  random_numbers::RandomNumberGenerator rng(42); // in order to get reproducible results
  std::vector<Eigen::Affine3d> ee_transforms(N);
  std::vector<double> elapsed(N);
  int nsuccess = 0;

  for (int i = 0; i < N; ++i)
  {
    state.setToRandomPositions(j_model_group, rng);
    ee_transforms[i] = state.getGlobalLinkTransform("iri_wam_catcher_sphere_link");
  }

  for (int i = 0; i < N; ++i)
  {
    //ROS_INFO_STREAM("Tranlation: " << ee_transforms[i].translation());
    //ROS_INFO_STREAM("Rotation: " << ee_transforms[i].rotation());
    tic();
    bool success = state.setFromIK(j_model_group, ee_transforms[i], "iri_wam_catcher_sphere_link");
    elapsed[i] = toc();
    //ROS_INFO("Elapsed: %f. IK: %s\n---", elapsed[i], success? "SUCCESS" : "FAIL");
    if (success) ++nsuccess;
  }

  double mean_elapsed = 0, var_elapsed = 0, std_elapsed;
  for (int i = 0; i < N; ++i)
  {
    mean_elapsed += elapsed[i];
    var_elapsed += elapsed[i]*elapsed[i];
  }
  mean_elapsed /= N;
  var_elapsed = var_elapsed/N - mean_elapsed*mean_elapsed;

  std_elapsed = sqrt(var_elapsed);

  ROS_INFO("Mean elapsed: %f. Std elapsed: %f. %%success: %f", mean_elapsed, std_elapsed, nsuccess*100.0/N);

}
