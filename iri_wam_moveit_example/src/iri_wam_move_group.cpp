#include <ros/ros.h>
#include <geometry_msgs/Pose.h>

// MoveIt!
#include <moveit/move_group_interface/move_group.h>
#include <moveit/planning_scene_interface/planning_scene_interface.h>

#include <moveit_msgs/DisplayRobotState.h>
#include <moveit_msgs/DisplayTrajectory.h>

#include <moveit_msgs/AttachedCollisionObject.h>
#include <moveit_msgs/CollisionObject.h>

#include <sys/time.h>
#include <time.h>

// struct timeval start, end;
// 
// void tic()
// {
//   gettimeofday(&start, NULL);
// }
// 
// void toc()
// {
//   gettimeofday(&end, NULL);
//   time_t sec = end.tv_sec - start.tv_sec;
//   suseconds_t usec = end.tv_usec - start.tv_usec;
//   ROS_INFO("Elapsed %lds %ldu", sec, usec);
// }

clock_t start;

void tic()
{
  start = clock();
}

void toc()
{
  clock_t end = clock();
  double elapsed = ((double) end - start)/CLOCKS_PER_SEC;
  ROS_INFO("elapsed %fs", elapsed);
}

int main(int argc, char* argv[])
{
  ros::init(argc, argv, "iri_wam_move_group");
  ros::AsyncSpinner spinner(1);
  spinner.start();

  ros::NodeHandle nh;

  bool success;

  //sleep(5.0);

  moveit::planning_interface::MoveGroup group("picker");
  group.allowReplanning(true);
  group.allowLooking(false);
  group.setPlannerId("RRTConnectkConfigDefault");
  //group.setPoseReferenceFrame("iri_wam_picker_link_base");

  moveit::planning_interface::PlanningSceneInterface planning_scene_interface; 

  //ros::Publisher display_publisher =
    //nh.advertise<moveit_msgs::DisplayTrajectory>(
        //"/move_group/display_planned_path", 1, true);

  //moveit_msgs::DisplayTrajectory display_trajectory;

  ROS_INFO("Reference frame: %s", group.getPoseReferenceFrame().c_str());

  ROS_INFO("End effector link: %s", group.getEndEffectorLink().c_str());

  geometry_msgs::PoseStamped target_pose1;
  target_pose1.header.frame_id = "iri_wam_picker_link_base";
  //geometry_msgs::Pose target_pose1;
  target_pose1.pose.orientation.w = 1.0;
  target_pose1.pose.position.x = 0.5;
  target_pose1.pose.position.y = 0.0;
  target_pose1.pose.position.z = 0.5;

  //group.setPoseTarget(target_pose1);
  tic();
  group.setJointValueTarget(target_pose1);
  toc();

  moveit::planning_interface::MoveGroup::Plan plan;
  tic();
  success = group.plan(plan);
  toc();

  ROS_INFO("Visualizing plan 1 (pose goal) %s",success?"":"FAILED");

  sleep(5.0);

  //if (success)
  //{
    //ROS_INFO("Visualizing plan 1 (again)");
    //display_trajectory.trajectory_start = plan.start_state_;
    //display_trajectory.trajectory.push_back(my_plan.trajectory_);
    //display_publisher.publish(display_trajectory);
    //sleep(5.0);
  //}
  
  if (success)
  {
    ROS_INFO("Moving to pose goal");
    group.move();
  }

  // Move in joints

  std::vector<double> group_joints;
  group.getCurrentState()->copyJointGroupPositions(group.getName(), group_joints);
  group_joints[0] = -1.57;
  group.setJointValueTarget(group_joints);
  tic();
  success = group.plan(plan);
  toc();

  ROS_INFO("Visualizing plan 2 (joint space goal) %s", success? "" : "FAILED");

  sleep(5.0);

  if (success)
  {
    ROS_INFO("Moving to pose goal");
    group.move();
  }

  // Plan for both groups
  
  geometry_msgs::PoseStamped target_pose2;
  target_pose2.header.frame_id = "iri_wam_picker_link_base";
  target_pose2.pose.orientation.w = 1.0;
  target_pose2.pose.position.x = 0.0;
  target_pose2.pose.position.y = -0.5;
  target_pose2.pose.position.z = 0.5;

  geometry_msgs::PoseStamped target_pose3;
  target_pose3.header.frame_id = "iri_wam_catcher_link_base";
  target_pose3.pose.orientation.w = 1.0;
  target_pose3.pose.position.x = 0.0;
  target_pose3.pose.position.y = 0.5;
  target_pose3.pose.position.z = 0.5;

  moveit::planning_interface::MoveGroup both_group("both");
  both_group.allowReplanning(true);
  both_group.allowLooking(false);
  both_group.setPlannerId("RRTConnectkConfigDefault");

  both_group.setPoseTarget(target_pose2, "iri_wam_picker_link_7");
  both_group.setPoseTarget(target_pose3, "iri_wam_catcher_link_7");

  tic();
  success = both_group.plan(plan);
  toc();

  ROS_INFO("Visualizing plan 3 (both arms) %s", success? "" : "FAILED");
  sleep(5.0);

  if (success)
  {
    ROS_INFO("Moving to pose goal");
    both_group.move();
  }

  //geometry_msgs::PoseStamped target_pose1;
  //target_pose1.header.frame_id = "iri_wam_link_base";

  // To compute a plan from an arbitrary start configurarion
  //robot_state::RobotState start_state(*group.getCurrentState());
  //std::vector<double> joints(7);
  //for (int i = 0; i < 7; ++i)
  //{
    //joints[i] = -1;
  //}
  //start_state.setJointGroupPositions(group.getName(), joints);

  ////start_state.setToRandomPositions();

  //target_pose1.orientation.w = 1.0;
  //target_pose1.position.x = 0.5;
  //target_pose1.position.y = 0.0;
  //target_pose1.position.z = 0.5;

  //group.setStartState(start_state);
  //group.setPoseTarget(target_pose1);

  //moveit::planning_interface::MoveGroup::Plan plan;
  //bool success = group.plan(plan);

  //ROS_INFO("Visualizing plan 1 (pose goal) %s",success?"":"FAILED");

  //sleep(5.0);

  //ros::Rate r(1);
  //while (ros::ok())
  //{
    //ROS_INFO("Hey");
    //r.sleep();
  //}
}
