#include "itm_piece_detector_alg.h"

#include <cmath>
#include <ros/package.h>

double ItmPieceDetectorAlgorithm::average(const std::vector<double>& vector)
{
  double sum = 0;
  for (int i = 0; i < vector.size(); ++i)
  {
    sum += vector[i];
  }
  return sum / vector.size();
}

void ItmPieceDetectorAlgorithm::average(
    const std::vector<iri_blobsegmenter::Matrix>& matrices,
    iri_blobsegmenter::Matrix& result)
{
  int M = matrices[0].size();
  int N = matrices[0][0].size();
  result = iri_blobsegmenter::Matrix(M, std::vector<double>(N));
  std::vector<double> sequence;
  for (int i = 0; i < M; ++i)
  {
    for (int j = 0; j < N; ++j)
    {
      extract_sequence(matrices, i, j, sequence);
      result[i][j] = average(sequence);
    }
  }
}

double ItmPieceDetectorAlgorithm::average_angles(
    const std::vector<double>& angles, double symmetry)
{
  std::vector<double> theta_x(angles.size());
  std::vector<double> theta_y(angles.size());

  double sum_x = 0;
  double sum_y = 0;
  for (int i = 0; i < angles.size(); ++i)
  {
    double angle = std::fmod(angles[i], symmetry);
    sum_x += std::cos(2*M_PI*angle/symmetry);
    sum_y += std::sin(2*M_PI*angle/symmetry);
  }
  return symmetry*std::atan2(sum_y, sum_x)/(2*M_PI);
}


void ItmPieceDetectorAlgorithm::average(
    const std::vector<std::vector<pcl::PointXYZ> >& vector,
    std::vector<pcl::PointXYZ>& average)
{
  average.resize(vector[0].size());
  for (int i = 0; i < vector[0].size(); ++i)
  {
    average[i].x = average[i].y = average[i].z = 0;
    for (int j = 0; j < vector.size(); ++j)
    {
      average[i].x += vector[j][i].x;
      average[i].y += vector[j][i].y;
      average[i].z += vector[j][i].z;
    }
    average[i].x /= vector.size();
    average[i].y /= vector.size();
    average[i].z /= vector.size();
  }
}

void ItmPieceDetectorAlgorithm::extract_sequence(
    const std::vector<iri_blobsegmenter::Matrix>& matrices, int i, int j,
    std::vector<double>& sequence)
{
  sequence.resize(matrices.size());
  for (int k = 0; k < matrices.size(); ++k)
  {
    sequence[k] = matrices[k][i][j];
  }
}

void ItmPieceDetectorAlgorithm::average_angles(
    const std::vector<iri_blobsegmenter::Matrix>& angles,
    iri_blobsegmenter::Matrix& result)
{
  int M = angles[0].size();
  int N = angles[0][0].size();
  result = iri_blobsegmenter::Matrix(M, std::vector<double>(N));
  std::vector<double> sequence;
  std::vector<std::string> names;
  library_.get_names(names);
  for (int i = 0; i < M; ++i)
  {
    for (int j = 0; j < N; ++j)
    {
      double symmetry = symmetry_[names[j]];
      extract_sequence(angles, i, j, sequence);
      result[i][j] = average_angles(sequence, symmetry);
    }
  }
}

ItmPieceDetectorAlgorithm::ItmPieceDetectorAlgorithm(void)
{
  pthread_mutex_init(&this->access_,NULL);

  segmenter_.set_color("red", cv::Vec3b());
  segmenter_.set_color("green", cv::Vec3b());
  segmenter_.set_color("yellow", cv::Vec3b());
  segmenter_.set_color("blue", cv::Vec3b());

  std::string tmp_path = ros::package::getPath("iri_itm_piece_detector") +
    "/templates";
  //library_.add_shape(tmp_path + "/star5.png", "star5", "blue", 1, 0);
  //library_.add_shape(tmp_path + "/star6.png", "star6", "blue", 0, 1);
  //library_.add_shape(tmp_path + "/star8.png", "star8", "green", 0.5, 0);
  //library_.add_shape(tmp_path + "/triangle.png", "triangle", "green", 1, 0);
  //library_.add_shape(tmp_path + "/hexagon.png", "hexagon", "green", 0, 1);
  //library_.add_shape(tmp_path + "/trapezium.png", "trapezium", "red");
  //library_.add_shape(tmp_path + "/trapezoid.png", "trapezoid", "red");
  //library_.add_shape(tmp_path + "/pentagon.png", "pentagon", "red", 0, 1);
  //library_.add_shape(tmp_path + "/cross.png", "cross", "yellow", 0, 1);
  //library_.add_shape(tmp_path + "/circularsector.png", "circularsector",
      //"yellow", 1, 0);
  library_.add_shape(tmp_path + "/star5.png", "star5", "blue");
  library_.add_shape(tmp_path + "/star6.png", "star6", "blue");
  library_.add_shape(tmp_path + "/star8.png", "star8", "green");
  library_.add_shape(tmp_path + "/triangle.png", "triangle", "green");
  library_.add_shape(tmp_path + "/hexagon.png", "hexagon", "green");
  library_.add_shape(tmp_path + "/trapezium.png", "trapezium", "red");
  library_.add_shape(tmp_path + "/trapezoid.png", "trapezoid", "red");
  library_.add_shape(tmp_path + "/pentagon.png", "pentagon", "red");
  library_.add_shape(tmp_path + "/cross.png", "cross", "yellow");
  library_.add_shape(tmp_path + "/circularsector.png", "circularsector",
      "yellow");

  symmetry_["star5"] = symmetry_["pentagon"] = 2*M_PI / 5;
  symmetry_["star6"] = symmetry_["hexagon"] = 2*M_PI / 6;
  symmetry_["star8"] = 2*M_PI / 8;
  symmetry_["triangle"] = 2*M_PI / 3;
  symmetry_["cross"] = 2*M_PI / 4;
  symmetry_["circularsector"] = symmetry_["trapezium"] = symmetry_["trapezoid"] = 2*M_PI;
}

cv::Mat ItmPieceDetectorAlgorithm::segmentate(const cv::Mat& img)
{
  cv::Mat img_f;
  if (config_.flip_h)
  {
    cv::flip(img, img_f, 0);
  }
  else
  {
    img_f = img;
  }
  segmenter_(img_f, 0.1);
  return segmenter_.get_feedback();
}

int ItmPieceDetectorAlgorithm::segmentate(const std::vector<cv::Mat>& images,
    const std::vector<PointCloud::ConstPtr>& pointclouds)
{
  iri_blobsegmenter::Matrix pca_orientations(images.size()), pca_orientations_t;
  std::vector<iri_blobsegmenter::Matrix> angles(images.size());
  std::vector<iri_blobsegmenter::Matrix> similitudes(images.size());
  std::vector<std::vector<pcl::PointXYZ> > points(images.size());
  int N;
  for (int i = 0; i < images.size(); ++i)
  {
    cv::Mat img;
    if (config_.flip_h)
    {
      cv::flip(images[i], img, 0);
    }
    else
    {
      img = images[i];
    }
    segmenter_(img);
    if (i == 0)
    {
      N = segmenter_.get_contours().size();
      if (N == 0)
      {
        return 0; // nothing to work with
      }
    }
    else
    {
      if (N != segmenter_.get_contours().size())
      {
        return -1; // different number of contours => Abort!!
      }
    }
    points[i].resize(N);
    for (int j = 0; j < N; ++j)
    {
      cv::Point centroid = segmenter_.get_means()[j];
      points[i][j] = (*pointclouds[i])(centroid.x, centroid.y);
    }
    pca_orientations[i] = segmenter_.get_orientations();
    library_(segmenter_);
    library_.get_corrected_angles(angles[i]);
    library_.get_similitudes(similitudes[i]);
  }
  average_angles(angles, avg_angles_);
  average(similitudes, avg_similitudes_);
  avg_pca_orientations_.resize(N);
  iri_blobsegmenter::traspose(pca_orientations, pca_orientations_t);
  for (int i = 0; i < pca_orientations_t.size(); ++i)
  {
    avg_pca_orientations_[i] = average_angles(pca_orientations_t[i], 2*M_PI);
  }
  average(points, centroids_);
  return 0;
}


ItmPieceDetectorAlgorithm::~ItmPieceDetectorAlgorithm(void)
{
  pthread_mutex_destroy(&this->access_);
}

void ItmPieceDetectorAlgorithm::config_update(Config& config, uint32_t level)
{
  this->lock();

  // save the current configuration
  this->config_=config;

  // roi
  segmenter_.set_roi(config_.crop_top, config_.crop_bottom, config_.crop_left,
      config_.crop_right);

  // tolerance
  segmenter_.set_tolerance(cv::Vec3b(config_.h_tolerance, config_.s_tolerance,
        config_.v_tolerance));

  // other
  segmenter_.set_fg_erosion_radius(config_.fg_erosion_r);
  segmenter_.set_bg_erosion_radius(config_.bg_erosion_r);
  segmenter_.set_min_area(config_.min_area);
  segmenter_.set_max_area(config_.max_area);

  // colors
  segmenter_.clear_colors();
  if (config_.segment_holes)
  {
    library_.set_ignore_label(true);
    segmenter_.set_color("holes", cv::Vec3b(config_.holes_h, config_.holes_s,
          config_.holes_v));
  }
  else
  {
    library_.set_ignore_label(false);
    segmenter_.set_color("red", cv::Vec3b(config_.red_h, config_.red_s,
          config_.red_v));
    segmenter_.set_color("blue", cv::Vec3b(config_.blue_h, config_.blue_s,
          config_.blue_v));
    segmenter_.set_color("green", cv::Vec3b(config_.green_h, config_.green_s,
          config_.green_v));
    segmenter_.set_color("yellow", cv::Vec3b(config_.yellow_h, config_.yellow_s,
          config_.yellow_v));
  }
  
  this->unlock();
}

// ItmPieceDetectorAlgorithm Public API
