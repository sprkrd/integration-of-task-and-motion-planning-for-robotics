#include "itm_piece_detector_alg_node.h"

ItmPieceDetectorAlgNode::ItmPieceDetectorAlgNode(void) :
  algorithm_base::IriBaseAlgorithm<ItmPieceDetectorAlgorithm>(),
  observe_scene_aserver_(public_node_handle_, "observe_scene")
  //feedback_camera_manager(ros::NodeHandle("~feedback")),
  //it(this->public_node_handle_)
{
  //init class attributes if necessary
  this->loop_rate_ = 10;//in [Hz]

  // [init publishers]
  //this->feedback_publisher_ = this->it.advertiseCamera("feedback/image_raw", 1);
  this->feedback_publisher_ =
    this->public_node_handle_.advertise<sensor_msgs::Image>(
        "feedback/image_raw", 1);
  // uncomment the following lines to load the calibration file for the camera
  // Change <cal_file_param> for the correct parameter name holding the configuration filename
  //std::string feedback_cal_file;
  //public_node_handle_.param<std::string>("<cal_file_param>",feedback_cal_file,"");
  //if(this->feedback_camera_manager.validateURL(feedback_cal_file))
  //{
  //  if(!this->feedback_camera_manager.loadCameraInfo(feedback_cal_file))
  //    ROS_INFO("Invalid calibration file");
  //}
  //else
  //  ROS_INFO("Invalid calibration file");

  
  // [init subscribers]
  this->points_subscriber_ = this->public_node_handle_.subscribe("points", 1,
      &ItmPieceDetectorAlgNode::points_callback, this);
  pthread_mutex_init(&this->points_mutex_,NULL);

  //this->imgin_subscriber_ = this->it.subscribeCamera("imgin/image_raw", 1, &ItmPieceDetectorAlgNode::imgin_callback, this);
  this->imgin_subscriber_ = this->public_node_handle_.subscribe(
      "imgin/image_raw", 1, &ItmPieceDetectorAlgNode::imgin_callback, this);
  pthread_mutex_init(&this->imgin_mutex_,NULL);

  
  // [init services]
  
  // [init clients]
  
  // [init action servers]
  observe_scene_aserver_.registerStartCallback(boost::bind(&ItmPieceDetectorAlgNode::observe_sceneStartCallback, this, _1));
  observe_scene_aserver_.registerStopCallback(boost::bind(&ItmPieceDetectorAlgNode::observe_sceneStopCallback, this));
  observe_scene_aserver_.registerIsFinishedCallback(boost::bind(&ItmPieceDetectorAlgNode::observe_sceneIsFinishedCallback, this));
  observe_scene_aserver_.registerHasSucceedCallback(boost::bind(&ItmPieceDetectorAlgNode::observe_sceneHasSucceededCallback, this));
  observe_scene_aserver_.registerGetResultCallback(boost::bind(&ItmPieceDetectorAlgNode::observe_sceneGetResultCallback, this, _1));
  observe_scene_aserver_.registerGetFeedbackCallback(boost::bind(&ItmPieceDetectorAlgNode::observe_sceneGetFeedbackCallback, this, _1));
  observe_scene_aserver_.start();
  frame_count_ = 0;
  pc_count_ = 0;
  this->observe_scene_active_=false;
  this->observe_scene_succeeded_=false;
  this->observe_scene_finished_=false;
  
  // [init action clients]
}

ItmPieceDetectorAlgNode::~ItmPieceDetectorAlgNode(void)
{
  // [free dynamic memory]
  pthread_mutex_destroy(&this->points_mutex_);
  pthread_mutex_destroy(&this->imgin_mutex_);
}

void ItmPieceDetectorAlgNode::mainNodeThread(void)
{
  // [fill msg structures]
  // Initialize the topic message structure
  //this->feedback_Image_msg_.data = my_var;

  // Uncomment the following lines two initialize the camera info structure
  //sensor_msgs::CameraInfo feedback_camera_info=this->feedback_camera_manager.getCameraInfo();
  //feedback_camera_info.header.stamp = <time_stamp>;
  //feedback_camera_info.header.frame_id = <frame_id>;

  
  // [fill srv structure and make request to the server]
  
  // [fill action structure and make request to the action server]
  // To finish the action server with success
  //this->observe_scene_succeeded_=true;
  //this->observe_scene_finished_=true;
  // To finish the action server with failure
  //this->observe_scene_succeeded_=false;
  //this->observe_scene_finished_=true;

  // IMPORTANT: it is better to use the boolean variables to control the
  // behavior of the action server instead of direclty calling the action server
  // class functions.
  
  // precheck if action is active before locking the algorithm unnecessarily
  if (observe_scene_active_)
  {
    alg_.lock();
    // now that the algorithm is locked, better check the "active" flag again
    // just in case
    observe_scene_finished_ = frame_count_ == acc_images_.size() and
      pc_count_ == acc_pc_.size();
    if (observe_scene_active_ and observe_scene_finished_)
    {
      observe_scene_succeeded_ = alg_.segmentate(acc_images_, acc_pc_) == 0;
      observe_scene_active_ = false;
    }

    alg_.unlock();
  }


  // [publish messages]
  // Uncomment the following line to convert an OpenCV image to a ROS image message
  //this->feedback_Image_msg_=*this->cv_image_->toImageMsg();
  // Uncomment the following line to publish the image together with the camera information
  //this->feedback_publisher_.publish(this->feedback_Image_msg_,feedback_camera_info);

}

/*  [subscriber callbacks] */
void ItmPieceDetectorAlgNode::points_callback(const PointCloud::ConstPtr& msg)
{
  //ROS_INFO("ItmPieceDetectorAlgNode::points_callback: New Message Received");

  //use appropiate mutex to shared variables if necessary
  this->alg_.lock();
  //this->points_mutex_enter();
  if (observe_scene_active_ and pc_count_ < acc_pc_.size() and frame_count_ > pc_count_)
  {
    acc_pc_[pc_count_++] = msg;
  }
  //std::cout << msg->data << std::endl;
  //unlock previously blocked shared variables
  this->alg_.unlock();
  //this->points_mutex_exit();
}

void ItmPieceDetectorAlgNode::points_mutex_enter(void)
{
  pthread_mutex_lock(&this->points_mutex_);
}

void ItmPieceDetectorAlgNode::points_mutex_exit(void)
{
  pthread_mutex_unlock(&this->points_mutex_);
}

//void ItmPieceDetectorAlgNode::imgin_callback(
    //const sensor_msgs::Image::ConstPtr& msg,
    //const sensor_msgs::CameraInfoConstPtr& info)
void ItmPieceDetectorAlgNode::imgin_callback(
    const sensor_msgs::Image::ConstPtr& msg)
{
  //ROS_INFO("ItmPieceDetectorAlgNode::imgin_callback: New Message Received");

  //use appropiate mutex to shared variables if necessary
  this->alg_.lock();
  //this->imgin_mutex_enter();
  //std::cout << msg->data << std::endl;
  // Uncomment the following line to convert the input image to OpenCV format
  this->cv_image_ = cv_bridge::toCvShare(msg, "bgr8");

  // Compute and publish feedback if there are subscribers to the feedback topic
  if (feedback_publisher_.getNumSubscribers() > 0)
  {
    cv::Mat feedback = alg_.segmentate(cv_image_->image);
    cv_bridge::CvImage cv_fb(cv_image_->header, cv_image_->encoding, feedback);
    //feedback_publisher_.publish(cv_fb.toImageMsg(), info);
    feedback_publisher_.publish(cv_fb.toImageMsg());
  }

  if (observe_scene_active_ and frame_count_ < acc_images_.size())
  {
    acc_images_[frame_count_++] = cv_image_->image;
  }

  //unlock previously blocked shared variables
  this->alg_.unlock();
  //this->imgin_mutex_exit();
}

void ItmPieceDetectorAlgNode::imgin_mutex_enter(void)
{
  pthread_mutex_lock(&this->imgin_mutex_);
}

void ItmPieceDetectorAlgNode::imgin_mutex_exit(void)
{
  pthread_mutex_unlock(&this->imgin_mutex_);
}


/*  [service callbacks] */

/*  [action callbacks] */
void ItmPieceDetectorAlgNode::observe_sceneStartCallback(const iri_itm_msgs::ObserveSceneGoalConstPtr& goal)
{
  this->alg_.lock();
  //check goal
  frame_count_ = 0;
  pc_count_ = 0;
  acc_images_.resize(goal->nframes);
  acc_pc_.resize(goal->nframes);
  this->observe_scene_active_=true;
  this->observe_scene_succeeded_=false;
  this->observe_scene_finished_=false;

  //execute goal
  this->alg_.unlock();
}

void ItmPieceDetectorAlgNode::observe_sceneStopCallback(void)
{
  this->alg_.lock();
  //stop action
  this->observe_scene_active_=false;
  this->alg_.unlock();
}

bool ItmPieceDetectorAlgNode::observe_sceneIsFinishedCallback(void)
{
  bool ret = false;

  this->alg_.lock();
  //if action has finish for any reason
  ret = this->observe_scene_finished_;
  this->alg_.unlock();

  return ret;
}

bool ItmPieceDetectorAlgNode::observe_sceneHasSucceededCallback(void)
{
  bool ret = false;

  this->alg_.lock();
  //if goal was accomplished
  ret = this->observe_scene_succeeded_;
  this->observe_scene_active_=false; // no need?
  this->alg_.unlock();

  return ret;
}

void ItmPieceDetectorAlgNode::observe_sceneGetResultCallback(
    iri_itm_msgs::ObserveSceneResultPtr& result)
{
  this->alg_.lock();

  if (alg_.get_number_of_shapes())
  {
    alg_.get_shape_names(result->shape_names);
    alg_.get_avg_similitudes(result->avg_similitudes);
    alg_.get_avg_angles(result->avg_angles);
    alg_.get_avg_pca_orientations(result->avg_pca_orientations);
    std::vector<pcl::PointXYZ> centroids;
    alg_.get_centroids(centroids);
    result->centroids.resize(centroids.size());
    for (int i = 0; i < centroids.size(); ++i)
    {
      result->centroids[i].x = centroids[i].x;
      result->centroids[i].y = centroids[i].y;
      result->centroids[i].z = centroids[i].z;
    }
    //update result data to be sent to client
    //result->data = data;
  }

  this->alg_.unlock();
}

void ItmPieceDetectorAlgNode::observe_sceneGetFeedbackCallback(iri_itm_msgs::ObserveSceneFeedbackPtr& feedback)
{
  this->alg_.lock();
  feedback->frame = frame_count_;
  feedback->pointcloud = pc_count_;
  //update feedback data to be sent to client
  //ROS_INFO("feedback: %s", feedback->data.c_str());
  this->alg_.unlock();
}


/*  [action requests] */

void ItmPieceDetectorAlgNode::node_config_update(Config &config, uint32_t level)
{
  this->alg_.lock();
  this->config_=config;
  this->alg_.unlock();
}

void ItmPieceDetectorAlgNode::addNodeDiagnostics(void)
{
}

/* main function */
int main(int argc,char *argv[])
{
  return algorithm_base::main<ItmPieceDetectorAlgNode>(argc, argv, "itm_piece_detector_alg_node");
}
