#!/usr/bin/env python

import rospy
import numpy as np
import cv2

from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError

bridge = CvBridge()

class DepthConverter:

    def __init__(self):
        rospy.init_node('extract_depth')
        self.bridge = CvBridge()
        rospy.Subscriber('/camera/depth_registered/image_raw', Image, self.imgin_cb)
        self.a = 1.000000
        self.b = 0.600000
        self.pub = rospy.Publisher('/camera/depth_registered/image_mono8', Image, queue_size=3)

    def imgin_cb(self, in_msg):
        in_cv = self.bridge.imgmsg_to_cv2(in_msg, "passthrough")
        # assert in_cv.dtype == np.float32
        if self.b is None:
            self.b = np.min(in_cv)
        if self.a is None:
            self.a = np.max(in_cv) - self.b
        out_cv_f = np.round(255.0*(in_cv-self.b)/self.a)
        out_cv = np.array(out_cv_f, dtype=np.uint8)
        out_msg = self.bridge.cv2_to_imgmsg(out_cv, "mono8")
        self.pub.publish(out_msg)


if __name__ == '__main__':
    DepthConverter()
    rospy.spin()
