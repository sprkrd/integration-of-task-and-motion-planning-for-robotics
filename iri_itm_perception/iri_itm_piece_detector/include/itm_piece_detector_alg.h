// Copyright (C) 2010-2011 Institut de Robotica i Informatica Industrial, CSIC-UPC.
// Author 
// All rights reserved.
//
// This file is part of iri-ros-pkg
// iri-ros-pkg is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
// 
// IMPORTANT NOTE: This code has been generated through a script from the 
// iri_ros_scripts. Please do NOT delete any comments to guarantee the correctness
// of the scripts. ROS topics can be easly add by using those scripts. Please
// refer to the IRI wiki page for more information:
// http://wikiri.upc.es/index.php/Robotics_Lab

#ifndef _itm_piece_detector_alg_h_
#define _itm_piece_detector_alg_h_

#include <iri_itm_piece_detector/ItmPieceDetectorConfig.h>
#include <pcl/common/common.h>
#include "blobsegmenter.h"

//include itm_piece_detector_alg main library

/**
 * \brief IRI ROS Specific Driver Class
 *
 *
 */
class ItmPieceDetectorAlgorithm
{
  private:
    typedef pcl::PointCloud<pcl::PointXYZ> PointCloud;

    static void extract_sequence(
        const std::vector<iri_blobsegmenter::Matrix>& matrices, int i, int j,
        std::vector<double>& sequence);

    static double average(const std::vector<double>& vector);

    static void average(const std::vector<iri_blobsegmenter::Matrix>& matrices,
        iri_blobsegmenter::Matrix& result);

    static double average_angles(const std::vector<double>& angles,
        double symmetry);

    static void average(const std::vector<std::vector<pcl::PointXYZ> >& vector,
        std::vector<pcl::PointXYZ>&  average);

    void average_angles(const std::vector<iri_blobsegmenter::Matrix>& angles,
        iri_blobsegmenter::Matrix& result);

    iri_blobsegmenter::CBlobSegmenter segmenter_;
    iri_blobsegmenter::CTemplateLibrary library_;
    std::map<std::string, double> symmetry_;

    std::vector<double> avg_pca_orientations_;
    std::vector<pcl::PointXYZ> centroids_;
    iri_blobsegmenter::Matrix avg_angles_;
    iri_blobsegmenter::Matrix avg_similitudes_;

  protected:
   /**
    * \brief define config type
    *
    * Define a Config type with the ItmPieceDetectorConfig. All driver implementations
    * will then use the same variable type Config.
    */
    pthread_mutex_t access_;    

    // private attributes and methods

  public:
   /**
    * \brief define config type
    *
    * Define a Config type with the ItmPieceDetectorConfig. All driver implementations
    * will then use the same variable type Config.
    */
    typedef iri_itm_piece_detector::ItmPieceDetectorConfig Config;

   /**
    * \brief config variable
    *
    * This variable has all the driver parameters defined in the cfg config file.
    * Is updated everytime function config_update() is called.
    */
    Config config_;

   /**
    * \brief constructor
    *
    * In this constructor parameters related to the specific driver can be
    * initalized. Those parameters can be also set in the openDriver() function.
    * Attributes from the main node driver class IriBaseDriver such as loop_rate,
    * may be also overload here.
    */
    ItmPieceDetectorAlgorithm(void);

    cv::Mat segmentate(const cv::Mat& img);

    int segmentate(const std::vector<cv::Mat>& images,
        const std::vector<PointCloud::ConstPtr>& pointclouds);
 
    void get_centroids(std::vector<cv::Point>& centroids);

    inline int get_number_of_shapes() const
    {
      return segmenter_.get_contours().size();
    }

    inline void get_shape_names(std::vector<std::string>& shape_names)
    {
      library_.get_names(shape_names);
    }

    inline void get_avg_pca_orientations(std::vector<double>& avg_pca_orientations)
    {
      avg_pca_orientations = avg_pca_orientations_;
    }

    inline void get_avg_similitudes(std::vector<double>& avg_similitudes)
    {
      iri_blobsegmenter::reshape(avg_similitudes_, avg_similitudes);
    }

    inline void get_avg_angles(std::vector<double>& avg_angles)
    {
      iri_blobsegmenter::reshape(avg_angles_, avg_angles);
    }

    inline void get_centroids(std::vector<pcl::PointXYZ>& centroids)
    {
      centroids = centroids_;
    }

   /**
    * \brief Lock Algorithm
    *
    * Locks access to the Algorithm class
    */
    void lock(void) { pthread_mutex_lock(&this->access_); };

   /**
    * \brief Unlock Algorithm
    *
    * Unlocks access to the Algorithm class
    */
    void unlock(void) { pthread_mutex_unlock(&this->access_); };

   /**
    * \brief Tries Access to Algorithm
    *
    * Tries access to Algorithm
    * 
    * \return true if the lock was adquired, false otherwise
    */
    bool try_enter(void) 
    { 
      if(pthread_mutex_trylock(&this->access_)==0)
        return true;
      else
        return false;
    };

   /**
    * \brief config update
    *
    * In this function the driver parameters must be updated with the input
    * config variable. Then the new configuration state will be stored in the 
    * Config attribute.
    *
    * \param new_cfg the new driver configuration state
    *
    * \param level level in which the update is taken place
    */
    void config_update(Config& config, uint32_t level=0);

    // here define all itm_piece_detector_alg interface methods to retrieve and set
    // the driver parameters

   /**
    * \brief Destructor
    *
    * This destructor is called when the object is about to be destroyed.
    *
    */
    ~ItmPieceDetectorAlgorithm(void);
};

#endif
