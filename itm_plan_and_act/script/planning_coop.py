#!/usr/bin/env python

import itertools
import rospy
import actionlib_msgs
import actionlib
import time
import tf2_geometry_msgs
import numpy as np
import random
import pyhop.itm_operators
import pyhop.itm_methods
from pyhop.itm_state import ItmState
from pyhop.pyhop import pyhop, operators
from utils.mtn_planning import MotionPlanner, transform_list_to_pose_stamped, transform_pose_stamped_to_list
from utils.aclient import observe, get_true_shape
from utils.math_functions import scale_matrix, ml_shape, entropy

# available_shapes = ['star5', 'star6', 'hexagon', 'star8', 'triangle']
# available_shapes = ['hexagon']
available_shapes = ['star5', 'star6', 'hexagon', 'star8', 'triangle', 'cross', 'circularsector', 'pentagon', 'trapezium', 'trapezoid']


# def next_plan(state):
    # if state.holding is not None:
        # # Give priority to the piece that is currently held
        # rospy.logwarn('Planning for the grasped piece: {}'.format(state.holding))
        # _, plan = pyhop(state, [('piece_handled', state.holding)])
        # rospy.logwarn('Current plan: {}'.format(plan))
        # return plan
    # best_plan = None
    # best_blob_idx = None
    # lowest_cost = float('inf')
    # for blob_idx in range(len(state.available_shapes)):
        # final_state, plan = pyhop(state, [('piece_handled', blob_idx)])
        # if final_state.cost < lowest_cost:
            # best_plan = plan
            # best_blob_idx = blob_idx
            # lowest_cost = final_state.cost
    # rospy.logwarn('Next piece to insert: {}'.format(best_blob_idx))
    # rospy.logwarn('Current plan: {}'.format(best_plan))
    # rospy.logwarn('(Estimated) cost of next state: {}s'.format(lowest_cost))
    # return best_plan

# def next_plan(state):
    # if state.holding is not None:
        # # Give priority to the piece that is currently held
        # rospy.logwarn('Planning for the grasped piece: {}'.format(state.holding))
        # _, plan = pyhop(state, [('piece_handled', state.holding)])
        # rospy.logwarn('Current plan: {}'.format(plan))
        # return plan
    # blob_idx = None
    # # min_h = 1.0
    # # for idx in range(len(state.available_shapes)):
        # # h = entropy(state.probabilities[idx,:], normalized=True)
        # # if h < min_h:
            # # blob_idx = idx
            # # min_h = h
    # min_y = float('inf')
    # for idx in range(len(state.available_shapes)):
        # y = state.objects[idx][0][1]
        # if y < min_y:
            # blob_idx = idx
            # min_y = y
    # next_state, plan = pyhop(state, [('piece_handled', blob_idx)])
    # rospy.logwarn('Next piece to insert: {}'.format(blob_idx))
    # rospy.logwarn('Current plan: {}'.format(plan))
    # rospy.logwarn('(Estimated) cost of next state: {}s'.format(next_state.cost))
    # return plan


def next_plan(state, horizon=2):
    if state.holding is not None:
        # Give priority to the piece that is currently held
        rospy.logwarn('Planning for the grasped piece: {}'.format(state.holding))
        _, plan = pyhop(state, [('piece_handled', state.holding)])
        rospy.logwarn('Current plan: {}'.format(plan))
        return plan
    horizon = min(horizon, len(state.available_shapes))
    perms = [(idx,) for idx in range(len(state.available_shapes))]
    for remaining in range(len(state.available_shapes)-1, len(state.available_shapes)-horizon, -1):
        perms = [p + (idx,) for p in perms for idx in range(remaining)]
    print perms
    # perms = itertools.permutations(range(len(state.available_shapes)), min(horizon, len(state.available_shapes)))
    best_plan = None
    best_perm = None
    lowest_cost = float('inf')
    for perm in perms:
        plan = []
        next_state = state
        for idx in perm:
            next_state, plan_ = pyhop(next_state, [('piece_handled', idx)])
            plan += plan_
        if next_state.cost < lowest_cost:
            best_plan = plan
            lowest_cost = next_state.cost
            best_perm = perm
    rospy.logwarn('next piece(s) to insert: {}'.format(best_perm))
    rospy.logwarn('current plan: {}'.format(best_plan))
    rospy.logwarn('Plan cost: {}s'.format(lowest_cost))
    return best_plan


def insert_piece(state=None):
    if not state:
        scene = observe('scene', 3)
        available_shapes_columns = [scene.shape_names.index(shape_n) for shape_n in available_shapes]
        similarities = np.reshape(scene.avg_similitudes, (-1, 10))
        similarities = similarities[:, available_shapes_columns]
        assert similarities.shape[0] == similarities.shape[1]
        probabilities = scale_matrix(similarities.copy())
        objects = []
        for centroid, theta in zip(scene.centroids, scene.avg_pca_orientations):
            centroid_s = transform_list_to_pose_stamped(
                    [centroid.x, centroid.y, centroid.z, 0, 0, theta],
                    'camera_rgb_optical_frame')
            centroid_w = mtn._make_parallel_to_xy_plane(centroid_s)
            centroid_w.pose.position.z -= 15e-3
            objects.append((transform_pose_stamped_to_list(centroid_w), 'world'))
        state = ItmState('', joints={}, cost=0.0, tool_status="closed",
                      holding=None,
                      at={robot: ('unknown',) for robot in ('picker', 'catcher')},
                      objects=objects, probabilities=probabilities,
                      similarities=similarities,
                      available_shapes=available_shapes,
                      mistakes=0, replan=0, plan_time=0, replan_time = 0)
    # start = time.time()
    # plan = next_plan(state, horizon=5)
    # elapsed = time.time() - start
    # rospy.logwarn('Elapsed: {}s'.format(elapsed))
    # return state
    if not state.available_shapes:
        state = operators['move_j_both'](state, ('picker', 'home'), ('catcher', 'home'), do=True)
        return state
    print state.similarities
    print state.probabilities

    replan_time = 0.0
    plan_time = 0.0
    replan = 0
    piece_handled = False
    while not piece_handled:
        tic = time.time()
        plan = next_plan(state)
        toc = time.time() - tic
        if replan > 0: replan_time += toc
        plan_time += toc
        piece_handled = True
        for action in plan:
            action_n = action[0]
            action_args = action[1:]
            rospy.loginfo("Now executing {}{}".format(action_n, action_args))
            true_shape = get_true_shape()
            next_state = operators[action_n](state, *action_args, do=True)
            if action_n == 'close_look':
                print state.similarities
                print state.probabilities
            if next_state is False and action_n == 'close_look':
                if replan >= 2: continue
                piece_handled = False
                rospy.logwarn("Replanning...")
                replan += 1
                break
            elif next_state is False: assert False
            else:
                # print state.similarities
                # print state.probabilities
                state = next_state
                if action_n == 'insert' and true_shape != action_args[1]:
                    rospy.logwarn('Made a mistake! Guess: {}; Truth: {}'.format(action_args[1], true_shape))
                    state.mistakes += 1
    state.plan_time += plan_time
    state.replan += replan
    state.replan_time += replan_time
    rospy.logwarn('Total time until now (sim time): {}s'.format(state.cost))
    rospy.logwarn('Total planning time until now: {}s'.format(state.plan_time))
    rospy.logwarn('Total re-planning time until now: {}s'.format(state.replan_time))
    rospy.logwarn('Total #replan until now: {}'.format(state.replan))
    rospy.logwarn('Total #mistakes until now: {}'.format(state.mistakes))
    return insert_piece(state)


if __name__ == '__main__':
    rospy.init_node("planning_simple_test")
    mtn = MotionPlanner.instance()
    mtn.go_home('both')
    random.seed(42)
    state = insert_piece()
    rospy.loginfo('Elapsed since begining (sim time): {}s'.format(state.cost))
    rospy.loginfo('Elapsed planning: {}s'.format(state.plan_time))
    rospy.logwarn('Elapsed re-planning: {}s'.format(state.replan_time))
    rospy.loginfo('#Replans: {}'.format(state.replan))
    rospy.loginfo('#Mistakes: {}'.format(state.mistakes))

