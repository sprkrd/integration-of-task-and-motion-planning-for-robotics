import numpy as np
import roslib
import rospy
import actionlib_msgs
import actionlib

from geometry_msgs.msg import Point, PointStamped
from iri_itm_msgs.msg import ObserveSceneGoal, ObserveSceneAction

tau = 2*np.pi
deg2rad = np.pi / 180
rad2deg = 180 / np.pi

sigma0_xyz = 0.0025
epsilon = 0.05
delta_xyz = 0.05
delta_theta = 5.0

available_shapes = ['star5',
                    'star6',
                    'star8',
                    'triangle',
                    'hexagon',
                    'cross',
                    'circularsector',
                    'trapezium',
                    'trapezoid',
                    'pentagon']

theta_max = {'pentagon':72.0,
             'trapezium':360.0,
             'trapezoid':360.0,
             'triangle':120.0,
             'hexagon':60.0,
             'star8':45.0,
             'cross':90.0,
             'circularsector':360.0,
             'star5':72.0,
             'star6':60.0}

def remove_shape(shape):
    available_shapes.remove(shape)

def average_angle(angles, symmetry=360.0):
    theta_x = np.sum(np.cos(tau*(angles/symmetry)))
    theta_y = np.sum(np.sin(tau*(angles/symmetry)))
    return np.arctan2(theta_y, theta_x)/tau * symmetry

def average_point(points):
    r_points = map(lambda point: point.point, points)
    points_np = np.array(map(lambda point: [point.x, point.y, point.z], r_points))
    avg_point_np = np.mean(points_np, 0)
    avg_point = PointStamped()
    avg_point.header.frame_id = points[0].header.frame_id
    avg_point.point.x = avg_point_np[0]
    avg_point.point.y = avg_point_np[1]
    avg_point.point.z = avg_point_np[2]
    return avg_point

def get_orientation_matrix(pieces):
    M = len(pieces)
    N = len(available_shapes)
    mat = np.zeros((M,N))
    for idx, piece in enumerate(pieces):
        for match in piece.matches:
            jdx = available_shapes.index(match.shape)
            mat[idx, jdx] = match.optimum_angle
    return mat

def get_similitude_matrix(pieces):
    M = len(pieces)
    N = len(available_shapes)
    mat = np.zeros((M,N))
    for idx, piece in enumerate(pieces):
        for match in piece.matches:
            jdx = available_shapes.index(match.shape)
            mat[idx, jdx] = match.similitude
    return mat

def most_likely_shape(similitudes, piece_idx):
    return available_shapes(np.argmax(similitudes[piece_idx,:]))

def scale_matrix(mat, epsilon=1E-5, maxiter=100):
  """ Scales the given np matrix *in-place* so it is doubly stochastic. It
  returns the same matrix.
  The parameters are:
    - mat: input matrix
    - epsilon: maximum tolerable error. The error is computed as the
      maximum absolute value of the difference of the rows/columns sum and 1.
  """
  t_mat = mat.transpose()
  error_rows = max([abs(sum(row)-1.0) for row in mat])
  error_cols = max([abs(sum(col)-1.0) for col in t_mat])
  error = max(error_rows,error_cols)
  it = 0
  while error > epsilon and it < maxiter:
    for row in mat:
      inv_sum = 1.0/sum(row)
      row *= inv_sum
    for col in t_mat:
      inv_sum = 1.0/sum(col)
      col *= inv_sum
    error = max([abs(sum(row)-1.0) for row in mat])
    it += 1
  return mat

def observe_near_client(nframes=5):
    client = actionlib.SimpleActionClient('~observe_cavity', ObserveSceneAction)
    rospy.loginfo("waiting for server...")
    client.wait_for_server()
    rospy.loginfo("sending goal...")
    goal = ObserveSceneGoal(nframes)
    rospy.loginfo("waiting response...")
    client.send_goal_and_wait(goal)
    # if client.get_state() != actionlib_msgs.msg.GoalStatus.SUCCEEDED:
        # print "Call to action detect_pieces failed"
            # assert False
    return client.get_result()

def observe_cavity_client(nframes=5):
    client = actionlib.SimpleActionClient('~observe_cavity', ObserveSceneAction)
    rospy.loginfo("waiting for server...")
    client.wait_for_server()
    rospy.loginfo("sending goal...")
    goal = ObserveSceneGoal(nframes)
    rospy.loginfo("waiting response...")
    client.send_goal_and_wait(goal)
    # if client.get_state() != actionlib_msgs.msg.GoalStatus.SUCCEEDED:
        # print "Call to action detect_pieces failed"
            # assert False
    return client.get_result()

def observe_table_client(nframes=5):
    client = actionlib.SimpleActionClient('~observe_scene', ObserveSceneAction)
    rospy.loginfo("waiting for server...")
    client.wait_for_server()
    rospy.loginfo("sending goal...")
    goal = ObserveSceneGoal(nframes)
    rospy.loginfo("waiting response...")
    client.send_goal_and_wait(goal)
    # if client.get_state() != actionlib_msgs.msg.GoalStatus.SUCCEEDED:
        # print "Call to action detect_pieces failed"
            # assert False
    return client.get_result()
