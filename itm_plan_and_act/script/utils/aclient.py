import roslib
import rospy
import actionlib_msgs
import actionlib

from geometry_msgs.msg import Point, PointStamped
from iri_itm_msgs.msg import ObserveSceneGoal, ObserveSceneAction

def observe(what='scene', nframes=5):
    client = actionlib.SimpleActionClient('/segmenter_{}/observe_scene'.format(what), ObserveSceneAction)
    rospy.loginfo("waiting for server...")
    client.wait_for_server()
    rospy.loginfo("sending goal...")
    goal = ObserveSceneGoal(nframes)
    rospy.loginfo("waiting response...")
    client.send_goal_and_wait(goal)
    # if client.get_state() != actionlib_msgs.msg.GoalStatus.SUCCEEDED:
        # print "Call to action detect_pieces failed"
            # assert False
    return client.get_result()

def get_true_shape():
    from gz_gripper_plugin.srv import CheckGrasped
    check_grasped = rospy.ServiceProxy('check_grasped', CheckGrasped)
    resp = check_grasped()
    grasped = resp.grasped.split('::')[0].replace('_', '')
    return grasped

