#!/usr/bin/python

# Motion planning and execution goodies

import sys
import time
import copy
import threading

import rospy
import moveit_commander
import tf2_ros
import tf2_geometry_msgs
import actionlib

import numpy as np

from tf.transformations import quaternion_from_euler, quaternion_matrix, quaternion_from_matrix, euler_from_quaternion

from moveit_msgs.srv import GetPositionIK, GetPositionFK, GetPositionFKRequest

from iri_common_drivers_msgs.msg import tool_openAction, tool_openGoal, tool_closeAction, tool_closeGoal

from geometry_msgs.msg  import Point, Pose, PoseStamped, PointStamped
from shape_msgs.msg     import SolidPrimitive
from moveit_msgs.msg    import PositionIKRequest, MoveGroupActionResult, MoveItErrorCodes, CollisionObject

class MtnException(Exception):
    """
    Custom exception that describes errors raised by methods from this module.
    Each MtnException contains:
        - msg: error message
        - reason: simple string for multiplexing error handling behaviours.
        This is optional and it is set to the empty string ("") by default.
    """
    def __init__(self, msg, reason=""):
        self.msg = msg
        self.reason = reason

    def __str__(self):
        return "{msg}: {reason}".format(msg=self.msg, reason=self.reason) if self.reason else self.msg

def plan_time(plan):
    return plan.joint_trajectory.points[-1].time_from_start.to_sec()

def plan_end_joints(plan):
    joint_n = plan.joint_trajectory.joint_names
    joint_v = plan.joint_trajectory.points[-1].positions
    return dict(zip(joint_n, joint_v))

def distance_angles(ang1, ang2):
    """
    Gives the shortest distance between two angles
    """
    diff = abs(ang1 - ang2) % (2*np.pi)
    if diff > np.pi: diff = 2*np.pi - diff
    return diff

def distance_joint_space(conf1, conf2):
    """
    Computes the sum of the absolute differences between the joints of two
    different robot configurations. Both configurations have to have the same
    joints. Configurations are passed as dictionaries whose keys are the
    joint names and the values the joint angle. The method assumes that
    the maximum difference between two angles is 2*pi (e.g. angles between
    -pi and pi or between 0 and 2*pi)
    """
    # Check that both configurations have the same joints
    if set(conf1) != set(conf2):
        raise MtnException("conf1 and conf2 do not have the same joints")
    distance = 0
    for j in conf1:
        distance += distance_angles(conf1[j], conf2[j])
    return distance

def transform_list_to_pose_stamped(pose_t, frame_id='', stamp=rospy.Time()):
    """
    Convenience method for converting a tuple or list into a PoseStamped
    message. The tuple can be in (x, y, z, R, P, Y) or
    (x, y, z, qx, qy, qz, qw) format (i.e. rotation as RPY or directly as
    a quaternion)
    """
    if not isinstance(pose_t, (tuple, list)):
        raise MtnException("Invalid type: {} (must be tuple or list)".format(
            type(pose_t)))
    elif len(pose_t) not in (6,7):
        raise MtnException("Invalid length: {} (must be 6 or 7)".format(
            len(pose_t)))

    pose_s = PoseStamped()
    pose_s.header.frame_id = frame_id
    pose_s.header.stamp = stamp
    pose_s.pose.position.x = pose_t[0]
    pose_s.pose.position.y = pose_t[1]
    pose_s.pose.position.z = pose_t[2]

    if len(pose_t) == 6:
        # RPY
        q = quaternion_from_euler(pose_t[3], pose_t[4], pose_t[5], axes='rxyz')
        pose_s.pose.orientation.x = q[0]
        pose_s.pose.orientation.y = q[1]
        pose_s.pose.orientation.z = q[2]
        pose_s.pose.orientation.w = q[3]
    else:
        # Quaternion
        pose_s.pose.orientation.x = pose_t[3]
        pose_s.pose.orientation.y = pose_t[4]
        pose_s.pose.orientation.z = pose_t[5]
        pose_s.pose.orientation.w = pose_t[6]

    return pose_s

def transform_pose_stamped_to_list(pose_s):
    """
    Convenience method for converting a PoseStamped into a list
    (x, y, z, R, P, Y).
    """
    pose_t = [pose_s.pose.position.x,
              pose_s.pose.position.y,
              pose_s.pose.position.z,
              0.0, 0.0, 0.0]
    q = [pose_s.pose.orientation.x,
         pose_s.pose.orientation.y,
         pose_s.pose.orientation.z,
         pose_s.pose.orientation.w]
    pose_t[3:] = euler_from_quaternion(q, axes='rxyz')
    return pose_t
 
def build_pose_stamped_from_point_and_yaw(centroid, yaw, frame_id):
    return transform_list_to_pose_stamped(
            [centroid.x, centroid.y, centroid.z, 0, 0, yaw], frame_id)

def reproject_into_feasible_space(joints_in):
    """
    Sometimes the IK service would return solutions that violate the limits.
    This method forces the joint values back into the feasible space.
    """
    min_v = [-2.60, -1.94, -2.73, -0.88, -4.74, -1.54, -2.98]
    max_v = [2.60, 1.94, 2.73, 3.08, 1.22, 1.54, 2.98]
    joints_out = joints_in.copy()
    for j_name, j_value in joints_out.items():
        idx = int(j_name.split('_')[-1]) - 1 # [0]_[1]_[2]_[3]_[4], e.g. iri_wam_catcher_joint_1
        joints_out[j_name] = min(max(min_v[idx], j_value), max_v[idx])
    return joints_out

def is_plan_valid(plan):
    l1 = len(plan.joint_trajectory.points)
    l2 = len(plan.joint_trajectory.joint_names)
    return l1 > 0 and l2 > 0

deg2rad = np.pi/180.0
sphere_pitch_angle = [0, deg2rad*60.0, deg2rad*135.0]
sphere_yaw_offset = deg2rad*180.0
sphere_yaw_inc = deg2rad*72.0

cavity_indices = {'star5': (0, 0),
        'hexagon': (1, 0),
        'ellipse': (1, 1),
        'trapezoid': (1, 2),
        'triangle': (1, 3),
        'trapezium': (1, 4),
        'star8': (2, 0),
        'cross': (2, 1),
        'star6': (2, 2),
        'pentagon': (2, 3),
        'circularsector': (2, 4),
}

def show_cavity_target(cavity):
    row, col = cavity_indices[cavity]
    pitch = sphere_pitch_angle[row]
    yaw = sphere_yaw_offset + sphere_yaw_inc*col
    return transform_list_to_pose_stamped((0.7, 0.1, 0.0, 0.0, pitch, yaw), 'iri_wam_catcher_link_base')

def get_trash_target():
    return transform_list_to_pose_stamped([0.093897, 0.668034, 0.8, 0, np.pi, 0], "world")

def get_home_joints(group):
    joint_v = [0, 0.10, 0, 2.90, 0, 0, 0]
    if group == 'both':
        joint_n = ['iri_wam_{}_joint_{}'.format(group_, idx) for group_ in ('picker', 'catcher') for idx in range(1,8)]
        joints = dict(zip(joint_n, joint_v*2))
    else:
        joint_n = ['iri_wam_{}_joint_{}'.format(group, idx) for idx in range(1, 8)]
        joints = dict(zip(joint_n, joint_v))
    return joints

def get_show_gripper_to_camera_target():
    return transform_list_to_pose_stamped((0, 0, 0.35, 0, np.pi, 0), "camera_rgb_optical_frame")

class MotionPlanner:

    E_CODE_STR = {code: error
            for error, code in vars(MoveItErrorCodes).items()
            if not error.startswith('_') if isinstance(code, int)}

    PLANNERS = ['RRTConnectkConfigDefault',
                'PRMkConfigDefault',
                'KPIECEkConfigDefault',
                'SBLkConfigDefault',
                'ESTkConfigDefault']

    __instance = None

    @staticmethod
    def instance():
        if not MotionPlanner.__instance:
            MotionPlanner.__instance = MotionPlanner()
        return MotionPlanner.__instance

    def __init__(self):
        moveit_commander.roscpp_initialize(sys.argv)
        self._robot = moveit_commander.RobotCommander()
        self._tf_buffer = tf2_ros.Buffer()
        self._tf_listener = tf2_ros.TransformListener(self._tf_buffer)
        self._is_tool_open = False
        self._groups = {}
        self._groups = {'picker': self._robot.get_group('picker'),
                        'catcher': self._robot.get_group('catcher'),
                        'both': self._robot.get_group('both')}
        self._add_table()
        # Gripper is NC (Normally closed), so make sure it starts closed!!
        self._close_tool()

    def _add_table(self):
        scene = moveit_commander.PlanningSceneInterface()
        table_pose = transform_list_to_pose_stamped([1.03, 0.15, 0.36, 0, 0, 0], 'world')
        time.sleep(5.0) # Needed... otherwise the object won't be sent
        scene.add_box('table', table_pose, (1.29, 1.19, 0.72))

    def _get_parent_pose(self, pose_s, child, parent):
        t_1_2 = self._tf_buffer.lookup_transform(child, parent, rospy.Time(), rospy.Duration(5))
        q_1_2 = [t_1_2.transform.rotation.x,
                 t_1_2.transform.rotation.y,
                 t_1_2.transform.rotation.z,
                 t_1_2.transform.rotation.w]

        m_1_2 = quaternion_matrix(q_1_2)
        m_1_2[0,3] = t_1_2.transform.translation.x
        m_1_2[1,3] = t_1_2.transform.translation.y
        m_1_2[2,3] = t_1_2.transform.translation.z
        m_1_2[3,3] = 1.0

        q_o_1 = [pose_s.pose.orientation.x,
                 pose_s.pose.orientation.y,
                 pose_s.pose.orientation.z,
                 pose_s.pose.orientation.w]
        m_o_1 = quaternion_matrix(q_o_1)
        m_o_1[0,3] = pose_s.pose.position.x
        m_o_1[1,3] = pose_s.pose.position.y
        m_o_1[2,3] = pose_s.pose.position.z
        m_o_1[3,3] = 1.0

        m_o_2 = np.matmul(m_o_1, m_1_2)
        q_o_2 = quaternion_from_matrix(m_o_2)

        parent_s = PoseStamped()
        parent_s.header.frame_id = pose_s.header.frame_id
        parent_s.pose.position.x = m_o_2[0,3]
        parent_s.pose.position.y = m_o_2[1,3]
        parent_s.pose.position.z = m_o_2[2,3]
        parent_s.pose.orientation.x = q_o_2[0]
        parent_s.pose.orientation.y = q_o_2[1]
        parent_s.pose.orientation.z = q_o_2[2]
        parent_s.pose.orientation.w = q_o_2[3]

        return parent_s

    def _open_tool(self):
        """
        Open tool action. Invokes the open_tool action and waits until its
        completion.
        """
        open_tool_client = actionlib.SimpleActionClient("/iri_wam_picker/tool_open_action", tool_openAction)
        open_tool_client.wait_for_server(rospy.Duration(100))
        open_tool_client.send_goal(tool_closeGoal())
        open_tool_client.wait_for_result(rospy.Duration.from_sec(5.0))
        self._is_tool_open = True

    def _close_tool(self):
        """
        Close tool action. Invokes the close_tool action and waits until its
        completion.
        """
        close_tool_client = actionlib.SimpleActionClient("/iri_wam_picker/tool_close_action", tool_closeAction)
        close_tool_client.wait_for_server(rospy.Duration(100))
        close_tool_client.send_goal(tool_openGoal())
        close_tool_client.wait_for_result(rospy.Duration.from_sec(5.0))
        self._is_tool_open = False

    def _try_planners(self, group):
        """
        Tries several motion planners for planning to a certain (joint) target,
        until one of them works or until there are no more planners to be
        tested.
        """
        group_ = self._groups[group]
        for planner in MotionPlanner.PLANNERS:
            group_.set_planner_id(planner)
            plan = group_.plan()
            if is_plan_valid(plan):
                return plan
        raise MtnException("Could not find plan")

    def _try_planners_go(self, group):
        """
        Tries several motion planners for going to a certain (joint) target,
        until one of them works or until there are no more planners to be
        tested.
        """
        group_ = self._groups[group]
        for planner in MotionPlanner.PLANNERS:
            group_.set_planner_id(planner)
            group_.set_goal_joint_tolerance(0.02)
            # sometimes the Gazebo controller gives problems regarding the
            # the tolerances. If the execution fails, check if the joints
            # are within some (more generous) tolerance from the target,
            # without calling the planner again.
            ok = group_.go()
            error = self._error_current_target(group)
            if not ok and error < 0.25:
                rospy.logwarn("MoveIt! execution failed! However, error is "
                              "lower than the high tolerance threshold "
                              "({} < 0.25). Moving on...".format(error))
                ok = True
            if ok: return
        raise MtnException("Could not find/execute plan")

    def _error_current_target(self, group="both"):
        """
        Gives the error between the current position of a group and its
        joint target.
        """
        return distance_joint_space(self.get_joints(group=group),
                                    self.get_joints_target(group=group))

    def _make_parallel_to_xy_plane(self, pose_s):
        """
        Approximate the given pose with another one parallel to the ground
        (in the world frame of reference). The transformation preserves the
        position (point) but it alters the rotation (as little as possible)
        so the z axis is oriented TOWARDS the ground (useful for picking
        objects).

        Returns: pose stamped IN WORLD FRAME.
        """

        if pose_s.header.frame_id != 'world':
            pose_s = self._tf_buffer.transform(pose_s, "world", rospy.Duration(5))
        pose_t = transform_pose_stamped_to_list(pose_s)
        if distance_angles(pose_t[3], np.pi) < distance_angles(pose_t[4], np.pi):
            pose_t[3] = np.pi
            pose_t[4] = 0.0
        else:
            pose_t[3] = 0.0
            pose_t[4] = np.pi
        return transform_list_to_pose_stamped(pose_t, "world")

    def get_joints(self, joint_values=None, group="both"):
        """
        Arguments:
            joint_values: update the joints' values with these values.
        Returns: dictionary {joint: value}
        """
        group_ = self._groups[group]
        joints = dict(zip(group_.get_joints(),
                          group_.get_current_joint_values()))
        if joint_values:
            joints.update(joint_values)
        return joints

    def get_joints_target(self, group="both"):
        """
        Gives the target joints of a given group, as a dictionary
        {joint: value}
        """
        group_ = self._groups[group]
        joints = dict(zip(group_.get_joints(),
                          group_.get_joint_value_target()))
        return joints

    def get_state(self, joint_values=None):
        """
        Constructs RobotStateMsg of the robot from a dictionary {joint: value}.
        The non-specified joints take the value of their current
        position. Should joint_values be empty or None, it returns the
        current state of the robot.
        """
        state = self._robot.get_current_state()
        if joint_values:
            joints = dict(zip(state.joint_state.name,
                state.joint_state.position))
            joints.update(joint_values)
            state.joint_state.name = joints.keys()
            state.joint_state.position = joints.values()
        return state

    def get_fk(self, fk_link_names, target_frame='world', joint_values=None):
        try:
            compute_fk = rospy.ServiceProxy('compute_fk', GetPositionFK)
            compute_fk.wait_for_service(timeout=5.0)
            req = GetPositionFKRequest()
            req.fk_link_names = fk_link_names
            req.robot_state = self.get_state(joint_values)
            req.header.frame_id = target_frame
            resp = compute_fk(req)
            error = MotionPlanner.E_CODE_STR[resp.error_code.val]
            if error != 'SUCCESS':
                raise MtnException("FK petition failed", reason=error)
            return resp.pose_stamped
        except rospy.ServiceException as e:
            raise MtnException("Service call failed: {}".format(3))

    def get_ik(self, target, ee, avoid_collisions=True, timeout=2E-2, joint_values=None):
        """
        Computes IK for a target and an end-effector (or a list of targets and
        end effectors). Returns joints as a dictionary.
        Parameters:
            target: a PoseStamped or a list/tuple of PoseStamped.
            ee: the name of an end effector (string) or a list of end effectors
                (one per target).
            group: planning group
            avoid_collisions: whether to use collision aware IK or not
            timeout: timeout for the IK ik_request
            joint_values: initial state of the robot (None to take the current
                state). Although this is seemingly unimportant, it may affect
                the IK solver (solving time and/or solution found).
        Returns: ik joints, as a dictionary
        Raises MtnException if IK is not possible or if the service call
        fails.
        """
        if isinstance(target, (list, tuple)) ^ isinstance(ee, (list, tuple)):
            raise MtnException("Invalid types for target and ee")

        if isinstance(target, (list, tuple)):
            group = 'both'
        else:
            group = ee.split('_')[2] # ee = [0]_[1]_[2]_... (e.g. iri_wam_picker_gripper_tcp)

        req = PositionIKRequest()
        if isinstance(target, (list, tuple)):
            # For some reason, catcher must go first (otherwise the service will
            # fail), so let's /sort/ this out :)
            ee_target = zip(ee, target)
            ee_target.sort()
            ee, target = zip(*ee_target)
            req.ik_link_names = ee
            req.pose_stamped_vector = target
        else:
            req.ik_link_name = ee
            req.pose_stamped = target
        req.group_name = group
        req.robot_state = self.get_state(joint_values)
        req.timeout.secs = timeout
        req.avoid_collisions = avoid_collisions
        try:
            compute_ik = rospy.ServiceProxy('compute_ik', GetPositionIK)
            compute_ik.wait_for_service(timeout=5.0)
            resp = compute_ik(ik_request=req)
            error = MotionPlanner.E_CODE_STR[resp.error_code.val]
            if error != 'SUCCESS':
                raise MtnException("IK petition failed", reason=error)
            joints = dict(zip(resp.solution.joint_state.name,
                              resp.solution.joint_state.position))
            return reproject_into_feasible_space(joints)
        except rospy.ServiceException as e:
            raise MtnException("Service call failed {}".format(e))

    def plan_in_joints(self, end_joints, group, start_joints=None):
        end_joints = self.get_joints(joint_values=end_joints, group=group)
        group_ = self._groups[group]
        group_.set_start_state(self.get_state(start_joints))
        group_.set_joint_value_target(end_joints)
        return self._try_planners(group)

    def plan_to_target(self, target, ee, start_joints=None):
        if isinstance(ee, (list, tuple)):
            group = 'both'
        else:
            group = ee.split('_')[2]
        joints = self.get_ik(target, ee, joint_values=start_joints, avoid_collisions=True)
        return self.plan_in_joints(joints, group, start_joints=start_joints)

    def plan_in_cartesian(self, waypoints, ee, start_joints=None):
        group = self._groups[ee.split('_')[2]]
        parent = "iri_wam_{}_link_7".format(group.get_name())
        frame_id = waypoints[0].header.frame_id
        if not all(map(lambda wp: wp.header.frame_id == frame_id, waypoints)):
            raise MtnException("The frame_id is not the same for all the waypoints")
        if ee != parent:
            waypoints = map(lambda wp: self._get_parent_pose(wp, ee, parent), waypoints)
        waypoints_ = map(lambda wp: wp.pose, waypoints)
        group.set_start_state(self.get_state(start_joints))
        group.set_pose_reference_frame(frame_id)
        group.set_end_effector_link(ee)
        for planner in MotionPlanner.PLANNERS:
            group.set_planner_id(planner)
            trajectory, fraction = group.compute_cartesian_path(waypoints_, 0.005, 0.0)
            if is_plan_valid(trajectory) and fraction > 0.5:
                return trajectory
        else:
            raise MtnException("Could not find cartesian path")

    def move_in_joints(self, end_joints, group):
        end_joints = self.get_joints(joint_values=end_joints, group=group)
        group_ = self._groups[group]
        group_.set_joint_value_target(end_joints)
        return self._try_planners_go(group)

    def move_to_target(self, target, ee):
        if isinstance(ee, (list, tuple)):
            group = 'both'
        else:
            group = ee.split('_')[2]
        joints = self.get_ik(target, ee, avoid_collisions=True)
        return self.move_in_joints(joints, group)

    def move_in_cartesian(self, waypoints, ee):
        group = self._groups[ee.split('_')[2]]
        trajectory = self.plan_in_cartesian(waypoints, ee)
        group.execute(trajectory)

    def pick(self, target):
        target_w = self._make_parallel_to_xy_plane(target)
        # if target.header.frame_id == 'world':
            # target_w = target
        # else:
            # target_w = self._tf_buffer.transform(target, "world", timeout=rospy.Duration(5.0))
        pregrasp = copy.deepcopy(target_w)
        pregrasp.pose.position.z += 0.1
        self.move_to_target(pregrasp, "iri_wam_picker_gripper_tcp")
        self._open_tool()
        self.move_in_cartesian([target_w], "iri_wam_picker_gripper_tcp")
        self._close_tool()
        self.move_in_cartesian([pregrasp], "iri_wam_picker_gripper_tcp")

    def place(self, target):
        target_w = self._make_parallel_to_xy_plane(target)
        # if target.header.frame_id == 'world':
            # target_w = target
        # else:
            # target_w = self._tf_buffer.transform(target, "world", timeout=rospy.Duration(5.0))
        preungrasp = copy.deepcopy(target_w)
        preungrasp.pose.position.z += 0.1
        self.move_to_target(preungrasp, "iri_wam_picker_gripper_tcp")
        self.move_in_cartesian([target_w], "iri_wam_picker_gripper_tcp")
        self._open_tool()
        self.move_in_cartesian([preungrasp], "iri_wam_picker_gripper_tcp")
        self._close_tool()

    def show_cavity(self, cavity):
        target = show_cavity_target(cavity)
        self.move_to_target(target, "iri_wam_catcher_sphere_link")

    def show_gripper_to_camera(self):
        target = get_show_gripper_to_camera_target()
        self.move_to_target(target, "iri_wam_picker_gripper_tcp")

    def go_home(self, group):
        joints = get_home_joints(group)
        self.move_in_joints(joints, group)

