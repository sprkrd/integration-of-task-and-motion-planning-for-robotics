#!/usr/bin/python

from pyhop.itm_state import *
from math import pi

state = ItmState("my-state", objects=[
    ([0.812, 0.376, 0.73, 0, pi, 0], "world"),
    ([0.7, 0.3, 0.73, 0, pi, 0], "world")])

print(state.get_pose("pre-grasp", 0))

print(state.get_pose("pre-grasp", 1))

print(state.get_pose("pre-ungrasp", "sphere-insert"))

print(state.get_pose('home'))

print(state.get_pose('camera-closer-look'))

print(state.get_pose('show-cavity', 'star8'))

