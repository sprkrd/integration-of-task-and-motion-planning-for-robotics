#!/usr/bin/python

import math
import numpy as np
import rospy
import pyhop.itm_operators as itmop
from pyhop.itm_state import ItmState
from pyhop.pyhop import State, print_state
from time import sleep
from utils.mtn_planning import transform_list_to_pose_stamped

def test_move_j(state):
    itmop.move_j_both(state, ('picker', 'home'), ('catcher', 'home'), do=True)
    print_state(state)
    sleep(5.0)
    itmop.move_j_one(state, ('catcher', 'show-cavity', 'star5'), do=True)
    print_state(state)
    sleep(5.0)
    itmop.move_j_both(state, ("picker", "pre-grasp", 0), ("catcher", "home"), do=True)
    print_state(state)
    sleep(5.0)

def test_push(state):
    print_state(itmop.move_j_one(state, ('picker', 'push', 0, 0), do=True))
    print_state(itmop.push(state, 0, do=True))

def test_pick(state):
    print_state(itmop.move_j_one(state, ('picker', 'pre-grasp', 0), do=True))
    print_state(itmop.pick(state, 0, do=True))
    sleep(5.0)

def test_insert(state):
    itmop.move_j_both(state, ('picker', 'pre-ungrasp', 'sphere-insert'),
            ('catcher', 'show-cavity', 'cross'), do=True)
    sleep(5.0)
    print_state(state)
    itmop.insert(state, 0, 'cross', do=True)
    sleep(5.0)
    print_state(state)

def test_throw_away(state):
    itmop.throw_away(state, 0, do=True)
    sleep(5.0)
    print_state(state)

def test_place(state):
    print_state(itmop.find_place_location(state, do=True))
    print_state(itmop.move_j_one(state, ('picker', 'pre-ungrasp', 'place-location'), do=True))
    print_state(itmop.release(state, 0, ('place-location',), do=True))
    sleep(5.0)

if __name__ == '__main__':
    rospy.init_node("operators_test")
    state = ItmState("my-state", joints={}, cost=0.0, tool_status="closed",
                     holding=None, available_shapes=['cross'], similarities=np.array([[0.75]]),
                     probabilities=np.array([[1.0]]),
                     at={robot: ('unknown',) for robot in ('picker', 'catcher')},
                     objects=[([0.812, 0.376, 0.73, 0, math.pi, 0], "world")])
    # test_move_j(state)
    test_push(state)
    # test_pick(state)
    # test_place(state)
    # test_insert(state)
    # test_throw_away(state)

