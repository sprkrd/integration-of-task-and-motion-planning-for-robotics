#!/usr/bin/env python

import rospy
import actionlib_msgs
import actionlib
import time
import tf2_geometry_msgs
import numpy as np
import random
import pyhop.itm_operators2
import pyhop.itm_methods2
from pyhop.itm_state import ItmState
from pyhop.pyhop import pyhop, operators
from utils.mtn_planning import MotionPlanner, transform_list_to_pose_stamped, transform_pose_stamped_to_list
from utils.aclient import observe, get_true_shape
from utils.math_functions import scale_matrix, ml_shape

# available_shapes = ['star5', 'star6', 'hexagon', 'star8', 'triangle']
available_shapes = ['star5', 'star6', 'hexagon', 'star8', 'triangle', 'cross', 'circularsector', 'pentagon', 'trapezium', 'trapezoid']


def insert_piece(state=None):
    mtn = MotionPlanner.instance()
    if not state:
        scene = observe('scene', 3)
        available_shapes_columns = [scene.shape_names.index(shape_n) for shape_n in available_shapes]
        similarities = np.reshape(scene.avg_similitudes, (-1, 10))
        similarities = similarities[:, available_shapes_columns]
        assert similarities.shape[0] == similarities.shape[1]
        probabilities = scale_matrix(similarities.copy())
        objects = []
        for centroid, theta in zip(scene.centroids, scene.avg_pca_orientations):
            centroid_s = transform_list_to_pose_stamped(
                    [centroid.x, centroid.y, centroid.z, 0, 0, theta],
                    'camera_rgb_optical_frame')
            centroid_w = mtn._make_parallel_to_xy_plane(centroid_s)
            centroid_w.pose.position.z -= 15e-3
            objects.append((transform_pose_stamped_to_list(centroid_w), 'world'))
        state = ItmState('', joints={}, cost=0.0, tool_status="closed",
                      holding=None,
                      at={robot: ('unknown',) for robot in ('picker', 'catcher')},
                      objects=objects, probabilities=probabilities,
                      similarities=similarities,
                      available_shapes=available_shapes,
                      mistakes=0, replan=0, plan_time=0)    
    if not state.available_shapes:
        # start = rospy.Time.now()
        # mtn.go_home('picker')
        # mtn.go_home('catcher')
        # time.sleep(0.1)
        # state.cost += (rospy.Time.now() - start).to_sec()
        state = operators['move_j_one'](state, ('picker', 'home'), do=True)
        state = operators['move_j_one'](state, ('catcher', 'home'), do=True)
        return state
    print state.similarities
    print state.probabilities
    blob_idx, _, _ = ml_shape(state.available_shapes, state.probabilities)
    rospy.logwarn('Next piece to insert: {}'.format(blob_idx))
    plan_time = 0.0
    replan = 0
    piece_handled = False
    while not piece_handled:
        tic = time.time()
        _, plan = pyhop(state, [("piece_handled", blob_idx)], verbose=1)
        plan_time += time.time() - tic
        piece_handled = True
        for action in plan:
            action_n = action[0]
            action_args = action[1:]
            rospy.loginfo("Now executing {}{}".format(action_n, action_args))
            true_shape = get_true_shape()
            next_state = operators[action_n](state, *action_args, do=True)
            if next_state is False and action_n == 'close_look':
                if replan >= 2: continue
                print state.similarities
                print state.probabilities
                piece_handled = False
                rospy.logwarn("Replanning...")
                replan += 1
                break
            elif next_state is False: assert False
            else:
                print state.similarities
                print state.probabilities
                state = next_state
                if action_n == 'insert' and true_shape != action_args[1]:
                    rospy.logwarn('Made a mistake! Guess: {}; Truth: {}'.format(action_args[1], true_shape))
                    state.mistakes += 1
    state.plan_time += plan_time
    state.replan += replan
    rospy.logwarn('Total time until now (sim time): {}s'.format(state.cost))
    rospy.logwarn('Total planning time until now: {}s'.format(state.plan_time))
    rospy.logwarn('Total #replan until now: {}'.format(state.replan))
    rospy.logwarn('Total #mistakes until now: {}'.format(state.mistakes))
    return insert_piece(state)


if __name__ == '__main__':
    rospy.init_node("planning_simple_test")
    mtn = MotionPlanner.instance()
    mtn.go_home('both')
    random.seed(42)
    state = insert_piece()
    rospy.loginfo('Elapsed since begining (sim time): {}s'.format(state.cost))
    rospy.loginfo('Elapsed planning: {}s'.format(state.plan_time))
    rospy.loginfo('#Replans: {}'.format(state.replan))
    rospy.loginfo('#Mistakes: {}'.format(state.mistakes))
