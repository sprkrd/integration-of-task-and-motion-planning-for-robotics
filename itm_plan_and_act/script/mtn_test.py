#!/usr/bin/python

import rospy
import utils.mtn_planning as mtn_planning
import math
import numpy as np
import time

def test_transform_pose_conversions():
    pose_t = (1, 2, 3, 0, math.pi/2, math.pi/2)
    pose_s = mtn_planning.transform_list_to_pose_stamped(pose_t, "fake_frame")
    got = [pose_s.pose.position.x,
           pose_s.pose.position.y,
           pose_s.pose.position.z,
           pose_s.pose.orientation.x,
           pose_s.pose.orientation.y,
           pose_s.pose.orientation.z,
           pose_s.pose.orientation.w]
    if not np.allclose(got, [1, 2, 3, 0.5, 0.5, 0.5, 0.5]):
        rospy.logerr("test_transform_pose_conversions: {} != {}".format(got,
                [1, 2, 3, 0.5, 0.5, 0.5, 0.5]))
    pose_t_ = mtn_planning.transform_pose_stamped_to_list(pose_s)
    if not np.allclose(pose_t_, pose_t):
        rospy.logerr("test_transform_pose_conversions: {} != {}".format(pose_t,
                                                                        pose_t_))

def test_reproject_into_feasible_space():
    joints1 = {'iri_wam_picker_joint_1': -2.62,
               'iri_wam_picker_joint_2': 2.0,
               'iri_wam_picker_joint_7': 1.0}
    r_joints1 = mtn_planning.reproject_into_feasible_space(joints1)
    rospy.loginfo("test_reproject_into_feasible_space: reprojected joints: {}".format(r_joints1))

def test_distance_joint_space():
    conf1 = {'joint1': 0.5, 'joint2': 0, 'joint3': -0.5}
    conf2 = {'joint1': -0.5, 'joint2': 0.5, 'joint3': -0.5}
    conf3 = {'joint1': -0.5, 'joint2': 0.5}
    dist = mtn_planning.distance_joint_space(conf1, conf2)
    if abs(dist - 1.5) > 1e-5:
        rospy.logerr("test_distance_joint_space: dist(conf1, conf2) = {}".format(dist))
    try:
        mtn_planning.distance_joint_space(conf1, conf3)
        rospy.logerr("test_distance_joint_space: dist(conf1, conf3) has not thrown exception")
    except mtn_planning.MtnException:
        pass

def test_gripper_open_close():
    mtn = mtn_planning.MotionPlanner.instance()
    rospy.loginfo("test_gripper_open_close: Openning tool...")
    mtn._open_tool()
    rospy.loginfo("test_gripper_open_close: OK! Now closing tool...")
    mtn._close_tool()
    rospy.loginfo("test_gripper_open_close: OK!")

def test_get_state():
    mtn = mtn_planning.MotionPlanner.instance()
    rospy.loginfo("test_get_state: Trying to retrieve state...")
    joints1 = mtn.get_state()
    rospy.loginfo("test_get_state: Retrieved!: {}".format(joints1))
    rospy.loginfo("test_get_state: Trying to retrieve modified state...")
    joints2 = mtn.get_state({"iri_wam_picker_link_base": math.pi/2})
    rospy.loginfo("test_get_state: Retrieved!: {}".format(joints2))

def test_ik():
    mtn = mtn_planning.MotionPlanner.instance()
    pose_s1 = mtn_planning.transform_list_to_pose_stamped(
            [0.5, 0.5, 0.5, 0, 0, 0, 1], 'iri_wam_picker_link_base')
    pose_s2 = mtn_planning.transform_list_to_pose_stamped(
            [0.5, 0.5, 0.5, 0, 0, 0, 1], 'iri_wam_catcher_link_base')
    rospy.loginfo("test_ik: Requesting IK for 1st pose...")
    start = time.clock()
    try:
        joints1 = mtn.get_ik(pose_s1, "iri_wam_picker_gripper_tcp")
        rospy.loginfo("test_ik: OK!: {}".format(joints1))
    except mtn_planning.MtnException as e:
        rospy.logerr("test_ik: KO!: {}".format(e))
    rospy.loginfo(joints1)
    rospy.loginfo("test_ik: Elapsed: {}s. Requesting IK for 2nd pose...".format(time.clock()-start))
    start = time.clock()
    try:
        joints2 = mtn.get_ik(pose_s2, "iri_wam_catcher_sphere_link")
        rospy.loginfo("test_ik: OK!: {}".format(joints2))
    except mtn_planning.MtnException as e: 
        rospy.logerr("test_ik: KO!: {}".format(e))
    rospy.loginfo(joints2)
    rospy.loginfo("test_ik: Elapsed: {}s. Requesting simultaneous IK".format(time.clock()-start))
    start = time.clock()
    try:
        joints3 = mtn.get_ik([pose_s2, pose_s1], ["iri_wam_catcher_sphere_link","iri_wam_picker_gripper_tcp"])
        rospy.loginfo("test_ik: OK!: {}".format(joints3))
    except mtn_planning.MtnException as e:
        rospy.logerr("test_ik: KO!: {}".format(e))
    rospy.loginfo("test_ik: Elapsed: {}s.".format(time.clock()-start))

def test_plan_in_joints():
    mtn = mtn_planning.MotionPlanner.instance()
    joints1 = {'iri_wam_picker_joint_{}'.format(idx): 0.5 for idx in range(1,8)}
    joints2 = {'iri_wam_catcher_joint_{}'.format(idx): 0.5 for idx in range(1,8)}
    joints3 = joints1.copy()
    joints3.update(joints2)
    rospy.loginfo("test_plan_in_joints: Computing 1st plan...")
    try:
        plan1 = mtn.plan_in_joints(joints1, "picker")
    except mtn_planning.MtnException as e:
        rospy.logerr("test_plan_in_joints: {}".format(e))
    rospy.loginfo("test_plan_in_joints: Computing 2nd plan...")
    try:
        plan2 = mtn.plan_in_joints(joints2, "catcher", start_joints={'iri_wam_catcher_joint_1': -0.5})
    except mtn_planning.MtnException as e:
        rospy.logerr("test_plan_in_joints: {}".format(e))
    rospy.loginfo("test_plan_in_joints: Computing 3rd plan...")
    try:
        plan3 = mtn.plan_in_joints(joints3, "both")
    except mtn_planning.MtnException as e:
        rospy.logerr("test_plan_in_joints: {}".format(e))

def test_plan_to_target():
    mtn = mtn_planning.MotionPlanner.instance()
    p1 = mtn_planning.transform_list_to_pose_stamped(
            [0.812, 0.376, 0.724, 0, math.pi, 0], 'world')
            # [0.812, 0.376, 0.724, 0, math.pi, 0], 'world')
    p2 = mtn_planning.transform_list_to_pose_stamped(
            [0.5, 0.5, 0.5, 0, 0, 0], 'iri_wam_catcher_link_base')
    rospy.loginfo("test_plan_to_target: Computing 1st plan...")
    try:
        plan1 = mtn.plan_to_target(p1, "iri_wam_picker_gripper_tcp")
        rospy.loginfo("test_plan_to_target: OK!")
    except mtn_planning.MtnException as e:
        rospy.logerr("test_plan_to_target: KO!: {}".format(e))
    rospy.loginfo("test_plan_to_target: Computing 2nd plan...")
    try:
        plan2 = mtn.plan_to_target(p2, "iri_wam_catcher_sphere_link")
        rospy.loginfo("test_plan_to_target: OK!")
    except mtn_planning.MtnException as e:
        rospy.logerr("test_plan_to_target: KO!: {}".format(e))
    rospy.loginfo("test_plan_to_target: Computing 3rd plan...")
    try:
        plan3 = mtn.plan_to_target([p1,p2], ["iri_wam_picker_gripper_tcp", "iri_wam_catcher_sphere_link"])
        rospy.loginfo("test_plan_to_target: OK!")
    except mtn_planning.MtnException as e:
        rospy.logerr("test_plan_to_target: KO!: {}".format(e))

def test_get_parent_pose():
    mtn = mtn_planning.MotionPlanner.instance()
    pose_s = mtn_planning.transform_list_to_pose_stamped([0.812, 0.376, 0.924, 0, math.pi, 0], "world")
    parent_s =  mtn._get_parent_pose(pose_s, "iri_wam_picker_gripper_tcp", "iri_wam_picker_link_7")
    parent_t = mtn_planning.transform_pose_stamped_to_list(parent_s)
    if not np.allclose(parent_t, [0.812, 0.376, 0.924+0.175, -math.pi, 0, -math.pi]):
        rospy.logerr("test_get_parent_pose: error! {} != [0.812, 0.376, 1.099, pi, 0, pi".format(parent_t))

def test_plan_in_cartesian():
    mtn = mtn_planning.MotionPlanner.instance()
    wp = [mtn_planning.transform_list_to_pose_stamped([0.812, 0.376, 0.924, 0, math.pi, 0], "world"),
          mtn_planning.transform_list_to_pose_stamped([0.812, 0.376, 0.724, 0, math.pi, 0], "world")]
    rospy.loginfo("test_plan_in_cartesian: Computing cartesian path...")
    try:
        plan = mtn.plan_in_cartesian(wp, "iri_wam_picker_gripper_tcp")
        rospy.loginfo("test_plan_in_cartesian: OK!")
    except mtn_planning.MtnException as e:
        rospy.logerr("test_plan_in_cartesian: KO!: {}".format(e))

def test_move_in_joints():
    mtn = mtn_planning.MotionPlanner.instance()
    joints1 = {'iri_wam_picker_joint_{}'.format(idx): 0.5 for idx in range(1,8)}
    joints2 = {'iri_wam_catcher_joint_{}'.format(idx): 0.5 for idx in range(1,8)}
    joints3 = {'iri_wam_{}_joint_{}'.format(group, idx): 0.0 for group in ("picker", "catcher") for idx in range(1,8)}
    rospy.loginfo("test_move_in_joints: Computing 1st plan...")
    try:
        plan1 = mtn.move_in_joints(joints1, "picker")
    except mtn_planning.MtnException as e:
        rospy.logerr("test_move_in_joints: {}".format(e))
    rospy.loginfo("test_move_in_joints: Computing 2nd plan...")
    try:
        plan2 = mtn.move_in_joints(joints2, "catcher")
    except mtn_planning.MtnException as e:
        rospy.logerr("test_move_in_joints: {}".format(e))
    rospy.loginfo("test_move_in_joints: Computing 3rd plan...")
    try:
        plan3 = mtn.move_in_joints(joints3, "both")
    except mtn_planning.MtnException as e:
        rospy.logerr("test_move_in_joints: {}".format(e))

def test_move_to_target():
    mtn = mtn_planning.MotionPlanner.instance()
    p1 = mtn_planning.transform_list_to_pose_stamped(
            [0.812, 0.376, 0.724, 0, math.pi, 0], 'world')
            # [0.812, 0.376, 0.724, 0, math.pi, 0], 'world')
    p2 = mtn_planning.transform_list_to_pose_stamped(
            [0.5, 0.5, 0.5, 0, 0, 0], 'iri_wam_catcher_link_base')
    p3 = mtn_planning.transform_list_to_pose_stamped(
            [0.98, 0.01, 0.724, 0, math.pi, 0], 'world')
    p4 = mtn_planning.transform_list_to_pose_stamped(
            [-0.5, -0.5, 0.5, 0, 0, 0], 'iri_wam_catcher_link_base')
    rospy.loginfo("test_move_to_target: Computing 1st plan...")
    try:
        plan1 = mtn.move_to_target(p1, "iri_wam_picker_gripper_tcp")
        rospy.loginfo("test_move_to_target: OK!")
    except mtn_planning.MtnException as e:
        rospy.logerr("test_move_to_target: KO!: {}".format(e))
    rospy.loginfo("test_move_to_target: Computing 2nd plan...")
    try:
        plan2 = mtn.move_to_target(p2, "iri_wam_catcher_sphere_link")
        rospy.loginfo("test_move_to_target: OK!")
    except mtn_planning.MtnException as e:
        rospy.logerr("test_move_to_target: KO!: {}".format(e))
    rospy.loginfo("test_move_to_target: Computing 3rd plan...")
    try:
        plan3 = mtn.move_to_target([p3,p4], ["iri_wam_picker_gripper_tcp", "iri_wam_catcher_sphere_link"])
        rospy.loginfo("test_move_to_target: OK!")
    except mtn_planning.MtnException as e:
        rospy.logerr("test_move_to_target: KO!: {}".format(e))

def test_move_in_cartesian():
    mtn = mtn_planning.MotionPlanner.instance()
    wp = [mtn_planning.transform_list_to_pose_stamped([0.812, 0.376, 0.924, 0, math.pi, 0], "world"),
          mtn_planning.transform_list_to_pose_stamped([0.812, 0.376, 0.73, 0, math.pi, 0], "world")]
    rospy.loginfo("test_plan_in_cartesian: Computing cartesian path...")
    try:
        plan = mtn.move_in_cartesian(wp, "iri_wam_picker_gripper_tcp")
        rospy.loginfo("test_plan_in_cartesian: OK!")
    except mtn_planning.MtnException as e:
        rospy.logerr("test_plan_in_cartesian: KO!: {}".format(e))

def test_pick_and_place():
    mtn = mtn_planning.MotionPlanner.instance()
    ps1 = mtn_planning.transform_list_to_pose_stamped([0.812, 0.376, 0.73, 0, math.pi, 0], "world")
    ps2 = mtn_planning.transform_list_to_pose_stamped([0.98, 0.01, 0.73, 0, math.pi, 0], "world")
    try:
        mtn.pick(ps1)
        rospy.loginfo("test_pick_and_place: OK! 1 grasped")
    except mtn_planning.MtnException as e:
        rospy.logerr("test_pick_and_place: KO! 1 not grasped: {}".format(e))
    # try:
        # mtn.place(ps1)
        # rospy.loginfo("test_pick_and_place: OK! 1 ungrasped")
    # except mtn_planning.MtnException as e:
        # rospy.logerr("test_pick_and_place: KO! 1 not ungrasped: {}".format(e))
    # try:
        # mtn.pick(ps2)
        # rospy.loginfo("test_pick_and_place: OK! 2 grasped")
    # except mtn_planning.MtnException as e:
        # rospy.logerr("test_pick_and_place: KO! 2 not grasped: {}".format(e))
    # try:
        # mtn.place(ps2)
        # rospy.loginfo("test_pick_and_place: OK! 2 ungrasped")
    # except mtn_planning.MtnException as e:
        # rospy.logerr("test_pick_and_place: KO! 2 not ungrasped: {}".format(e))

def test_show_cavity():
    mtn = mtn_planning.MotionPlanner.instance()
    for shape in ('star5', 'pentagon', 'triangle', 'cross', 'circularsector', 'star6', 'pentagon', 'trapezoid', 'trapezium', 'hexagon', 'star8'):
        try:
            mtn.show_cavity(shape)
        except mtn_planning.MtnException as e:
            rospy.logerr("test_show_cavity: show_cavity star5 failed!!: {}".format(e))
        joints = mtn.get_joints(group="catcher").items()
        joints.sort()
        joints = map(lambda x: x[1], joints)
        rospy.loginfo("{}: {}".format(shape, joints))

def test_show_to_camera():
    mtn = mtn_planning.MotionPlanner.instance()
    try:
        mtn.show_gripper_to_camera()
        rospy.loginfo("test_show_to_camera: OK!")
    except mtn_planning.MtnException as e:
        rospy.logerr("test_show_to_camera: KO! {}".format(e))

def test_go_home():
    mtn = mtn_planning.MotionPlanner.instance()
    try:
        mtn.go_home("both")
        rospy.loginfo("test_go_home: OK!")
    except mtn_planning.MtnException as e:
        rospy.logerr("test_go_home: KO! {}".format(e))

def test_fk():
    mtn = mtn_planning.MotionPlanner.instance()
    try:
        pose_stamped = mtn.get_fk(['iri_wam_picker_link_7', 'iri_wam_picker_gripper_tcp'])
        rospy.loginfo(pose_stamped)
        pose_stamped = mtn.get_fk(['iri_wam_catcher_sphere_link'],
                target_frame='iri_wam_catcher_link_base', joint_values={'iri_wam_catcher_joint_2':1.57})
        rospy.loginfo(pose_stamped)
        rospy.loginfo("test_fk: OK!")
    except mtn_planning.MtnException as e:
        rospy.logerr("test_fk: KO!")

if __name__ == '__main__':
    rospy.init_node("mtn_test")
    # test_transform_pose_conversions()
    # test_reproject_into_feasible_space()
    # test_distance_joint_space()
    # test_gripper_open_close()
    # test_get_state()
    # test_ik()
    # test_plan_in_joints()
    # test_plan_to_target()
    # test_get_parent_pose()
    # test_plan_in_cartesian()
    # test_move_in_joints()
    # test_move_to_target()
    # test_move_in_cartesian()
    # test_pick_and_place()
    test_show_cavity()
    # start = rospy.Time.now()
    # while start.is_zero():
        # start = rospy.Time.now()
    # test_show_to_camera()
    # elapsed = rospy.Time.now() - start
    # print "Elapsed going home: {}s".format(elapsed.to_sec())
    # test_go_home()
    # test_fk()

