#!/usr/bin/python

import math
import rospy
import numpy as np
from time import sleep, clock
from utils.mtn_planning import transform_list_to_pose_stamped
from utils.math_functions import scale_matrix, ml_shape

import pyhop.itm_operators2 as itmop
import pyhop.itm_methods2 as methods
from pyhop.pyhop import print_state, print_operators, print_methods, pyhop, operators
from pyhop.itm_state import ItmState
 

def test_decorator(f):
    def xf(*args, **kwargs):
        print "###############\n{}\n###############".format(f.__name__)
        return f(*args, **kwargs)
    return xf

@test_decorator
def test_robot_at():
    state = ItmState("my-state", joints={}, cost=0.0, tool_status="closed",
                  holding=None,
                  at={robot: ('unknown',) for robot in ('picker', 'catcher')},
                  objects=[([0.812, 0.376, 0.73, 0, math.pi, 0], "world")])
    pyhop(state, [('robot_at', ("picker", "pre-grasp", 0))], verbose=3)


@test_decorator
def test_tool_ready():
    state = ItmState("my-state", joints={'iri_wam_picker_joint_2': 0.1, 'iri_wam_picker_joint_4': 2.9}, cost=0.0, tool_status="closed",
                  holding=0,
                  at={robot: ('unknown',) for robot in ('picker', 'catcher')},
                  objects=[("picked",)])
    pyhop(state, [("tool_ready",)], verbose=3)
    state = ItmState("my-state", joints={'iri_wam_picker_joint_2': 0.1, 'iri_wam_picker_joint_4': 2.9}, cost=0.0, tool_status="open",
                  holding=None,
                  at={robot: ('unknown',) for robot in ('picker', 'catcher')},
                  objects=[])
    pyhop(state, [("tool_ready",)], verbose=3)

@test_decorator
def test_grasped():
    state = ItmState("my-state", joints={}, cost=0.0, tool_status="closed",
                  holding=None,
                  at={robot: ('unknown',) for robot in ('picker', 'catcher')},
                  objects=[([0.812, 0.376, 0.73, 0, math.pi, 0], "world")])
    pyhop(state, [("grasped", 0)], verbose=3)
    state = ItmState("my-state", joints={'iri_wam_catcher_joint_1': 0.2, 'iri_wam_catcher_joint_2': 1.55, 'iri_wam_catcher_joint_4': 0}, cost=0.0, tool_status="closed",
                  holding=None,
                  at={robot: ('unknown',) for robot in ('picker', 'catcher')},
                  objects=[([0.812, 0.0, 0.73, 0, math.pi, 0], "world")])
    pyhop(state, [("grasped", 0)], verbose=3)

@test_decorator
def test_verify():
    available_shapes = ['star5', 'star6', 'star8', 'triangle', 'hexagon', 'trapezium', 'trapezoid', 'pentagon', 'cross', 'circularsector']
    similarities = np.reshape([0.5994777543436778, 0.863477377863026, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.6871228509620858, 0.797472639061266, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.8528752800597461, 0.5655179322781005, 0.829976861770503, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.7253907042825248, 0.8637858851674641, 0.5717713600137115, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.8598381715688155, 0.5117173524150268, 0.9156740080555318, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.8256673768642687, 0.6249876737994281, 0.7192390834414181, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.6823663831407897, 0.711737964797762, 0.5878945092952875, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.7210731107925455, 0.5357596948506039, 0.8882836143536532, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.8951465201465202, 0.6402923419129329, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.6356227106227106, 0.9836705715299965], (-1, 10))
    probabilities = scale_matrix(similarities.copy())
    holding = 0
    shape_idx, shape_n = ml_shape(available_shapes, probabilities[holding, :])
    objects = ["trash"]*10
    objects[holding] = "picked"
    state = ItmState("my-state", joints={}, cost=0.0, tool_status="closed",
                  holding=holding,
                  at={robot: ('unknown',) for robot in ('picker', 'catcher')},
                  objects=objects, probabilities=probabilities,
                  similarities=similarities,
                  available_shapes=available_shapes)
    pyhop(state, [("verified", holding, shape_n, 0.6, 0.995)], verbose=3)

@test_decorator
def test_ready_to_insert():
    available_shapes = ['star5', 'star6', 'star8', 'triangle', 'hexagon', 'trapezium', 'trapezoid', 'pentagon', 'cross', 'circularsector']
    similarities = np.reshape([0.5994777543436778, 0.863477377863026, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.6871228509620858, 0.797472639061266, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.8528752800597461, 0.5655179322781005, 0.829976861770503, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.7253907042825248, 0.8637858851674641, 0.5717713600137115, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.8598381715688155, 0.5117173524150268, 0.9156740080555318, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.8256673768642687, 0.6249876737994281, 0.7192390834414181, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.6823663831407897, 0.711737964797762, 0.5878945092952875, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.7210731107925455, 0.5357596948506039, 0.8882836143536532, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.8951465201465202, 0.6402923419129329, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.6356227106227106, 0.9836705715299965], (-1, 10))
    probabilities = scale_matrix(similarities.copy())
    holding = 0
    shape_idx, shape_n = ml_shape(available_shapes, probabilities[holding, :])
    objects = ["trash"]*10
    objects[holding] = "grasped"
    state = ItmState("my-state", joints={}, cost=0.0, tool_status="closed",
                  holding=holding,
                  at={robot: ('unknown',) for robot in ('picker', 'catcher')},
                  objects=objects, probabilities=probabilities,
                  similarities=similarities,
                  available_shapes=available_shapes)
    pyhop(state, [("ready_to_insert", holding, shape_n)], verbose=3)

@test_decorator
def test_insertion():
    available_shapes = ['star5', 'star6', 'star8', 'triangle', 'hexagon', 'trapezium', 'trapezoid', 'pentagon', 'cross', 'circularsector']
    similarities = np.reshape([0.5994777543436778, 0.863477377863026, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.6871228509620858, 0.797472639061266, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.8528752800597461, 0.5655179322781005, 0.829976861770503, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.7253907042825248, 0.8637858851674641, 0.5717713600137115, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.8598381715688155, 0.5117173524150268, 0.9156740080555318, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.8256673768642687, 0.6249876737994281, 0.7192390834414181, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.6823663831407897, 0.711737964797762, 0.5878945092952875, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.7210731107925455, 0.5357596948506039, 0.8882836143536532, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.8951465201465202, 0.6402923419129329, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.6356227106227106, 0.9836705715299965], (-1, 10))
    probabilities = scale_matrix(similarities.copy())
    holding = 3
    shape_idx, shape_n = ml_shape(available_shapes, probabilities[holding, :])
    objects = ["trash"]*10
    objects[holding] = "grasped"
    state = ItmState("my-state", joints={}, cost=0.0, tool_status="closed",
                  holding=holding,
                  at={robot: ('unknown',) for robot in ('picker', 'catcher')},
                  objects=objects, probabilities=probabilities,
                  similarities=similarities,
                  available_shapes=available_shapes)
    pyhop(state, [("inserted", holding, shape_n)], verbose=3)

@test_decorator
def test_handle_piece():
    available_shapes = ['cross', 'circularsector']
    similarities = np.array([[0.55]])
    probabilities = scale_matrix(similarities.copy())
    objects = [([0.812, 0.376, 0.73, 0, math.pi, 0], "world")]
    state = ItmState("my-state", joints={}, cost=0.0, tool_status="closed",
                  holding=None,
                  at={robot: ('unknown',) for robot in ('picker', 'catcher')},
                  objects=objects, probabilities=probabilities,
                  similarities=similarities, available_shapes=available_shapes)
    start = clock()
    final_state, plan = pyhop(state, [("piece_handled", 0)], verbose=3)
    planning_time = clock() - start
    for action in plan:
        action_n = action[0]
        action_args = action[1:]
        print "Now executing {}{}".format(action_n, action_args)
        state = operators[action_n](state, *action_args, do=True)

    print "predicted final time: {}".format(final_state.cost)
    print "final_time: {}s".format(state.cost)
    print "planning_time: {}".format(planning_time)


if __name__ == '__main__':
    rospy.init_node("operators_test")
    print_operators()
    print_methods()
    # test_robot_at()
    # test_tool_ready()
    # test_grasped()
    # test_verify()
    # test_ready_to_insert()
    # test_insertion()
    test_handle_piece()


