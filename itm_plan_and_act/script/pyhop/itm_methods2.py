from math import sqrt, pi
from geometry_msgs.msg import PoseStamped
from .pyhop import declare_methods
from utils.math_functions import entropy, ml_shape

def euclidean_distance(ps1, ps2):
    dx = ps1[0] - ps2[0]
    dy = ps1[1] - ps2[1]
    dz = ps1[2] - ps2[2]
    return sqrt(dx*dx + dy*dy + dz*dz)

# methods for robot_at task

def robot_at_m1(state, args):
    if state.at[args[0]] == args[1:]:
        return []
    return False

def robot_at_m2(state, args):
    return [("move_j_one", args)]

declare_methods("robot_at", robot_at_m1, robot_at_m2)

def tool_ready_m1(state):
    if state.holding is None and state.tool_status == "closed":
        return []
    return False

def tool_ready_m2(state):
    if state.tool_status == "open":
        return [("close_tool",)]
    return False

def tool_ready_m3(state):
    if state.holding is None: return False
    object_ = state.holding
    return [('robot_at', ('catcher', 'home')),
            ('find_place_location',),
            ("robot_at", ("picker", "pre-ungrasp", "place-location")),
            ('release', object_, ('place-location',))]

declare_methods("tool_ready", tool_ready_m1, tool_ready_m2, tool_ready_m3)


def grasped_m1(state, object_):
    if state.holding == object_:
        return []
    return False

def grasped_m2(state, object_):
    return [("tool_ready", ),
            ('robot_at', ('catcher', 'home')),
            ("robot_at", ("picker", "pre-grasp", object_)),
            ("pick", object_)]

declare_methods("grasped", grasped_m1, grasped_m2)

def verified_m1(state, object_, shape, min_similarity, max_entropy):
    shape_idx = state.available_shapes.index(shape)
    similarity = state.similarities[object_, shape_idx]
    n_ent = entropy(state.probabilities[object_], normalized=True)
    if similarity > min_similarity and n_ent < max_entropy:
        return []
    return False

def verified_m2(state, object_, shape, min_similarity, max_entropy):
    return [('robot_at', ('catcher', 'home')),
            ("robot_at", ("picker", "camera-closer-look")),
            ("close_look", object_, shape, min_similarity, max_entropy)]

declare_methods("verified", verified_m1, verified_m2)

def ready_to_insert_m1(state, object_, shape):
    return [("verified", object_, shape, 0.0, 0.98),
            ('robot_at', ('picker', 'home')),
            ('robot_at', ('catcher', 'show-cavity', shape)),
            ('robot_at', ('picker', 'pre-ungrasp', 'sphere-insert'))]

declare_methods("ready_to_insert", ready_to_insert_m1)

def insertion_m1(state, object_, shape):
    return [("ready_to_insert", object_, shape),
            ("insert", object_, shape)]

declare_methods("inserted", insertion_m1)

def piece_handled_m1(state, object_):
    shape_idx, shape_n = ml_shape(state.available_shapes, state.probabilities[object_,:])
    return [("grasped", object_),
            ("inserted", object_, shape_n),
            ("robot_at", ("picker", "home")),
            ("robot_at", ("catcher", "home"))]

declare_methods("piece_handled", piece_handled_m1)

