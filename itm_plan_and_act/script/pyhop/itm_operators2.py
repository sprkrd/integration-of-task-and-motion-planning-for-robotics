import rospy
import numpy as np
import random

from .pyhop import declare_operators

from copy import deepcopy

from time import sleep

from utils.aclient import observe, get_true_shape
from utils.math_functions import scale_matrix, entropy
from utils.mtn_planning import MotionPlanner, MtnException, get_home_joints, \
                               show_cavity_target, \
                               transform_pose_stamped_to_list, \
                               transform_list_to_pose_stamped, \
                               get_show_gripper_to_camera_target, \
                               build_pose_stamped_from_point_and_yaw, \
                               get_trash_target

# Auxiliary functions

def plan_in_joints(state, joints_dst, group, do=False):
    try:
        mtn = MotionPlanner.instance()
        if do:
            start = rospy.Time.now()
            mtn.move_in_joints(joints_dst, group)
            sleep(0.1) # without this sleep, the clock doesn't get update appropriately
            elapsed = (rospy.Time.now() - start).to_sec()
            state.cost += elapsed
        state.joints.update(joints_dst)
        return state
    except MtnException as e:
        return False

def plan_in_cartesians(state, wp, ee, do=False):
    try:
        mtn = MotionPlanner.instance()
        if do:
            start = rospy.Time.now()
            mtn.move_in_cartesian(wp, ee)
            sleep(0.1) # without this sleep, the clock doesn't get update appropriately
            elapsed = (rospy.Time.now() - start).to_sec()
            state.cost += elapsed
            state.joints.update(mtn.get_joints(group='both'))
        return state
    except MtnException as e:
        return False

# Operators

def move_j_one(state, args, do=False):
    mtn = MotionPlanner.instance()
    group = args[0]
    pose = state.get_pose(*args[1:])
    try:
        if pose[0] == 'target6d':
            # ['target6d', ee, pose_stamped]
            joints_dst = mtn.get_ik(pose[2], pose[1], joint_values=state.joints)
        elif pose[0] == 'joints':
            # ['joints', [v1, v2, ..., v7]]
            joints_dst = {'iri_wam_{}_joint_{}'.format(group, idx+1): pose[1][idx]
                          for idx in range(7)}
    except MtnException as e:
        return False
    new_state = plan_in_joints(state, joints_dst, group, do=do)
    if new_state:
        new_state.at[group] = args[1:]
        return new_state
    return False

def push(state, object_, do=False):
    mtn = MotionPlanner.instance()

    if state.at['picker'] != ('push', object_, 0): return False

    pose_1 = state.get_pose('push', object_, 1)
    pose_2 = state.get_pose('push', object_, 2)
    pose_3 = state.get_pose('push', object_, 3)

    new_state = plan_in_cartesians(state, [pose_1[2], pose_2[2], pose_3[2]], pose_1[1], do=do)
    if not new_state: return False
    new_state.objects[object_][0][1] += 0.045 + 0.05
    new_state.at['picker'] = ('hovering',)
    return new_state

def pick(state, object_, do=False):
    mtn = MotionPlanner.instance()

    if state.holding != None: return False
    if state.at['picker'] != ('pre-grasp', object_): return False

    pose = state.get_pose('grasp', object_)
    pose_pre = state.get_pose('pre-grasp', object_)

    if do: mtn._open_tool()
    state.tool_status = 'open'
    new_state = plan_in_cartesians(state, [pose[2]], pose[1], do=do)
    if not new_state: return False
    if do: mtn._close_tool()
    new_state.tool_status = 'closed'
    new_state = plan_in_cartesians(state, [pose_pre[2]], pose_pre[1], do=do)
    if not new_state: return False
    new_state.at['picker'] = ('hovering',)
    new_state.objects[object_] = ('picked',)
    new_state.holding = object_
    return new_state

def find_place_location(state, do=False):
    mtn = MotionPlanner.instance()
    valid_location_found = False
    while not valid_location_found:
        x = random.uniform(0.56, 1.05)
        y = random.uniform(-0.33, 0.65)
        valid_location_found = True
        for location in state.objects:
            if len(location) == 1: continue
            dx = location[0][0] - x
            dy = location[0][1] - y
            if dx*dx + dy*dy < 5e-2**2:
                valid_location_found = False
                break
        place_location = [x, y, 0.73, 0, np.pi, 0]
        pose_s = transform_list_to_pose_stamped(place_location, 'world')
        try:
            mtn.get_ik(pose_s, 'iri_wam_picker_gripper_tcp', joint_values=state.joints,
                    avoid_collisions=False) 
        except MtnException:
            valid_location_found = False
    state.place_location = place_location
    return state

def release(state, object_, location, do=False):
    mtn = MotionPlanner.instance()
    if state.holding != object_: return False
    if state.at['picker'][0] != 'pre-ungrasp' or state.at['picker'][1:] != location: return False

    pose = state.get_pose('ungrasp', *location)
    pose_pre = state.get_pose('pre-ungrasp', *location)
    
    new_state = plan_in_cartesians(state, [pose[2]], pose[1], do=do)
    if not new_state: return False
    if do: mtn._open_tool()
    new_state.tool_status = 'open'
    new_state = plan_in_cartesians(state, [pose_pre[2]], pose_pre[1], do=do)
    if not new_state: return False
    if do: mtn._close_tool()
    state.tool_status = 'closed'
    new_state.at['picker'] = ('hovering',)
    print object_
    new_state.objects[object_] = (transform_pose_stamped_to_list(pose[2]), 'world')
    new_state.holding = None
    return new_state

def insert(state, object_, cavity, do=False):
    mtn = MotionPlanner.instance()

    if state.holding != object_: return False
    if state.at['picker'] != ('pre-ungrasp', 'sphere-insert'): return False
    if state.at['catcher'] != ('show-cavity', cavity): return False

    pose = state.get_pose('ungrasp', 'sphere-insert')
    pose_pre = state.get_pose('pre-ungrasp', 'sphere-insert')

    new_state = plan_in_cartesians(state, [pose[2]], pose[1], do=do)
    if not new_state: return False
    if do: mtn._open_tool()
    new_state.tool_status = 'open'
    new_state = plan_in_cartesians(state, [pose_pre[2]], pose_pre[1], do=do)
    if not new_state: return False
    if do: mtn._close_tool()
    new_state.tool_status = 'closed'
    new_state.at['picker'] = ('hovering',)
    new_state.holding = None
    del state.objects[object_]
    shape_idx = state.available_shapes.index(cavity)
    del state.available_shapes[shape_idx]
    if state.objects:
        state.similarities = np.delete(state.similarities, shape_idx, 1)
        state.similarities = np.delete(state.similarities, object_, 0)
        state.probabilities = scale_matrix(state.similarities.copy())
    else:
        state.similarities = np.array([])
        state.probabilities = np.array([])
    return new_state
    
def throw_away(state, object_, do=False):
    mtn = MotionPlanner.instance()
    if state.holding != object_ or state.at['picker'] != ('ungrasp', 'trash'): return False
    if do:
        mtn._open_tool()
        sleep(0.1)
        mtn._close_tool()
    state.objects[object_] = ('trash',)
    state.holding = None
    return state

def close_tool(state, do=False):
    mtn = MotionPlanner.instance()
    if state.tool_status != 'open': return False
    mtn._close_tool()
    state.tool_status = 'closed'
    return state

def close_look(state, object_, shape, min_similarity, max_entropy, do=False):
    if state.holding != object_ or state.at['picker'] != ('camera-closer-look',):
        return False
    if do:
        near_scene = observe('near', 3)
        if len(near_scene.avg_similitudes) != 10: return False
        similarities_row = np.array(near_scene.avg_similitudes)
        available_shapes_columns = [near_scene.shape_names.index(shape_n)
                                    for shape_n in state.available_shapes]
        shape_idx = state.available_shapes.index(shape)
        similarities_row = similarities_row[available_shapes_columns]
        state.similarities[object_, :] = similarities_row
        state.probabilities = scale_matrix(state.similarities.copy())
        probability = state.probabilities[object_, shape_idx]
        n_ent = entropy(state.probabilities[object_], normalized=True)
        similarity = state.similarities[object_, shape_idx]
        if n_ent > max_entropy or similarity < min_similarity or probability < 0.5: return False
    return state

declare_operators(move_j_one, push, pick, find_place_location, release, insert, throw_away, close_tool, close_look)

