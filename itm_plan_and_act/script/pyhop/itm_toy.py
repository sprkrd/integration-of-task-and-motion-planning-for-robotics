from __future__ import print_function

from pyhop import *
from random import choice

def move_to_named_pose(state, group, *args):
    if state.at[group] == args:
        return False
    else:
        state.at[group] = args
    return state

def pick(state, blob):
    if state.grasped is not None or blob not in state.blobs or state.at['catcher'] != ('home',):
        return False
    else:
        state.at['picker'] = ('post-grasp', blob)
        state.grasped = blob
        state.blobs.remove(blob)
    return state

def insert(state, blob):
    if state.grasped != blob or state.at['catcher'] != ('showing_cavity', blob):
        return False
    else:
        state.grasped = None
        state.at['picker'] = ('post-ungrasp', blob)
    return state

declare_operators(move_to_named_pose, pick, insert)

def choose_and_insert(state):
    blob = choice(state.blobs)
    return [('insert_one', blob)]

declare_methods('choose_and_insert', choose_and_insert)

def insert_one(state, blob):
    return [('go_home', 'catcher'), ('pick', blob),
            ('move_to_named_pose', 'catcher', 'showing_cavity', blob),
            ('insert', blob)]

declare_methods('insert_one', insert_one)

def already_in_home(state, group):
    if (state.at[group] == ('home',)):
        return []
    else:
        return False

def not_in_home_yet(state, group):
    return [('move_to_named_pose', 'catcher', 'home')]

declare_methods('go_home', already_in_home, not_in_home_yet)

state1 = State('state1')
state1.at = {'picker': ('home',), 'catcher': ('home',)}
state1.blobs = ['a', 'b', 'c']
state1.grasped = None

pyhop(state1, [('choose_and_insert',), ('choose_and_insert',)], verbose=3)

