import utils
import math

alpha = 1
sigma_xyz0 = 0.0025
sigma_theta0 = 5.0
delta_xyz = 0.005
delta_theta = 5.0
s_min = 0.5

def observe_table(state, n):
    if not state.away['catcher'] and not state.away['picker']:
        return False

    state.sigma_xyz = sigma_xyz0 / math.sqrt(n)
    state.sigma_theta = sigma_theta0 / math.sqrt(n)

    return state

def pick(state, b):
    mtn = motion_planning_utils.MoveItWrapper.instance
    if not state.empty_hand:
        return False
    try:
        theta = 0
        centroid = state.centroids[b]
        joints = mtn.pregrasp_joints(centroid, theta, state.joints)
        state.cost += alpha*motion_planning_utils.distance_joint_space(
                joints, state.joints)
        epsilon_xyz = 1 - math.erf(delta_xyz/(math.sqrt(2)*state.sigma_xyz))
        epsilon_theta = 1 - math.erf(delta_theta/(math.sqrt(2)*state.sigma_theta))
        state.cost -= 3*math.log(1-epsilon_xyz) + math.log(1-epsilon_theta)
        state.joints = joints
        state.away['picker'] = False
        state.showing_at_camera = False
        state.empty_hand = False
        state.grasped = b
    except motion_planning_utils.MtnException as e:
        print "pick: ", str(e)
        return False

def move_away(state, robot):
    if state.away[robot]:
        return False
    state.cost += abs(math.pi/2 - state.joints['iri_wam_{}_joint_1'.format(robot)])
    state.joints['iri_wam_{}_joint_1'.format(robot)] = math.pi
    if robot == 'picker':
        state.showing_at_camera = False
    if robot == 'catcher':
        state.showing_cavity = None
    return state

def show_to_camera(state, b):
    state.showing_to_camera = True
    return state

def observe_grasped(state, b, p, s, n):
    if state.empty_hand or state.grasped != b or state.showing_to_camera=True:
        return False
    return state

def show_cavity(state, p):
    mtn = motion_planning_utils.MoveItWrapper.instance
    try:
        joints = mtn.show_cavity_joints(state.available_shapes[p], start_joints=state.joints)
        state.cost += alpha*motion_planning_utils.distance_joint_space(joints, state.joints)
        state.away['catcher'] = False
        state.showing_cavity = p
    except motion_planning_utils.MtnException as e:
        print "show_cavity: ", str(e)
    return state

def put(state, b, p):
    mtn = motion_planning_utils.MoveItWrapper.instance
    if not
    try:
       joints = mtn.pregrasp_joints()
    except motion_planning_utils.MtnException as e:


# METHODS

def clear_view(state):
    subtasks = []
    if not state.away['catcher']:
        subtasks.append(('move_away', 'catcher'))
    if not state.away['picker']:
        subtasks.append(('move_away', 'picker'))
    return subtasks
