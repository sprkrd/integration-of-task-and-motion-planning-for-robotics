from __future__ import print_function

from pyhop import *

# aux fun

def search(l,f):
  for x in l:
    if f(x):
      return x
  return False

# operators

def swap(state,a,b):
  ret = False
  if a!=b and (a in state.pos) and (b in state.pos):
    tmp = state.pos[a]
    state.pos[a] = state.pos[b]
    state.pos[b] = tmp
    ret = state
  return ret

declare_operators(swap)

# methods

def sortall_m(state,goal):
  subtasks = [('sort_one', x, goal.pos[x]) for x in goal.pos]
  return subtasks

declare_methods('sort_all', sortall_m)

def letbe_m(state, x, pos):
  return [] if state.pos[x] == pos else False

def sort1_m(state, x, pos):
  y = search(state.pos, lambda z: state.pos[z] == pos)
  return [('swap',x,y)] if y!=False else False

declare_methods('sort_one',letbe_m,sort1_m)

state1 = State('state1')
state1.pos = {'d':1, 'a':2, 'c':3, 'b':4}

goal1 = Goal('goal1')
goal1.pos = {'a':1, 'b':2, 'c':3, 'd':4}

pyhop(state1,[('sort_all', goal1)], verbose=3)
