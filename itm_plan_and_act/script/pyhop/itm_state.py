from __future__ import print_function
import pyhop
import pickle as pkl
from utils.mtn_planning import transform_list_to_pose_stamped
from math import pi
from sys import stderr

def eprint(*args, **kwargs):
    print(*args, file=stderr, **kwargs)



cavity_joints = {
        'star5': [-0.3199490828357856, 1.9525684363477929, 1.779626272025343, 1.3848433899491424, 1.1439935715886165, 1.4171336646392643, -1.0083884583638305],
        'pentagon': [-0.4308213533234442, 1.2847551376272177, 1.4891077316371444, 1.8874728387838857, 1.012766866407631, -1.3451868671199758, -0.9509576932794133],
        'triangle': [-0.44903682570251124, 1.7135210073923979, 1.5134356309770487, 1.996719615575845, -0.6463030936178153, -1.5361430814491097, -0.9437270497183752],
        'cross': [-0.017371101946210388, 0.7914051302204221, 0.2864967329475556, 1.9195397534639937, 0.7535527186205213, -0.4447593885841119, -2.595972302279913],
        'circularsector': [0.2714993348256769, 0.7729422640353274, -0.19565561616708305, 1.9095156523895902, 0.008118588981235852, -0.2855596626258041, 2.091264131074137],
        'star6': [0.6301032907331221, 0.9182417503894618, -0.8639484054558038, 1.8922759118812387, -0.8961176513306395, -0.5582065939425931, 0.7244809038828937],
        'pentagon': [0.4778835545039133, 0.8499923360841155, -0.5532656015724502, 1.910371486817148, -0.6149267107677652, -0.38101122210485716, 1.530191463262721],
        'trapezoid': [0.7988392714896095, 1.8530265760948037, -1.7116302780291512, 2.013367834885006, 0.6575511773277354, -1.0790546988125245, 0.9725109331222086],
        'trapezium': [0.7390182591413588, 1.428103698494282, -1.091192261391308, 2.0144850990544967, 0.6685358912872754, -1.5414462236955764, 2.864920081035933],
        'hexagon': [0.726560762229087, 1.4122818250553202, -1.0834662141144573, 2.0308839899743845, 0.6629249213686315, -1.5412533542997284, -2.1633843317798416],
        'star8': [0.6192182874115941, 0.9306843390924593, -0.851056681161162, 1.8948263099584501, -0.8880845636397368, -0.5641817621097509, -1.8390643420715769],
}

class ItmState(pyhop.State):
    def get_grasp_pose(self, object_, pre=False):
        object_location = self.objects[object_]
        pose = transform_list_to_pose_stamped(*object_location)
        if pre: pose.pose.position.z += 0.1
        return ("target6d", "iri_wam_picker_gripper_tcp", pose)

    def get_ungrasp_pose(self, location, pre=False):
        if location == 'sphere-insert':
            pose = transform_list_to_pose_stamped([0.7, 0.1, 0.08, 0, pi, 0],
                    "iri_wam_catcher_link_base")
        elif location == 'trash':
            pose = transform_list_to_pose_stamped([0.093897, 0.668034, 0.8, 0, pi, 0],
                    'world')
        elif location == 'place-location':
            pose = transform_list_to_pose_stamped(self.place_location)
        else: assert False
        if pre: pose.pose.position.z += 0.1
        return ("target6d", "iri_wam_picker_gripper_tcp", pose)

    def get_camera_closer_look_pose(self):
        pose = transform_list_to_pose_stamped([0, 0, 0.35, 0, pi, 0],
                "camera_rgb_optical_frame")
        return ("target6d", "iri_wam_picker_gripper_tcp", pose)

    def get_push_pose(self, object_, phase=0):
        object_location = self.objects[object_]
        pose = transform_list_to_pose_stamped(*object_location)
        pose.pose.position.x -= 0.01
        if phase < 2: pose.pose.position.y -= 0.10
        else: pose.pose.position.y += 0.05
        if phase == 0 or phase == 3: pose.pose.position.z += 0.1
        else: pose.pose.position.z += 0.02
        pose.pose.orientation.x = 0
        pose.pose.orientation.y = -0.996
        pose.pose.orientation.z = 0
        pose.pose.orientation.w = -0.0872
        return ('target6d', 'iri_wam_picker_link_tcp', pose)

    def get_pose(self, *args):
        pose_name = args[0]
        if pose_name == 'grasp':
            return self.get_grasp_pose(args[1])
        elif pose_name == 'pre-grasp':
            return self.get_grasp_pose(args[1], pre=True)
        elif pose_name == 'ungrasp':
            return self.get_ungrasp_pose(args[1])
        elif pose_name == 'pre-ungrasp':
            return self.get_ungrasp_pose(args[1], pre=True)
        elif pose_name == 'camera-closer-look':
            return self.get_camera_closer_look_pose() 
        elif pose_name == 'show-cavity':
            return ('joints', cavity_joints[args[1]])
        elif pose_name == 'home':
            return ('joints', [0, 0.10, 0, 2.90, 0, 0, 0])
        elif pose_name == 'push':
            return self.get_push_pose(*args[1:])
        else:
            assert False



def save_state(state, filename="statedump.pkl"):
    with open(filename, 'wb') as f:
        pkl.dump(state, f)


def load_state(filename="statedump.pkl", default=None):
    try:
        with open(filename, 'rb') as f:
            print("Found {}. Loading...".format(filename))
            state = pkl.load(f)
    except IOError:
        print("{} not found. Using default...".format(filename))
        state = default
    return state


