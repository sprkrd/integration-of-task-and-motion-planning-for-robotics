#!/usr/bin/env python

import rospy
import actionlib_msgs
import actionlib
import time
import tf2_geometry_msgs
import numpy as np
import random
from utils.mtn_planning import MotionPlanner, transform_list_to_pose_stamped, transform_pose_stamped_to_list
from utils.aclient import observe, get_true_shape
from utils.math_functions import scale_matrix, ml_shape


class State:
    def __init__(self, available_shapes):
        self.available_shapes = available_shapes
        self.mistakes = 0
        self.elapsed = 0

def close_look(state):
    mtn.show_gripper_to_camera()
    near_scene = observe('near', 3)
    if len(near_scene.avg_similitudes) != 10: return False
    similitudes_row = np.array(near_scene.avg_similitudes)
    similitudes_row = similitudes_row[state.available_shapes_columns]
    state.avg_similitudes[state.holding, :] = similitudes_row
    return state
    

def insert_piece(state, cautious=False):
    mtn = MotionPlanner.instance()
    start = rospy.Time.now()
    scene = observe('scene', 3)
    state.available_shapes_columns = [scene.shape_names.index(shape_n) for shape_n in state.available_shapes]
    state.avg_similitudes = np.reshape(scene.avg_similitudes, (-1, 10))
    state.avg_similitudes = state.avg_similitudes[:,state.available_shapes_columns]
    assert state.avg_similitudes.shape[0] == state.avg_similitudes.shape[1]
    blob_idx = random.randrange(state.avg_similitudes.shape[0])
    # blob_idx, shape_idx, shape = ml_shape(state.available_shapes, probabilities)
    theta_pick = scene.avg_pca_orientations[blob_idx]
    centroid_s = transform_list_to_pose_stamped([0.0, 0.0, 0.0, 0.0, 0.0, theta_pick],
            "camera_rgb_optical_frame")
    centroid_s.pose.position = scene.centroids[blob_idx]
    centroid_w = mtn._make_parallel_to_xy_plane(centroid_s)
    centroid_w.pose.position.z -= 15e-3
    insert_point = transform_list_to_pose_stamped([0.7, 0.1, 0.1, 0, np.pi, 0],
            "iri_wam_catcher_link_base")
    # print insert_point
    mtn.pick(centroid_w)
    state.holding = blob_idx
    if cautious:
        state = close_look(state)
    probabilities = scale_matrix(state.avg_similitudes.copy())
    shape_idx, shape = ml_shape(state.available_shapes, probabilities[blob_idx, :])
    true_shape = get_true_shape()
    if true_shape != shape:
        rospy.logwarn('Made a mistake! Guess: {}; Truth: {}'.format(shape, true_shape))
        state.mistakes += 1
    mtn.go_home('picker')
    mtn.show_cavity(shape)
    mtn.place(insert_point)
    # mtn.go_home('both')
    mtn.go_home('picker')
    mtn.go_home('catcher')
    state.holding = None
    del state.available_shapes[shape_idx]
    time.sleep(0.05)
    state.elapsed += (rospy.Time.now() - start).to_sec()
    rospy.logwarn('Total time until now: {}'.format(state.elapsed))
    return state


if __name__ == '__main__':
    rospy.init_node("wo_planning_test")
    mtn = MotionPlanner.instance()
    mtn.go_home('both')
    # state = State(['star5', 'star6', 'hexagon', 'star8', 'triangle'])
    state = State(['star5', 'star6', 'star8', 'triangle', 'hexagon', 'trapezium', 'trapezoid', 'pentagon', 'cross', 'circularsector'])
    random.seed(42)
    while state.available_shapes:
        state = insert_piece(state, cautious=False)
    rospy.loginfo('Elapsed since begining (sim time): {}s'.format(state.elapsed))
    rospy.loginfo('#Mistakes: {}'.format(state.mistakes))

