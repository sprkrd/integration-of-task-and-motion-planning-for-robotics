#!/usr/bin/env python

import rospy
import actionlib_msgs
import actionlib
import time
import tf2_geometry_msgs
import numpy as np
import random
from utils.mtn_planning import MotionPlanner, transform_list_to_pose_stamped, transform_pose_stamped_to_list
from utils.aclient import observe

def pick_place_test():
    mtn = MotionPlanner.instance()
    scene = observe('scene', 1)
    blob_idx = random.randrange(len(scene.centroids))
    theta_pick = scene.avg_pca_orientations[blob_idx]
    centroid_s = transform_list_to_pose_stamped([0.0, 0.0, 0.0, 0.0, 0.0, theta_pick],
            "camera_rgb_optical_frame")
    centroid_s.pose.position = scene.centroids[blob_idx]
    centroid_w = mtn._make_parallel_to_xy_plane(centroid_s)
    centroid_w.pose.position.z += 15e-3
    mtn.pick(centroid_w)
    centroid_w.pose.position.x -= 100e-3
    centroid_w.pose.position.y -= 100e-3
    mtn.place(centroid_w)


if __name__ == '__main__':
    rospy.init_node("pick_place_test")
    mtn = MotionPlanner.instance()
    mtn.go_home('both')
    pick_place_test()

