#!/usr/bin/env python

import roslib
import rospy
import actionlib_msgs
import actionlib
import sys
import getopt
import csv
import time
import numpy as np
import utils
import tf2_geometry_msgs
from motion_planning_utils import MoveItWrapper, MtnException

from geometry_msgs.msg import Point, PointStamped, PoseStamped


def check_ik_all():
    motion = MoveItWrapper.instance()
    can_transform = motion._tf_buffer.can_transform("world",
            "iri_wam_picker_link_base", rospy.Time(),
            rospy.Duration.from_sec(60))
    assert can_transform
    scene = utils.observe_scene_client()
    for theta, centroid in zip(scene.avg_pca_orientations, scene.centroids):
        centroid_stamped = PoseStamped()
        centroid_stamped.pose.position = centroid
        centroid_stamped.pose.position.z += 0.005
        centroid_stamped.pose.orientation.x = 0
        centroid_stamped.pose.orientation.y = 0
        centroid_stamped.pose.orientation.z = np.sin(theta)
        centroid_stamped.pose.orientation.w = np.cos(theta)
        centroid_stamped.header.frame_id = "camera_rgb_optical_frame"
        centroid_stamped = motion._tf_buffer.transform(centroid_stamped,
                "world")
        try:
            motion.get_ik(centroid_stamped, "picker", timeout=1e-1, avoid_collisions=False)
        except MtnException as e:
            if e.reason == "NO_IK_SOLUTION":
                rospy.logerr("No IK solution (w/o avoid_collisions) for: {}".format(centroid_stamped))
        try:
            motion.get_ik(centroid_stamped, "picker", timeout=1e-1, avoid_collisions=True)
        except MtnException as e:
            if e.reason == "NO_IK_SOLUTION":
                rospy.logerr("No IK solution (w avoid_collisions) for: {}".format(centroid_stamped))

if __name__ == '__main__':
    rospy.init_node("wo_planning_test")
    check_ik_all()

