#include "wam_catcher_actions_alg.h"

#include <fstream>

#include <ros/package.h>
#include <tf/transform_datatypes.h>

WamCatcherActionsAlgorithm::WamCatcherActionsAlgorithm(void)
{
  pthread_mutex_init(&this->access_,NULL);

  /* Read joints dictionary from file */
  std::string pkg_path = ros::package::getPath("iri_wam_catcher_actions");
  std::string cnf_path = pkg_path + "/cfg/joints_dictionary.cfg";
  ROS_INFO_STREAM("Reading " << cnf_path);
  std::ifstream fin(cnf_path.c_str(),std::fstream::in);
  std::string position;
  while (fin >> position)
  {
    joints_dict_[position] = std::vector<double>(7);
    for (int i = 0; i < 7; ++i) fin >> joints_dict_[position][i];
  }
}

WamCatcherActionsAlgorithm::~WamCatcherActionsAlgorithm(void)
{
  pthread_mutex_destroy(&this->access_);
}

bool WamCatcherActionsAlgorithm::obtainJoints(const std::string& position,
    Joints& joints) const
{
  typedef std::map<std::string,Joints>::const_iterator CtrIterator;
  /* Try to find shape. If not found, print error msg and return false. */
  CtrIterator joints_it = joints_dict_.find(position);
  if (joints_it == joints_dict_.end())
  {
    ROS_ERROR_STREAM("Invalid position: " << position);
    return false;
  }
  joints = joints_it->second;
  return true;
}

void WamCatcherActionsAlgorithm::config_update(Config& config, uint32_t level)
{
  this->lock();

  // save the current configuration
  this->config_=config;
  
  this->unlock();
}

// WamCatcherActionsAlgorithm Public API
