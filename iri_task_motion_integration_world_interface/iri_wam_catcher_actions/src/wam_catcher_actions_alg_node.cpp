#include "wam_catcher_actions_alg_node.h"

WamCatcherActionsAlgNode::WamCatcherActionsAlgNode(void) :
  algorithm_base::IriBaseAlgorithm<WamCatcherActionsAlgorithm>(),
  adopt_position_aserver_(public_node_handle_, "adopt_position")
{
  //init class attributes if necessary
  this->loop_rate_ = 10; /* in [Hz] */

  // [init publishers]
  
  // [init subscribers]
  
  // [init services]
  
  // [init clients]
  joints_move_client_ = this->public_node_handle_
    .serviceClient<iri_common_drivers_msgs::QueryJointsMovement>("joints_move");

  get_wam_ik_client_ = this->public_node_handle_
    .serviceClient<iri_common_drivers_msgs::QueryInverseKinematics>("get_wam_ik");

  
  // [init action servers]
  adopt_position_aserver_.registerStartCallback(boost::bind(
        &WamCatcherActionsAlgNode::adopt_positionStartCallback, this, _1));
  adopt_position_aserver_.registerStopCallback(boost::bind(
        &WamCatcherActionsAlgNode::adopt_positionStopCallback, this));
  adopt_position_aserver_.registerIsFinishedCallback(boost::bind(
        &WamCatcherActionsAlgNode::adopt_positionIsFinishedCallback, this));
  adopt_position_aserver_.registerHasSucceedCallback(boost::bind(
        &WamCatcherActionsAlgNode::adopt_positionHasSucceededCallback, this));
  adopt_position_aserver_.registerGetResultCallback(boost::bind(
        &WamCatcherActionsAlgNode::adopt_positionGetResultCallback, this, _1));
  adopt_position_aserver_.registerGetFeedbackCallback(boost::bind(
        &WamCatcherActionsAlgNode::adopt_positionGetFeedbackCallback,
        this, _1));
  adopt_position_aserver_.start();
  this->adopt_position_active_ = false;
  this->adopt_position_ok_ = false;
  this->adopt_position_finished_ = false;

  
  // [init action clients]
}

WamCatcherActionsAlgNode::~WamCatcherActionsAlgNode(void)
{
  // [free dynamic memory]
}

void WamCatcherActionsAlgNode::mainNodeThread(void)
{
  // [fill msg structures]
  
  // [fill action structure and make request to the action server]
  
  if (adopt_position_active_)
  {
    /* Try to retrieve joints */
    adopt_position_info_ = "Going to " + adopt_position_goal_;
    adopt_position_ok_ = alg_.obtainJoints(adopt_position_goal_,
        joints_move_srv_.request.positions);
    if (adopt_position_ok_)
    {
      /* Try to move to position */
      joints_move_srv_.request.velocity = config_.max_v;
      joints_move_srv_.request.acceleration = config_.max_a;
      adopt_position_ok_ = joints_move_client_.call(joints_move_srv_);
      adopt_position_info_ = adopt_position_ok_?
        ("I am now in " + adopt_position_goal_) :
        ("Failure calling " + joints_move_client_.getService());
    }
    else adopt_position_info_ = "Unknown position: " + adopt_position_goal_;
    adopt_position_active_ = false;
    adopt_position_finished_ = true;
  }
  
  // [publish messages]
}



/*  [subscriber callbacks] */

/*  [service callbacks] */

/*  [action callbacks] */
void WamCatcherActionsAlgNode::adopt_positionStartCallback(const iri_world_interface_msgs::AdoptPositionGoalConstPtr& goal)
{
  this->alg_.lock();
  //check goal
  adopt_position_goal_ = goal->position;
  this->adopt_position_active_ = true;
  this->adopt_position_ok_ = false;
  this->adopt_position_finished_ = false;
  //execute goal
  this->alg_.unlock();
}

void WamCatcherActionsAlgNode::adopt_positionStopCallback(void)
{
  this->alg_.lock();
  //stop action
  this->adopt_position_active_ = false;
  this->alg_.unlock();
}

bool WamCatcherActionsAlgNode::adopt_positionIsFinishedCallback(void)
{
  bool ret = false;

  this->alg_.lock();
  //if action has finish for any reason
  ret = this->adopt_position_finished_;
  this->alg_.unlock();

  return ret;
}

bool WamCatcherActionsAlgNode::adopt_positionHasSucceededCallback(void)
{
  bool ret = false;

  this->alg_.lock();
  //if goal was accomplished
  ret = this->adopt_position_ok_;
  this->adopt_position_active_ = false;
  this->alg_.unlock();

  return ret;
}

void WamCatcherActionsAlgNode::adopt_positionGetResultCallback(iri_world_interface_msgs::AdoptPositionResultPtr& result)
{
  this->alg_.lock();
  result->success = adopt_position_ok_;
  result->info = adopt_position_info_;
  adopt_position_info_ = "";
  adopt_position_goal_ = "";
  this->alg_.unlock();
}

void WamCatcherActionsAlgNode::adopt_positionGetFeedbackCallback(
    iri_world_interface_msgs::AdoptPositionFeedbackPtr& feedback)
{
  this->alg_.lock();
  feedback->info = adopt_position_info_;
  ROS_INFO("feedback: %s",adopt_position_info_.c_str());
  this->alg_.unlock();
}


/*  [action requests] */

void WamCatcherActionsAlgNode::node_config_update(Config &config, uint32_t level)
{
  this->alg_.lock();
  this->config_=config;
  joints_move_srv_.request.velocity = config_.max_v;
  joints_move_srv_.request.acceleration = config_.max_a;
  this->alg_.unlock();
}

void WamCatcherActionsAlgNode::addNodeDiagnostics(void)
{
}

/* main function */
int main(int argc,char *argv[])
{
  return algorithm_base::main<WamCatcherActionsAlgNode>(argc, argv, "wam_catcher_actions_alg_node");
}
