#include "world_interface_alg.h"

WorldInterfaceAlgorithm::WorldInterfaceAlgorithm(void)
{
  pthread_mutex_init(&this->access_,NULL);
}

WorldInterfaceAlgorithm::~WorldInterfaceAlgorithm(void)
{
  pthread_mutex_destroy(&this->access_);
}

void WorldInterfaceAlgorithm::config_update(Config& config, uint32_t level)
{
  this->lock();

  // save the current configuration
  this->config_=config;
  
  this->unlock();
}

// WorldInterfaceAlgorithm Public API
