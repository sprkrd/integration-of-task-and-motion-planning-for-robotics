// Copyright (C) 2010-2011 Institut de Robotica i Informatica Industrial, CSIC-UPC.
// Author 
// All rights reserved.
//
// This file is part of iri-ros-pkg
// iri-ros-pkg is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
// 
// IMPORTANT NOTE: This code has been generated through a script from the 
// iri_ros_scripts. Please do NOT delete any comments to guarantee the correctness
// of the scripts. ROS topics can be easly add by using those scripts. Please
// refer to the IRI wiki page for more information:
// http://wikiri.upc.es/index.php/Robotics_Lab

#ifndef _wam_picker_actions_alg_node_h_
#define _wam_picker_actions_alg_node_h_

#include <random_numbers/random_numbers.h>
#include <tf/transform_listener.h>
#include <tf/transform_datatypes.h>

#include <iri_base_algorithm/iri_base_algorithm.h>
#include "wam_picker_actions_alg.h"

// [publisher subscriber headers]

// [service client headers]
#include <iri_common_drivers_msgs/QueryInverseKinematics.h>
#include <iri_common_drivers_msgs/QueryJointsMovement.h>

// [action server client headers]
#include <random_numbers/random_numbers.h>
#include <iri_world_interface_msgs/AdoptPositionAction.h>
#include <iri_common_drivers_msgs/tool_closeAction.h>
#include <actionlib/client/simple_action_client.h>
#include <actionlib/client/terminal_state.h>
#include <iri_common_drivers_msgs/tool_openAction.h>
#include <iri_action_server/iri_action_server.h>
#include <iri_world_interface_msgs/PickOrPlacePieceAction.h>

/**
 * \brief IRI ROS Specific Algorithm Class
 *
 */
class WamPickerActionsAlgNode : public algorithm_base::IriBaseAlgorithm<WamPickerActionsAlgorithm>
{
  private:
    /* For generating random trajectories. */
    random_numbers::RandomNumberGenerator rand_;

    /* We want to be able of transforming from one frame of reference to
     * another, so we declare a tf listener. */
    tf::TransformListener tf_listener_;

    // [publisher attributes]

    // [subscriber attributes]

    // [service attributes]

    // [client attributes]
    ros::ServiceClient get_wam_ik_client_;
    iri_common_drivers_msgs::QueryInverseKinematics get_wam_ik_srv_;

    ros::ServiceClient joints_move_client_;
    iri_common_drivers_msgs::QueryJointsMovement joints_move_srv_;


    // [action server attributes]
    IriActionServer<iri_world_interface_msgs::AdoptPositionAction>
      assume_position_aserver_;
    void assume_positionStartCallback(
        const iri_world_interface_msgs::AdoptPositionGoalConstPtr& goal);
    void assume_positionStopCallback(void);
    bool assume_positionIsFinishedCallback(void);
    bool assume_positionHasSucceededCallback(void);
    void assume_positionGetResultCallback(
        iri_world_interface_msgs::AdoptPositionResultPtr& result);
    void assume_positionGetFeedbackCallback(
        iri_world_interface_msgs::AdoptPositionFeedbackPtr& feedback);
    std::string assume_position_info_;
    std::string next_position_;

    /* Control variables */
    bool assume_position_active_;
    bool assume_position_ok_;
    bool assume_position_finished_;

    IriActionServer<iri_world_interface_msgs::PickOrPlacePieceAction>
      pick_or_place_piece_aserver_;
    void pick_or_place_pieceStartCallback(
        const iri_world_interface_msgs::PickOrPlacePieceGoalConstPtr& goal);
    void pick_or_place_pieceStopCallback(void);
    bool pick_or_place_pieceIsFinishedCallback(void);
    bool pick_or_place_pieceHasSucceededCallback(void);
    void pick_or_place_pieceGetResultCallback(
        iri_world_interface_msgs::PickOrPlacePieceResultPtr& result);
    void pick_or_place_pieceGetFeedbackCallback(
        iri_world_interface_msgs::PickOrPlacePieceFeedbackPtr& feedback);

    /* Info about the state of the action */
    std::string porp_info_;

    /* Current goal of the pick or place piece action */
    iri_world_interface_msgs::PickOrPlacePieceGoalConstPtr porp_goal_;
    int porp_step_;
    geometry_msgs::PoseStamped pregrasp_pose_;
    geometry_msgs::PoseStamped grasp_pose_;
    std::vector<double> joints_pregrasp_pose_;
    std::vector<double> joints_grasp_pose_;
    /* Control variables */
    bool gripper_open_;
    bool pick_or_place_piece_ok_;
    bool pick_or_place_piece_finished_;

    /* Auxiliary methods */
    bool obtainJoints(std::vector<double>& joints,
        const geometry_msgs::PoseStamped& pose_stmp);
    bool moveToJoints(const std::vector<double>& joints, double velocity,
        double acceleration);

    // [action client attributes]
    actionlib::SimpleActionClient<iri_common_drivers_msgs::tool_closeAction>
      close_tool_client_;
    iri_common_drivers_msgs::tool_closeGoal close_tool_goal_;
    bool close_toolMakeActionRequest();
    void close_toolDone(
        const actionlib::SimpleClientGoalState& state, 
        const iri_common_drivers_msgs::tool_closeResultConstPtr& result);
    void close_toolActive();
    void close_toolFeedback(
        const iri_common_drivers_msgs::tool_closeFeedbackConstPtr& feedback);

    actionlib::SimpleActionClient<iri_common_drivers_msgs::tool_openAction>
      open_tool_client_;
    iri_common_drivers_msgs::tool_openGoal open_tool_goal_;
    bool open_toolMakeActionRequest();
    void open_toolDone(const actionlib::SimpleClientGoalState& state, 
        const iri_common_drivers_msgs::tool_openResultConstPtr& result);
    void open_toolActive();
    void open_toolFeedback(
        const iri_common_drivers_msgs::tool_openFeedbackConstPtr& feedback);


   /**
    * \brief config variable
    *
    * This variable has all the driver parameters defined in the cfg config file.
    * Is updated everytime function config_update() is called.
    */
    Config config_;
  public:
   /**
    * \brief Constructor
    * 
    * This constructor initializes specific class attributes and all ROS
    * communications variables to enable message exchange.
    */
    WamPickerActionsAlgNode(void);

   /**
    * \brief Destructor
    * 
    * This destructor frees all necessary dynamic memory allocated within this
    * this class.
    */
    ~WamPickerActionsAlgNode(void);

  protected:
   /**
    * \brief main node thread
    *
    * This is the main thread node function. Code written here will be executed
    * in every node loop while the algorithm is on running state. Loop frequency 
    * can be tuned by modifying loop_rate attribute.
    *
    * Here data related to the process loop or to ROS topics (mainly data structs
    * related to the MSG and SRV files) must be updated. ROS publisher objects 
    * must publish their data in this process. ROS client servers may also
    * request data to the corresponding server topics.
    */
    void mainNodeThread(void);

   /**
    * \brief dynamic reconfigure server callback
    * 
    * This method is called whenever a new configuration is received through
    * the dynamic reconfigure. The derivated generic algorithm class must 
    * implement it.
    *
    * \param config an object with new configuration from all algorithm 
    *               parameters defined in the config file.
    * \param level  integer referring the level in which the configuration
    *               has been changed.
    */
    void node_config_update(Config &config, uint32_t level);

   /**
    * \brief node add diagnostics
    *
    * In this abstract function additional ROS diagnostics applied to the 
    * specific algorithms may be added.
    */
    void addNodeDiagnostics(void);

    // [diagnostic functions]
    
    // [test functions]
};

#endif
