#include "wam_picker_actions_alg.h"

#include <tf/transform_datatypes.h>

namespace
{

const double PI = 3.14159265358979;

} /* end unnamed namespace */

WamPickerActionsAlgorithm::WamPickerActionsAlgorithm(void)
{
  pthread_mutex_init(&this->access_,NULL);
}

WamPickerActionsAlgorithm::~WamPickerActionsAlgorithm(void)
{
  pthread_mutex_destroy(&this->access_);
}

void WamPickerActionsAlgorithm::config_update(Config& config, uint32_t level)
{
  this->lock();

  // save the current configuration
  this->config_=config;
  
  this->unlock();
}

// WamPickerActionsAlgorithm Public API
