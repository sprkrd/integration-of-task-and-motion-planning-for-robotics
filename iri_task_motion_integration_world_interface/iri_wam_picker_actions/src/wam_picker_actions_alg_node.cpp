#include "wam_picker_actions_alg_node.h"

#include <ros/assert.h>

namespace
{

const double PI = 3.14159265358979;

}

WamPickerActionsAlgNode::WamPickerActionsAlgNode(void) :
  algorithm_base::IriBaseAlgorithm<WamPickerActionsAlgorithm>(),
  assume_position_aserver_(public_node_handle_, "assume_position"),
  close_tool_client_("close_tool", true),
  open_tool_client_("open_tool", true),
  pick_or_place_piece_aserver_(public_node_handle_, "pick_or_place_piece"),
  tf_listener_(public_node_handle_)
{
  //init class attributes if necessary
  this->loop_rate_ = 10; //in [Hz]

  // [init publishers]
  
  // [init subscribers]
  
  // [init services]
  
  // [init clients]
  get_wam_ik_client_ = this->public_node_handle_
    .serviceClient<iri_common_drivers_msgs::QueryInverseKinematics>(
        "get_wam_ik");

  joints_move_client_ = this->public_node_handle_
    .serviceClient<iri_common_drivers_msgs::QueryJointsMovement>(
        "joints_move");

  
  // [init action servers]
  assume_position_aserver_.registerStartCallback(boost::bind(
        &WamPickerActionsAlgNode::assume_positionStartCallback, this, _1));
  assume_position_aserver_.registerStopCallback(boost::bind(
        &WamPickerActionsAlgNode::assume_positionStopCallback, this));
  assume_position_aserver_.registerIsFinishedCallback(boost::bind(
        &WamPickerActionsAlgNode::assume_positionIsFinishedCallback, this));
  assume_position_aserver_.registerHasSucceedCallback(boost::bind(
        &WamPickerActionsAlgNode::assume_positionHasSucceededCallback, this));
  assume_position_aserver_.registerGetResultCallback(boost::bind(
        &WamPickerActionsAlgNode::assume_positionGetResultCallback, this, _1));
  assume_position_aserver_.registerGetFeedbackCallback(boost::bind(
        &WamPickerActionsAlgNode::assume_positionGetFeedbackCallback, this, _1));
  assume_position_aserver_.start();
  this->assume_position_active_=false;
  this->assume_position_ok_=false;
  this->assume_position_finished_=false;

  pick_or_place_piece_aserver_.registerStartCallback(boost::bind(
        &WamPickerActionsAlgNode::pick_or_place_pieceStartCallback, this, _1));
  pick_or_place_piece_aserver_.registerStopCallback(boost::bind(
        &WamPickerActionsAlgNode::pick_or_place_pieceStopCallback, this));
  pick_or_place_piece_aserver_.registerIsFinishedCallback(boost::bind(
        &WamPickerActionsAlgNode::pick_or_place_pieceIsFinishedCallback, this));
  pick_or_place_piece_aserver_.registerHasSucceedCallback(boost::bind(
        &WamPickerActionsAlgNode::pick_or_place_pieceHasSucceededCallback, this));
  pick_or_place_piece_aserver_.registerGetResultCallback(boost::bind(
        &WamPickerActionsAlgNode::pick_or_place_pieceGetResultCallback, this, _1));
  pick_or_place_piece_aserver_.registerGetFeedbackCallback(boost::bind(
        &WamPickerActionsAlgNode::pick_or_place_pieceGetFeedbackCallback, this, _1));
  pick_or_place_piece_aserver_.start();
  gripper_open_ = false;
  this->porp_step_ = 0;
  this->pick_or_place_piece_ok_=false;
  this->pick_or_place_piece_finished_=false;

  
  // [init action clients]
}

WamPickerActionsAlgNode::~WamPickerActionsAlgNode(void)
{
  // [free dynamic memory]
}

void WamPickerActionsAlgNode::mainNodeThread(void)
{
  // [fill msg structures]
  
  // [fill srv structure and make request to the server]
  
  // [fill action structure and make request to the action server]
  
//   if (pick_or_place_piece_active_)
//     executePickOrPlaceAction();

  /* --- Steps of pick_or_place_piece --- */

  if (porp_step_ == 1)
  {
    /* Compute grasp/ungrasp pose in joints */
    ROS_ASSERT(porp_goal_);
    porp_info_ = "Obtaining pose in robot's link base frame";

    /* Transform (un)grasp point to robot's frame */
    geometry_msgs::PointStamped point_alien_frame = porp_goal_->centroid;
    point_alien_frame.header.stamp = ros::Time(0);
    geometry_msgs::PointStamped point_robot_frame;
    porp_info_ = "Waiting for transform between " +
      point_alien_frame.header.frame_id + " and " + config_.robot_base_frame;

    tf_listener_.waitForTransform(config_.robot_base_frame,
        point_alien_frame.header.frame_id,ros::Time(0),ros::Duration(100));
    ROS_INFO("HEY");
    tf_listener_.transformPoint(config_.robot_base_frame,point_alien_frame,
        point_robot_frame);
    ROS_INFO("BYE");
    /* Obtain (un)grasp pose in cartesian coordinates */
    grasp_pose_.header = point_robot_frame.header;
    grasp_pose_.pose.position = point_robot_frame.point;
    grasp_pose_.pose.position.z -= 0.02; // ugly
    grasp_pose_.pose.orientation = tf::createQuaternionMsgFromRollPitchYaw(
        0,PI,porp_goal_->most_likely_rotation*(PI/180));
    /* Pre(un)grasp is the same but with the z increased. */
    pregrasp_pose_ = grasp_pose_;
    pregrasp_pose_.pose.position.z += config_.pre_grasp_height;

    /* Calculate the IK of the pre(un)grasp pose and store the joints. */
    pick_or_place_piece_ok_ = obtainJoints(joints_pregrasp_pose_,
        pregrasp_pose_);
    porp_info_ = "Computing IK of pre(un)grasp pose";
    if (pick_or_place_piece_ok_) porp_step_ = 2;
    else
    {
      porp_info_ = "Couldn't compute IK of pre(un)grasp pose";
      porp_step_ = 9;
    }
  }
  else if (porp_step_ == 2)
  {
    /* Go to pre(un)grasp pose at max speed*/
    ROS_ASSERT(porp_goal_ and joints_pregrasp_pose_.size() == 7);
    porp_info_ = porp_goal_->pick? "Going to pre-grasp position" :
      "Going to pre-ungrasp position";

    pick_or_place_piece_ok_ =
      moveToJoints(joints_pregrasp_pose_,config_.max_v,config_.max_a);

    if (pick_or_place_piece_ok_) porp_step_ = 3;
    else
    {
      porp_step_ = 9;
      porp_info_ = porp_goal_->pick? "Error going to pre-grasp position" :
        "Error going to pre-ungrasp position";
    }
  }
  else if (porp_step_ == 3)
  {
    /* Compute IK of the (un)grasp pose. It would be ideal to compute it
     * from the beginning, but we do it now so the IK solver takes as current
     * pose the pre(un)grasp pose and doesn't perform weird twists. */
    porp_info_ = "Computing IK of grasp pose";
    pick_or_place_piece_ok_ = obtainJoints(joints_grasp_pose_,grasp_pose_);
    if (pick_or_place_piece_ok_) porp_step_ = porp_goal_->pick? 4 : 5; 
    else
    {
      porp_info_ = "Couldn't compute IK of (un)grasp pose";
      porp_step_ = 9;
    }
  }
  else if (porp_step_ == 4)
  {
    /* Open gripper */

    /* [TO-DO] We should cancel the action somehow if the open-tool action
     * fails or does not complete in a reasonable amount of time. This applies
     * for all the times we open-close the gripper. For now this will work. */
    ROS_ASSERT(porp_goal_ and porp_goal_->pick and joints_grasp_pose_.size() == 7);
    porp_info_ = "Opening gripper";
    pick_or_place_piece_ok_ = open_toolMakeActionRequest();
    if (pick_or_place_piece_ok_) porp_step_ = 5;
    else
    {
      porp_step_ = 9;
      porp_info_ = "Error calling action open_tool_action";
    }
  }
  else if (porp_step_ == 5 and (gripper_open_ or not porp_goal_->pick))
  {
    /* Go to (un)grasp pose */
    ROS_ASSERT(porp_goal_ and joints_grasp_pose_.size() == 7);
    porp_info_ = porp_goal_->pick? "Going to grasp pose" :
      "Going to ungrasp pose";
    pick_or_place_piece_ok_ =
      moveToJoints(joints_grasp_pose_,config_.v_near,config_.max_a);
    if (pick_or_place_piece_ok_) porp_step_ = 6;
    else
    {
      porp_step_ = 9;
      porp_info_ = porp_goal_->pick? "Error going to grasp position" :
        "Error going to ungrasp position";
    }
  }
  else if (porp_step_ == 6 and porp_goal_->pick)
  {
    /* Close gripper */
    ROS_ASSERT(porp_goal_);
    porp_info_ = "Closing gripper";
    pick_or_place_piece_ok_ = close_toolMakeActionRequest();
    if (pick_or_place_piece_ok_) porp_step_ = 7;
    else
    {
      porp_step_ = 9;
      porp_info_ = "Error calling action close_tool_action";
    }
  }
  else if (porp_step_ == 6 and not porp_goal_->pick)
  {
    /* Open gripper */
    ROS_ASSERT(porp_goal_);
    porp_info_ = "Opening gripper";
    pick_or_place_piece_ok_ = open_toolMakeActionRequest();
    if (pick_or_place_piece_ok_) porp_step_ = 7;
    else
    {
      porp_step_ = 9;
      porp_info_ = "Error calling action open_tool_action";
    }
  }
  else if (porp_step_ == 7 and ((porp_goal_->pick and not gripper_open_) or
        (not porp_goal_->pick) and gripper_open_))
  {
    /* Go to pre(un)grasp pose again */
    ROS_ASSERT(porp_goal_ and joints_pregrasp_pose_.size() == 7);
    porp_info_ = porp_goal_->pick? "Going to pregrasp pose" :
      "Going to preungrasp pose";
    pick_or_place_piece_ok_ =
      moveToJoints(joints_pregrasp_pose_,config_.max_v,config_.max_a);
    if (pick_or_place_piece_ok_) porp_step_ = porp_goal_->pick? 9 : 8;
    else
    {
      porp_step_ = 9;
      porp_info_ = porp_goal_->pick? "Error going to pregrasp position" :
        "Error going to preungrasp position";
    }
  }
  else if (porp_step_ == 8)
  {
    /* Close gripper */
    ROS_ASSERT(porp_goal_);
    porp_info_ = "Closing gripper";
    pick_or_place_piece_ok_ = close_toolMakeActionRequest();
    porp_step_ = 9;
    if (not pick_or_place_piece_ok_)
      porp_info_ = "Error calling action close_tool_action";
  }
  else if (porp_step_ == 9)
  {
    /* Clean data structures and set control variables */
    porp_goal_.reset();
    joints_pregrasp_pose_.clear();
    joints_grasp_pose_.clear();
    porp_step_ = 0;
    pick_or_place_piece_finished_ = true;
    if (pick_or_place_piece_ok_) /* The action went just fine. Say so. */
      porp_info_ = "Action executed successfully!";
  }

  /* --- Steps of assume_position ---*/

  if (assume_position_active_)
  {
    /* Obtain appropiate vector of joint values. */
    std::vector<double> joints(7,0);
    assume_position_info_ = "Going to " + next_position_ + " position";
    if (next_position_ == "show")
    {
      /* I know it is a bit ugly to hardcode the joint values in the code, but
      * for now, it will do. */
      joints[0] = -0.0020453077171808547;
      joints[1] = 1.3300835185578328;
      joints[2] = 0.29848563158763164;
      joints[3] = -0.25412948385972123;
      joints[4] = -0.38926739271963984;
      joints[5] = -0.8886809317034393;
      joints[6] = 0.08188765491928039;
    }
    else if (next_position_ != "neutral")
    {
      assume_position_ok_ = "false";
      assume_position_info_ = "Unknown position: " + next_position_;
    }

    if (assume_position_ok_)
    {
      /* Try moving to joints*/
      assume_position_ok_ = moveToJoints(joints,config_.max_v,config_.max_a);
      /* Set the assume_position_info_ to an appropiate message. */
      if (assume_position_ok_) assume_position_info_ = "Cool, I'm not in " +
        next_position_ + " position";
      else assume_position_info_ = "Error moving to " + next_position_ +
        " position";
    }

    /* Clear next_position_ and set control variables */
    next_position_.clear();
    assume_position_active_ = false;
    assume_position_finished_ = true;
  }

  // [publish messages]
}

bool WamPickerActionsAlgNode::obtainJoints(std::vector<double>& joints,
    const geometry_msgs::PoseStamped& pose_stmp)
{
  /* Fill request */
  get_wam_ik_srv_.request.pose = pose_stmp;
  /* Call service */
  bool ok= get_wam_ik_client_.call(get_wam_ik_srv_);
  /* Print appropriate log messages. */
  if (ok)
  {
    joints = get_wam_ik_srv_.response.joints.position;
    ROS_INFO("IK computation complete!");
  }
  else ROS_ERROR("Error calling %s",get_wam_ik_client_.getService().c_str());
  return ok;
}

bool WamPickerActionsAlgNode::moveToJoints(const std::vector<double>& joints,
    double velocity, double acceleration)
{
  /* Fill request. */
  joints_move_srv_.request.positions = joints;
  joints_move_srv_.request.velocity = velocity;
  joints_move_srv_.request.acceleration = acceleration;
  /* Call service */
  bool ok = joints_move_client_.call(joints_move_srv_);
  /* Print appropriate log messages. */
  if (ok) ROS_INFO("Movement in joints complete");
  else ROS_ERROR("Error calling %s",joints_move_client_.getService().c_str());
  return ok;
}

/*  [subscriber callbacks] */

/*  [service callbacks] */

/*  [action callbacks] */
void WamPickerActionsAlgNode::assume_positionStartCallback(
    const iri_world_interface_msgs::AdoptPositionGoalConstPtr& goal)
{
  this->alg_.lock();
  /* Store objective position */
  next_position_ = goal->position;
  /* assume_position_ok_ is true from the begining. Only set to false in case
   * of error. */
  this->assume_position_ok_ = true;
  this->assume_position_finished_=false;
  this->assume_position_active_ = true;
  //execute goal
  this->alg_.unlock();
}

void WamPickerActionsAlgNode::assume_positionStopCallback(void)
{
  this->alg_.lock();
  //stop action
  this->assume_position_active_ = false;
  this->alg_.unlock();
}

bool WamPickerActionsAlgNode::assume_positionIsFinishedCallback(void)
{
  bool ret = false;
  this->alg_.lock();
  //if action has finish for any reason
  ret = this->assume_position_finished_;
  this->alg_.unlock();
  return ret;
}

bool WamPickerActionsAlgNode::assume_positionHasSucceededCallback(void)
{
  bool ret = false;
  this->alg_.lock();
  //if goal was accomplished
  ret = this->assume_position_ok_;
  this->alg_.unlock();
  return ret;
}

void WamPickerActionsAlgNode::assume_positionGetResultCallback(
    iri_world_interface_msgs::AdoptPositionResultPtr& result)
{
  this->alg_.lock();
  result->success = assume_position_ok_;
  result->info = assume_position_info_;
  assume_position_info_.clear();
  this->alg_.unlock();
}

void WamPickerActionsAlgNode::assume_positionGetFeedbackCallback(
    iri_world_interface_msgs::AdoptPositionFeedbackPtr& feedback)
{
  this->alg_.lock();
  ROS_INFO_STREAM(assume_position_info_);
  feedback->info = assume_position_info_;
  this->alg_.unlock();
}

void WamPickerActionsAlgNode::close_toolDone(
    const actionlib::SimpleClientGoalState& state,
    const iri_common_drivers_msgs::tool_closeResultConstPtr& result)
{
  alg_.lock();
  if(state == actionlib::SimpleClientGoalState::SUCCEEDED)
  {
    gripper_open_ = false;
    ROS_INFO("WamPickerActionsAlgNode::close_toolDone: Goal Achieved!");
  }
  else
    ROS_INFO("WamPickerActionsAlgNode::close_toolDone: %s", state.toString().c_str());

  //copy & work with requested result
  alg_.unlock();
}

void WamPickerActionsAlgNode::close_toolActive()
{
  /* Don't do anything. */
}

void WamPickerActionsAlgNode::close_toolFeedback(
    const iri_common_drivers_msgs::tool_closeFeedbackConstPtr& feedback)
{
  /* Don't do anything. */
}

void WamPickerActionsAlgNode::open_toolDone(
    const actionlib::SimpleClientGoalState& state, 
    const iri_common_drivers_msgs::tool_openResultConstPtr& result)
{
  alg_.lock();
  if( state == actionlib::SimpleClientGoalState::SUCCEEDED )
  {
    ROS_INFO("Gripper is open now");
    gripper_open_ = true;
  }
  else
    ROS_INFO("Gripper has failed to open: %s", state.toString().c_str());

  alg_.unlock();
}

void WamPickerActionsAlgNode::open_toolActive()
{
  /* Don't do anything. */
}

void WamPickerActionsAlgNode::open_toolFeedback(
    const iri_common_drivers_msgs::tool_openFeedbackConstPtr& feedback)
{
  /* Don't do anything. */
}

void WamPickerActionsAlgNode::pick_or_place_pieceStartCallback(
    const iri_world_interface_msgs::PickOrPlacePieceGoalConstPtr& goal)
{
  this->alg_.lock();
  if (assume_position_active_ or porp_step_ != 0)
  {
    /* Only one action at a time */
    porp_step_ = 9;
    pick_or_place_piece_ok_ = false;
    pick_or_place_piece_finished_ = true;
  }
  else
  {
    this->porp_goal_ = goal;
    porp_step_ = 1;
    pick_or_place_piece_ok_ = true;
    pick_or_place_piece_finished_ = false;
  }
  this->alg_.unlock();
}

void WamPickerActionsAlgNode::pick_or_place_pieceStopCallback(void)
{
  this->alg_.lock();
  //stop action
  this->porp_step_ = 0;
  this->alg_.unlock();
}

bool WamPickerActionsAlgNode::pick_or_place_pieceIsFinishedCallback(void)
{
  return pick_or_place_piece_finished_;
  bool ret = false;

  this->alg_.lock();
  //if action has finish for any reason
  ret = this->pick_or_place_piece_finished_;
  this->alg_.unlock();

  return ret;
}

bool WamPickerActionsAlgNode::pick_or_place_pieceHasSucceededCallback(void)
{
  bool ret = false;

  this->alg_.lock();
  //if goal was accomplished
  ret = this->pick_or_place_piece_ok_;
  this->alg_.unlock();

  return ret;
}

void WamPickerActionsAlgNode::pick_or_place_pieceGetResultCallback(
    iri_world_interface_msgs::PickOrPlacePieceResultPtr& result)
{
  this->alg_.lock();
  //update result data to be sent to client
  result->info = porp_info_;
  result->success = pick_or_place_piece_ok_;
  /* Clear info */
  porp_info_ = "";
  this->alg_.unlock();
}

void WamPickerActionsAlgNode::pick_or_place_pieceGetFeedbackCallback(
    iri_world_interface_msgs::PickOrPlacePieceFeedbackPtr& feedback)
{
  this->alg_.lock();
  feedback->info = porp_info_;
  //update feedback data to be sent to client
  ROS_INFO("feedback: %s", porp_info_.c_str());
  this->alg_.unlock();
}


/*  [action requests] */
bool WamPickerActionsAlgNode::close_toolMakeActionRequest()
{
  if(close_tool_client_.isServerConnected())
  {
    close_tool_client_.sendGoal(close_tool_goal_,
                boost::bind(&WamPickerActionsAlgNode::close_toolDone,     this, _1, _2),
                boost::bind(&WamPickerActionsAlgNode::close_toolActive,   this),
                boost::bind(&WamPickerActionsAlgNode::close_toolFeedback, this, _1));
    return true;
  }
  else
  {
    ROS_ERROR("WamPickerActionsAlgNode::close_toolMakeActionRequest: HRI server is not connected");
    return false;
  }
}

bool WamPickerActionsAlgNode::open_toolMakeActionRequest()
{
  if(open_tool_client_.isServerConnected())
  {
    open_tool_client_.sendGoal(open_tool_goal_,
                boost::bind(&WamPickerActionsAlgNode::open_toolDone,     this, _1, _2),
                boost::bind(&WamPickerActionsAlgNode::open_toolActive,   this),
                boost::bind(&WamPickerActionsAlgNode::open_toolFeedback, this, _1));
    return true;
  }
  else
  {
    ROS_ERROR("WamPickerActionsAlgNode::open_toolMakeActionRequest: HRI server is not connected");
    return false;
  }
}


void WamPickerActionsAlgNode::node_config_update(Config &config, uint32_t level)
{
  this->alg_.lock();
  this->config_=config;
  this->alg_.unlock();
}

void WamPickerActionsAlgNode::addNodeDiagnostics(void)
{
}

/* main function */
int main(int argc,char *argv[])
{
  return algorithm_base::main<WamPickerActionsAlgNode>(
      argc, argv, "wam_picker_actions_alg_node");
}
