import roslib
import rospy
import actionlib

from iri_perception_pipeline_msgs.msg import  DetectPiecesGoal,\
                                              DetectPiecesResult,\
                                              DetectPiecesAction

from numpy import std

action_name = "/perception_pipeline/DetectPieces"
shape = "triangle"
number_of_iter = 50
hz = 20

def obtain_pose():
  detect_pieces_client.wait_for_server(rospy.Duration(10))
  goal = DetectPiecesGoal(available_shapes=[shape],detection_type=0)
  detect_pieces_client.send_goal(goal)
  detect_pieces_client.wait_for_result(rospy.Duration(10))
  result = detect_pieces_client.get_result()
  rotation = 0
  try:
    centroid = result.detected_pieces[0].centroid
    rotation = result.detected_pieces[0].matches[0].optimum_angle
    pose = (centroid.x,centroid.y,centroid.z,rotation)
  except IndexError:
    print "Could not obtain pose."
    pose = (0,)*4
  return pose

if __name__ == '__main__':
  try:
    rospy.init_node('measure_std_dev_pieces_on_table_py')
    detect_pieces_client = actionlib.SimpleActionClient(action_name,
        DetectPiecesAction)
    rate = rospy.Rate(hz)
    x = []
    y = []
    z = []
    theta = []
    for idx in xrange(number_of_iter):
      pose = obtain_pose()
      print pose
      x.append(pose[0])
      y.append(pose[1])
      z.append(pose[2])
      theta.append(pose[3])
      rate.sleep()
    print "Std dev in x: " + str(std(x))
    print "Std dev in y: " + str(std(y))
    print "Std dev in z: " + str(std(z))
    # Watch out when calculating the std of several angles! The result may be
    # invalid.
    print "Std dev in theta: " + str(std(theta))

  except rospy.ROSInterruptException:
    print "Interrupted before completion"
