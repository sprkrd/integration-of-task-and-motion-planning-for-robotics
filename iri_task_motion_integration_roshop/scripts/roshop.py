"""
Pyhop for ROS -- This is a modification by Alejandro Suarez Hernandez of
the original Pyhop by Dana S. Nau. Despite its name, it is actually usable
on its own, without ROS (although the author's intention was to use it in
a ROS environment). The main new feature introduced in this modification is
the support for metric minimization. Now, the "pyhop" method supports
an additional argument minimize_metric. If None, the planner runs just like
always. Otherwise, it tries to find a plan that minimizes the
vars(state)[minimize_metric] (i.e. the variable binding minimize_metric from
the state). It uses a simple B&B algorithm to do so. There are other minor
modifications:
    - No need for Goal class since it is not conceptually different from
    a state
    - When seeking for a plan, the recursion depth is increased only in
    compound tasks. If a task is primitive, it just applies it to the state
    and moves to the next task.

Below there is the original note from Dana S. Nau

---

Pyhop, version 1.2.2 -- a simple SHOP-like planner written in Python.
Author: Dana S. Nau, 2013.05.31

Copyright 2013 Dana S. Nau - http://www.cs.umd.edu/~nau

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

Pyhop should work correctly in both Python 2.7 and Python 3.2.
For examples of how to use it, see the example files that come with Pyhop.

Pyhop provides the following classes and functions:

- foo = State('foo') tells Pyhop to create an empty state object named 'foo'.
  To put variables and values into it, you should do assignments such as
  foo.var1 = val1

- bar = Goal('bar') tells Pyhop to create an empty goal object named 'bar'.
  To put variables and values into it, you should do assignments such as
  bar.var1 = val1

- print_state(foo) will print the variables and values in the state foo.

- print_goal(foo) will print the variables and values in the goal foo.

- declare_operators(o1, o2, ..., ok) tells Pyhop that o1, o2, ..., ok
  are all of the planning operators; this supersedes any previous call
  to declare_operators.

- print_operators() will print out the list of available operators.

- declare_methods('foo', m1, m2, ..., mk) tells Pyhop that m1, m2, ..., mk
  are all of the methods for tasks having 'foo' as their taskname; this
  supersedes any previous call to declare_methods('foo', ...).

- print_methods() will print out a list of all declared methods.

- pyhop(state1,tasklist) tells Pyhop to find a plan for accomplishing tasklist
  (a list of tasks), starting from an initial state state1, using whatever
  methods and operators you declared previously.

- In the above call to pyhop, you can add an optional 3rd argument called
  'verbose' that tells pyhop how much debugging printout it should provide:
- if verbose = 0 (the default), pyhop returns the solution but prints nothing;
- if verbose = 1, it prints the initial parameters and the answer;
- if verbose = 2, it also prints a message on each recursive call;
- if verbose = 3, it also prints info about what it's computing.
"""

# Pyhop's planning algorithm is very similar to the one in SHOP and JSHOP
# (see http://www.cs.umd.edu/projects/shop). Like SHOP and JSHOP, Pyhop uses
# HTN methods to decompose tasks into smaller and smaller subtasks, until it
# finds tasks that correspond directly to actions. But Pyhop differs from
# SHOP and JSHOP in several ways that should make it easier to use Pyhop
# as part of other programs:
#
# (1) In Pyhop, one writes methods and operators as ordinary Python functions
#     (rather than using a special-purpose language, as in SHOP and JSHOP).
#
# (2) Instead of representing states as collections of logical assertions,
#     Pyhop uses state-variable representation: a state is a Python object
#     that contains variable bindings. For example, to define a state in
#     which box b is located in room r1, you might write something like this:
#     s = State()
#     s.loc['b'] = 'r1'
#
# (3) You also can define goals as Python objects. For example, to specify
#     that a goal of having box b in room r2, you might write this:
#     g = Goal()
#     g.loc['b'] = 'r2'
#     Like most HTN planners, Pyhop will ignore g unless you explicitly
#     tell it what to do with g. You can do that by referring to g in
#     your methods and operators, and passing g to them as an argument.
#     In the same fashion, you could tell Pyhop to achieve any one of
#     several different goals, or to achieve them in some desired sequence.
#
# (4) Unlike SHOP and JSHOP, Pyhop doesn't include a Horn-clause inference
#     engine for evaluating preconditions of operators and methods. So far,
#     I've seen no need for it; I've found it easier to write precondition
#     evaluations directly in Python. But I could consider adding such a
#     feature if someone convinces me that it's really necessary.
#
# Accompanying this file are several files that give examples of how to use
# Pyhop. To run them, launch python and type "import blocks_world_examples"
# or "import simple_travel_example".


from __future__ import print_function
import copy,sys, pprint

############################################################
# States and goals

class State():
    """A state is just a collection of variable bindings. Define any custom
    object and bind the variables you want to it. Make sure you inherit
    from this class or implement the copy method yourself."""
    def __init__(self,name,**kwargs):
        """
        The original Pyhop requested a name for the creation of the
        state. This means that there is an additional overhead (although
        small) each time the state is copied. We have decided to just ignore
        the name since it does not make that much of a difference when
        debugging (in fact is a little confusing because all the states
        after the initial state have the same name). We still accept the
        "name" parameter for backward compatibility. In addition we copy
        all the keyword arguments to the instance's __dict__ so the
        states can be initially created in a much direct way.
        """
        # self.__name__ = name
        self.__dict__ = kwargs.copy()
    def copy(self):
        """Convenience method. Returns a deep copy of self"""
        return copy.deepcopy(self)

# We have decided to remove the Goal state. It does not make much sense to
# have two identical classes that are basically the same. Even conceptually,
# a Goal is also a state. The semantic nature of a goal can be indicated by
# means of a relevant variable name or, if a goal is in fact conceptually
# different from a State, State can be subclassed.

def print_state(state,indent=4):
    """Print each variable in state, indented by indent spaces."""
    if state != False:
        for (name,val) in vars(state).items():
            sys.stdout.write(' '*indent)
            sys.stdout.write('s.' + name)
            print(' = ', val)
    else: print('False')

############################################################
# Helper functions that may be useful in domain models

def forall(seq,cond):
    """True if cond(x) holds for all x in seq, otherwise False."""
    for x in seq:
        if not cond(x): return False
    return True

def find_if(cond,seq):
    """
    Return the first x in seq such that cond(x) holds, if there is one.
    Otherwise return None.
    """
    for x in seq:
        if cond(x): return x
    return None

############################################################
# Commands to tell Pyhop what the operators and methods are

operators = {}
methods = {}

def declare_operators(*op_list):
    """
    Call this after defining the operators, to tell Pyhop what they are.
    op_list must be a list of functions, not strings.
    """
    operators.update({op.__name__:op for op in op_list})
    return operators

def declare_methods(task_name,*method_list):
    """
    Call this once for each task, to tell Pyhop what the methods are.
    task_name must be a string.
    method_list must be a list of functions, not strings.
    """
    methods.update({task_name:list(method_list)})
    return methods[task_name]

############################################################
# Commands to find out what the operators and methods are

def print_operators(olist=operators):
    """Print out the names of the operators"""
    print('OPERATORS:', ', '.join(olist))

def print_methods(mlist=methods):
    """Print out a table of what the methods are for each task"""
    print('{:<14}{}'.format('TASK:','METHODS:'))
    for task in mlist:
        print('{:<14}'.format(task) + ', '.join([f.__name__ for f in mlist[task]]))

############################################################
# The actual planner

maximum_depth = 50

def pyhop(state,tasks,verbose=0,minimize_metric=None,upper_bound=float('inf')):
    """
    Try to find a plan that accomplishes tasks in state.
    If successful, return the plan. Otherwise return False.
    """
    if verbose>0:
        print('** pyhop, verbose={}: **\n   tasks = {}'\
                .format(verbose, tasks))
        print_state(state)
    endstate,result = seek_plan(state,tasks,0,verbose,minimize_metric,upper_bound)
    if verbose>0:
        print('** result =',result,'\n')
        if endstate: print_state(endstate)
    return result

def seek_plan(state,tasks,depth,verbose=0,minimize_metric=None,
        upper_bound=float('inf')):
    """
    Workhorse for pyhop. state and tasks are as in pyhop.
    - state is the current state.
    - depth is the recursion depth, for use in debugging
    - verbose is whether to print debugging messages
    - minimize_metric is the name of the state variable that the planner
    shall try to minimize. None if no optimization is desired
    - upper_bound: seek_plan stops if the value of vars(state)[minimize_metric]
    exceeds upper_bound
    """

    if verbose>1: print('depth {} tasks {} minimize_metric={}'.format(
        depth,tasks,minimize_metric))

    if depth > maximum_depth:
        if verbose > 1: print("Depth ({}) exceeded".format(maximum_depth))

    plan = []

    for task in tasks:
        if task[0] in operators:
            if verbose > 2: print('depth {} action {}'.format(depth,task))
            operator = operators[task[0]]
            newstate = operator(state.copy(),*task[1:])
            if verbose > 2:
                print('depth {} new state:'.format(depth))
                print_state(newstate)
            accept_state = newstate and (not minimize_metric or\
                    vars(newstate)[minimize_metric] < upper_bound)
            if accept_state:
                plan.append(task)
                state = newstate
            else:
                state,plan = None,None
                break
        elif task[0] in methods:
            if verbose > 2:
                print('depth {} method instance {}'.format(depth,task))
            relevant = methods[task[0]]
            upper_bound_methods = float('inf')
            next_state,partial_plan = None,None
            for method in relevant:
                subtasks = method(state,*task[1:])
                if verbose > 2:
                    print('depth {} new tasks: {}'.format(depth,subtasks))
                # Can't just say "if subtasks:", because that's wrong if
                # subtasks == []
                if subtasks != False:
                    newstate,incplan = seek_plan(state,subtasks,depth+1,
                            verbose,minimize_metric,upper_bound_methods)
                    if incplan != None:
                        next_state = newstate
                        partial_plan = incplan
                        if minimize_metric:
                            upper_bound_methods =\
                                    vars(newstate)[minimize_metric]
                        else: break
            accept_state = next_state and (not minimize_metric or\
                    vars(next_state)[minimize_metric] < upper_bound)
            if accept_state:
                state = next_state
                plan += partial_plan
            else:
                state,plan = None,None
                break
        else: assert False

    if plan != None and verbose > 2:
        print('depth {} returns plan {}'.format(depth,plan))
    elif plan == None and verbose > 2:
        print('depth {} returns failure'.format(depth))

    return state,plan


