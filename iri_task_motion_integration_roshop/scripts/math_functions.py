from math import erf,exp,sqrt,pi,log
import numpy as np
# Mathematical constants and functions

INFINITY = float('Inf')
SQRT_2 = sqrt(2)
SQRT_PI = sqrt(pi)
DIV_2_SQRT_PI = 2/SQRT_PI

def spherical_gaussian_cdf(x):
  """ Spherical gaussian CDF. We define X as a random variable that measures
  the distance from the mode of an isotropic gaussian distribution with
  standard deviation equal to sqrt(2)^-1. This function returns the probability
  of X being smaller than a certain x. Call
  spherical_gaussian_cdf(d/(sqrt(2)*sigma)) in order to obtain the probability
  of X being nearer than d, with X following an isotropic spherical gaussian
  distribution with deviation equal to an arbitrary sigma. In the thesis
  report this function is known as g(x) """
  return erf(x)-DIV_2_SQRT_PI*x*exp(-x*x) if x > 0 else 0

def spherical_gaussian_pdf(x):
  """ Spherical gaussian PDF. Returns probability density of X (check
  spherical_gaussian_cdf to see how X is defined). """
  x2 = x*x
  return 2*DIV_2_SQRT_PI*x2*exp(-x2) if x > 0 else 0

def newton_raphson(f,df,guess,epsilon=1E-12,maxiter=100):
  """ Helper method. Applies Newton-Raphson method to obtain the root of
  a function f (only finds one root at maximum). The method requires:
  - f: an univariate function
  - df: derivative of f
  - guess: initial guess
  - epsilon: the method will stop when it finds an x such that
             f(x) < epsilon or ...
  - maxiter: ... when maxiter iterations of the Newton-Raphson method have
             been performed. """
  remaining_iter = maxiter
  x = guess
  f_x = f(x)
  while abs(f_x) > epsilon and remaining_iter:
      df_x = df(x)
      x = x - f_x/df_x
      f_x = f(x)
      remaining_iter -= 1
  return x

def spherical_gaussian_icdf(y):
  """ Spherical gaussian inverse CDF. Obtains icdf of random variable X.
  Check spherical_gaussian_icdf to see how X is defined. """
  if y == 0: return 0.0
  if y == 1: return INFINITY
  return newton_raphson(lambda x: spherical_gaussian_cdf(x)-y,
      spherical_gaussian_pdf,1.09)

def entropy(probabilities,normalized=False):
  """ Returns the entropy of a set of probabilities. It uses base 2 for the
  logarithm. The parameters are:
    -probabilities: an iterable object containing the probabilities. The
    method does not check it for efficiency, but in order for the result to
    be correct these should sum 1.
    -normalized: if set to True, it will divide the entropy by the maximum
    possible entropy with the same number of elements than the given set. """
  ret = reduce(lambda acc,prob: acc-prob*log(prob,2) if prob > 0 else acc,
      probabilities,0.0)
  if normalized:
    nonzero = reduce(lambda acc,prob: acc if prob < 1e-9 else acc+1,
        probabilities,0.0)
    if nonzero > 1: ret /= log(nonzero,2)
  return ret


def scale_matrix(mat,epsilon=1E-5,maxiter=100):
  """ Scales the given np matrix *in-place* so it is doubly stochastic. It
  returns the same matrix.
  The parameters are:
    - mat: input matrix
    - epsilon: maximum tolerable error. The error is computed as the
      maximum absolute value of the difference of the rows/columns sum and 1.
  """
  t_mat = mat.transpose()
  error_rows = max([abs(sum(row)-1.0) for row in mat])
  error_cols = max([abs(sum(col)-1.0) for col in t_mat])
  error = max(error_rows,error_cols)
  it = 0
  while error > epsilon and it < maxiter:
    for row in mat:
      inv_sum = 1.0/sum(row)
      row *= inv_sum
    for col in t_mat:
      inv_sum = 1.0/sum(col)
      col *= inv_sum
    error = max([abs(sum(row)-1.0) for row in mat])
    it += 1
  return mat
