#! /usr/bin/env python

#  - This example can be called after executing:
#    $ rosrun iri_wam_controller test_load_and_follow_joint_trajectory_in_a_csv_file.py -j poses_in_joint.csv -c poses_in_cartesian.csv
#
# This example shows how to send a QueryJointsMovement Service call,
# how to read multiple poses from a CSV file and how to ask for the
# Inverse Kinematics of the robot to achieve a joints movement

import roslib
import rospy
import actionlib_msgs
import actionlib
import sys
import getopt
import csv
import time
import tf
import numpy as np
import toy_sphere_world
import math_functions

from math                        import sqrt
from geometry_msgs.msg           import Point,Quaternion,PoseStamped
from geometry_msgs.msg           import PointStamped,Vector3Stamped
from iri_common_drivers_msgs.srv import QueryJointsMovement, QueryJointsMovementRequest
from iri_common_drivers_msgs.srv import QueryInverseKinematics, QueryInverseKinematicsRequest
from iri_common_drivers_msgs.srv import QueryInverseKinematicsResponse
from control_msgs.msg            import FollowJointTrajectoryAction, FollowJointTrajectoryGoal
from trajectory_msgs.msg         import JointTrajectory, JointTrajectoryPoint
from tf                          import TransformListener
from tf.transformations          import quaternion_from_euler

from iri_perception_pipeline_msgs.msg import DetectPiecesGoal, DetectPiecesAction
from iri_world_interface_msgs.msg import PickOrPlacePieceGoal, PickOrPlacePieceAction
from iri_world_interface_msgs.msg import AdoptPositionGoal, AdoptPositionAction

theta_max = {'pentagon':72.0,
    'trapezium':360.0,
    'trapezoid':360.0,
    'triangle':120.0,
    'hexagon':60.0,
    'star8':45.0,
    'cross':90.0,
    'circularsector':360.0,
    'star5':72.0,
    'star6':60.0}

available_shapes = ['star5',
                    'star6',
                    'star8',
                    'triangle',
                    'hexagon',
                    'cross',
                    'circularsector',
                    'trapezium',
                    'trapezoid',
                    'pentagon'] # ['pentagon','trapezium','star6','star5']
robot_position = {'picker':'unknown','catcher':'unknown'}
orientation = np.array([])
similitudes = np.array([])
probabilities = np.array([])
blobs_coordinates = []
sure_match = []
holding = None
on_table = []
n = 0
nu = float('inf')
nu_obs = 5.0
sigma = 1000
sigma_obs = 0.005

def print_current_state():
  print "Orientation matrix: " + str(orientation)
  print "Similitudes matrix: " + str(similitudes)
  print "Probabilities matrix: " + str(probabilities)
  print "Blob coordinates: "
  for coord in blobs_coordinates:
    print coord
  print "Sure matches: " + str(sure_match)
  print "Holding: " + str(holding)
  print "On table: " + str(on_table)
  print "n: " + str(n)
  print "nu: " + str(nu)
  print "sigma: " + str(sigma)
  print "-"*81

#####################################

def detect_pieces_client(detection_type=0,available_shapes=available_shapes):
    client = actionlib.SimpleActionClient('~detect_pieces',DetectPiecesAction)
    client.wait_for_server()
    goal = DetectPiecesGoal(detection_type,available_shapes)
    client.send_goal_and_wait(goal)
    if client.get_state() != actionlib_msgs.msg.GoalStatus.SUCCEEDED:
        print "Call to action detect_pieces failed"
        assert False
    detected_pieces = client.get_result().detected_pieces
    if len(detected_pieces) == 1:
        print detected_pieces
    return client.get_result().detected_pieces

def pick_or_place_client(centroid,pick=True,most_likely_rotation=0):
    print "I'm here"
    client = actionlib.SimpleActionClient('~pick_or_place_piece',PickOrPlacePieceAction)
    client.wait_for_server()
    goal = PickOrPlacePieceGoal(centroid,most_likely_rotation,pick,False)
    client.send_goal_and_wait(goal)
    if client.get_state() != actionlib_msgs.msg.GoalStatus.SUCCEEDED:
        print "Call to action pick_or_place_client failed"
        assert False
    return client.get_result()

def assume_position_client(robot,position):
    client = actionlib.SimpleActionClient('~{}_assume_position'.format(robot),
            AdoptPositionAction)
    client.wait_for_server()
    goal = AdoptPositionGoal(position)
    client.send_goal_and_wait(goal)
    if client.get_state() != actionlib_msgs.msg.GoalStatus.SUCCEEDED:
        print "Call to action assume_position_client failed"
        assert False
    return client.get_result()


#####################################

def ml(idx_piece):
  ml_shape = -1
  highest_prob = 0
  for idx_shape in xrange(len(available_shapes)):
    if probabilities[idx_piece,idx_shape] > highest_prob:
      highest_prob = probabilities[idx_piece,idx_shape]
      ml_shape = idx_shape
  return ml_shape

def build_probabilities():
  global probabilities
  probabilities = math_functions.scale_matrix(similitudes.copy())
  for idx in xrange(n):
    if sure_match[idx] != None:
      probabilities[idx] = 0.0
      probabilities.transpose()[sure_match[idx]] = 0.0
      probabilities[idx,sure_match[idx]] = 1.0
  math_functions.scale_matrix(probabilities)

def observe_table(first_time=False):
  global n, similitudes, blobs_coordinates, orientation, on_table, sigma
  global nu, sure_match
  p=5
  detected_pieces = detect_pieces_client()
  if first_time:
    if len(detected_pieces) != len(available_shapes):
      print "Action (observe_table) failed! Reason: |detected_pieces| != |available_shapes|"
      return False
    n = len(available_shapes)
    similitudes = np.array([[0.0]*n]*n)
    orientation = np.array([[0.0]*n]*n)
    on_table = [True]*n
    sure_match = [None]*n
    idx_piece = 0
    blobs_coordinates = []
    for piece in detected_pieces:
      blobs_coordinates.append(piece.centroid)
      for match in piece.matches:
        idx_shape = available_shapes.index(match.shape)
        similitudes[idx_piece,idx_shape] = match.similitude**p
        orientation[idx_piece,idx_shape] = match.optimum_angle
      idx_piece += 1
    sigma = sigma_obs
    build_probabilities()
  else:
    updated = [False]*n
    for piece in detected_pieces:
      idx_piece = find_nearest_blob(piece.centroid)
      if updated[idx_piece]:
        print "Action (observe_table) failed! Reason: piece position updated twice".format(idx_piece)
        return False
      updated[idx_piece]  = True
      update_position(blobs_coordinates[idx_piece],piece.centroid)
      for match in piece.matches:
        idx_shape = available_shapes.index(match.shape)
        similitudes[idx_piece,idx_shape] = 0.5*similitudes[idx_piece,idx_shape] + 0.5*match.similitude**p
        orientation[idx_piece,idx_shape] = match.optimum_angle
        # print str(match.optimum_angle) + " " + str(orientation[idx_piece,idx_shape])
    for idx_piece in xrange(len(updated)):
      if not updated[idx_piece] and on_table[idx_piece]:
        print "Action (observe_table) failed! Reason: piece position {} not updated".format(idx_piece)
        return False
    sigma = sigma*sigma_obs/sqrt(sigma**2 + sigma_obs**2)
    build_probabilities()
  return True

def assume_position(robot,position):
  if robot == 'catcher' and position != "neutral": position = available_shapes[position]
  if robot_position[robot] == position: return False
  assume_position_client(robot,position)
  robot_position[robot] = position
  return True

def pick(idx_piece):
  global on_table, holding, nu, robot_position
  ml_shape = ml(idx_piece)
  ml_rotation = orientation[idx_piece,ml_shape]
  nu = float('inf')
  pick_or_place_client(blobs_coordinates[idx_piece],
      pick=True,most_likely_rotation=ml_rotation-3.33)
  holding = idx_piece
  on_table[idx_piece] = False
  robot_position['picker'] = 'over-table'
  return True

def receive_feedback(idx_piece,idx_shape):
  global sure_match,nu
  # Prompt user for feedback
  msg1 = """I think I am holding the {}. However, I am not sure
    (normalized entropy={},
    similitude={}). Select one of the following options:"
    [0] You got it right. Please go on."
    [1] You got it right. However, the grip was poor. It is already fixed.
    [2] You got it wrong. You are actually holding another piece."""\
    .format(available_shapes[idx_shape],
        math_functions.entropy(probabilities[idx_piece],normalized=True),
        similitudes[idx_piece,idx_shape])
  in1 = input(msg1)

  if in1 == 0:
    sure_match[idx_piece] = idx_shape
    build_probabilities()
    return True
  elif in1 == 1:
    sure_match[idx_piece] = idx_shape
    nu = float('inf')
    build_probabilities()
    return True
  elif in1 == 2:
    msg2 = "Which piece am I holding?\n"
    for idx in xrange(len(available_shapes)):
      msg2 += "[{}] {}\n".format(idx,available_shapes[idx])
    sure_match[idx_piece] = input(msg2)
    build_probabilities()
    print "Action (receive_feedback) failed! Reason, guessed shape different than chosen shape"
    return False
  else: assert False

def insert(idx_piece):
  global sure_match, holding, robot_position
  ml_shape_idx = ml(idx_piece)
  ml_shape = available_shapes[ml_shape_idx]
  if robot_position['catcher'] != ml_shape:
    print "Action (insert) failed! Reason: wrong cavity"
    return False
  if holding != idx_piece:
    print "Action (insert) failed! Reason: not holding {}".format(idx_piece)
    return False
  detected_cavity = detect_pieces_client(
      detection_type=1,available_shapes=[ml_shape])

  cavity_robot = tf_listener.transformPoint("iri_wam_picker_link_base",
      detected_cavity[0].centroid)

  # cavity_robot.point.z += 0.02

  pick_or_place_client(cavity_robot,False,most_likely_rotation=0)
      # most_likely_rotation=orientation[idx_piece,ml_shape_idx]-3.33)

  sure_match[idx_piece] = ml_shape_idx
  robot_position['picker'] = 'over-sphere'
  holding = None
  build_probabilities()
  return True

def show_blob(idx_piece):
  global nu
  p = 5
  detected_piece = detect_pieces_client(detection_type=2)
  if nu == float('inf'):
    nu = nu_obs
    for match in detected_piece[0].matches:
      idx_shape = available_shapes.index(match.shape)
      similitudes[idx_piece,idx_shape] = match.similitude**p
      orientation[idx_piece,idx_shape] = match.optimum_angle
      return False # Fail so we can replan using the close-look info
  else:
    for match in detected_piece[0].matches:
      idx_shape = available_shapes.index(match.shape)
      similitudes[idx_piece,idx_shape] = 0.5*similitudes[idx_piece,idx_shape] +\
          0.5*match.similitude**p
      orientation[idx_piece,idx_shape] = update_rotation(
          orientation[idx_piece,idx_shape],
          match.optimum_angle,
          theta_max[match.shape])
    nu = nu*nu_obs/sqrt(nu**2 + nu_obs**2)
    return True

def plan_for_current_state():
  current_state = toy_sphere_world.State(
      catcher_position = robot_position['catcher'],
      picker_position = robot_position['picker'],
      P = probabilities,
      on_table = on_table,
      holding = holding,
      sigma_current = sigma,
      nu_current = nu,
      user_match = sure_match)
  # print current_state
  toy_sphere_world.n = n
  toy_sphere_world.B = similitudes
  return toy_sphere_world.plan_for_state(current_state)

action_dict = {
  'observe_table':observe_table,
  'assume_position':assume_position,
  'pick':pick,
  'show_blob':show_blob,
  'insert':insert,
  'receive_feedback':receive_feedback
}

def interleaved_execution():
  print_current_state()
  p = plan_for_current_state()
  print p
  for action in p:
    raw_input("Executing " + str(action) + " now. Press ENTER to continue")
    action_method = action_dict[action[0]]
    success = action_method(*action[1:])
    if success:
      print "Action executed successfully. Current state:"
      print_current_state()
    else: return False
  return True

def interleaved_execution_top():
  success = interleaved_execution()
  while not success:
    success = interleaved_execution()

#####################################

def update_position(pos_estimation,pos_observation):
  w1 = sigma_obs**2/(sigma**2+sigma_obs**2)
  w2 = 1 - w1
  pos_estimation.point.x = w1*pos_estimation.point.x + w2*pos_observation.point.x
  pos_estimation.point.y = w1*pos_estimation.point.y + w2*pos_observation.point.y
  pos_estimation.point.z = w1*pos_estimation.point.z + w2*pos_observation.point.z
  return pos_estimation

def update_rotation(current_estimator,new_observation,maximum_rotation=360):
    w1 = nu_obs**2/(nu_obs**2+nu**2)
    w2 = 1-w1
    if new_observation - current_estimator > maximum_rotation/2:
        return w1*current_estimator + w2*(new_observation-maximum_rotation)
    elif current_estimator - new_observation > maximum_rotation/2:
        return w1*(current_estimator-maximum_rotation) + w2*new_observation
    else:
        return w1*current_estimator + w2*new_observation

def find_nearest_blob(point_stmp):
  min_sqdist = float('inf')
  min_idx = -1
  for idx in xrange(len(blobs_coordinates)):
    point_stmp_idx = blobs_coordinates[idx]
    sqdist = (point_stmp.point.x-point_stmp_idx.point.x)**2 + \
        (point_stmp.point.y-point_stmp_idx.point.y)**2 + \
        (point_stmp.point.z-point_stmp_idx.point.z)**2
    if sqdist < min_sqdist:
      min_sqdist = sqdist
      min_idx = idx
  return min_idx

################################

if __name__ == '__main__':
  rospy.init_node('continuous_planning')

  tf_listener = TransformListener()
  tf_listener.waitForTransform("iri_wam_picker_link_base","camera_rgb_optical_frame",rospy.Time(),rospy.Duration(10))

  assume_position('picker','neutral')
  assume_position('catcher','neutral')
  observe_table(first_time=True)

  print_current_state()

  raw_input("Press ENTER to continue")

  interleaved_execution_top()

