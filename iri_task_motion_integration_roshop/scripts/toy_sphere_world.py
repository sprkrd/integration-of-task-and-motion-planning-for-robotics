import roshop
import numpy as np
import math_functions

from math import erf, sqrt, log10

# State definition

# PARAMETERS THAT REMAIN CONSTANT BETWEEN CALLS TO THE PLANNER

delta = 0.005       # Distance tolerance
alpha = 2.5         # Angle tolerance
sigma_obs = 0.005   # Std dev of the position observations
nu_obs = 5          # Std dev of the angle observations

b_min = 0.5         # Minimum similitude to insert directly
zeta = 0.95         # controls how the entropy will affect the robot's
                    # decision. A higher zeta means a lower influence,
                    # and viceversa. Should be between 0 and 1.
rho = 1             # Has repercussion on the cost of the pick action.
                    # Therefore, the higher, the more observations will be
                    # performed. It is unbounded.

epsilon_maximum = 0.15  # maximum allowed uncertainty


# PARAMETERS THAT ARE CONSTANT IN A SINGLE PLANNING PROCESS

n = 0               # Number of available shapes/detected blobs. We use just
                    # their indices, so
                    # the ir no need to store a list.
B = np.array([])    # Matrix of similitudes

class State(roshop.State):
  """ World state. It has the following attributes:
    -catcher_position: a qualitative name for the catcher description
    -picker_position: a qualitative name for the picker position
    -P: the matrix of probabilities. Should not it be provided, it is
    automatically computed from scratch.
    -on_table: a vector of boolean objects. on_table[idx] indicates whether
    the blob with index idx is located on the table or not.
    -holding: None, if the picker is not holding anything, or the index
    of one of the blobs
    -sigma_current: the std deviation of the pieces that are distributed over
    the table
    -nu_current: the std deviation of the rotation of the piece that is
    being held by the picker.
    -user_match: determines, for ech blob, if the user has stablished
    an unequivocal match between another shape. user_match[idx] = None means
    that there is not such match. Otherwise, user_match[idx] will contain
    the index of a shape.
    -cost: accumulated cost.
  """
  def __init__(self,
      catcher_position="neutral",
      picker_position="neutral",
      P=None,
      on_table=[],
      holding=None,
      sigma_current=1000,
      nu_current=1000,
      user_match=[],
      cost=0.0):
    self.robot_position = {"catcher":catcher_position,"picker":picker_position}
    self.on_table = on_table

    # Set P, is specified. Otherwise calculate it.
    if P != None:
      self.P = P
    else:
      self.P = math_functions.scale_matrix(B.copy())

    self.holding = holding
    self.sigma_current = sigma_current
    self.nu_current = nu_current
    self.user_match = user_match
    for piece_idx in xrange(n):
      if self.user_match[piece_idx] != None:
        self.update_probabilities(piece_idx,self.user_match[piece_idx])
    self.cost = cost

  def ml(self,piece_idx):
    """ Returns the index and the probability of the most likely shape for
    piece with index piece_idx """
    max_prob = 0
    max_idx = -1
    idx = 0
    for prob in self.P[piece_idx]:
      if prob > max_prob:
        max_prob = prob
        max_idx = idx
      idx += 1
    return max_idx,max_prob

  def bv_xyz(self):
    """Returns probability of position estimation being outside a delta-sphere
    centered on the mode"""
    arg = delta/(sqrt(2)*self.sigma_current)
    return 1 - math_functions.spherical_gaussian_cdf(arg)

  def bv_theta(self):
    """Returns probability of rotation estimation being outside an
    alpha-interval centered on the mode."""
    arg = alpha/(sqrt(2)*self.nu_current)
    return 1 - erf(arg)

  def entropy(self,piece_idx,normalized=False):
    """Returns the entropy of the piece with index piece_idx."""
    return math_functions.entropy(self.P[piece_idx],normalized)

  def update_probabilities(self,piece_idx,shape_idx):
    """ Matches piece with index piece_idx with shape with index
    shape_idx. This means that P[piece_idx,shape_idx] = 1. The probabilities
    matrix is scaled accordingly. """
    self.P[piece_idx] = 0.0
    self.P.transpose()[shape_idx] = 0.0
    self.P[piece_idx,shape_idx] = 1.0
    math_functions.scale_matrix(self.P)

  def __str__(self):
    ret = "State:\n" + \
      "  robot_position: " + str(self.robot_position) + "\n" + \
      "  on_table: " + str(self.on_table) + "\n" + \
      "  holding: " + str(self.holding) + "\n" + \
      "  sigma_current: " + str(self.sigma_current) + "\n" \
      "  nu_current: " + str(self.nu_current) + "\n" \
      "  user_match: " + str(self.user_match) + \
      "  cost: " + str(self.cost) + "\n" + \
      "  P: " + str(self.P)
    return ret

# Operators

def assume_position(state,robot,position):
  """ Make that a robot assumes a certain position """
  # Check that it is not already in such position
  if state.robot_position[robot] == position:
    return False

  state.robot_position[robot] = position
  state.cost += 1.0

  return state

def observe_table(state):
  """ Updates the std deviation about the location of the pieces in the table.
  """

  # Check precondition
  if state.robot_position["picker"] != "neutral":
    return False
  if state.robot_position["catcher"] != "neutral":
    return False

  # Update state
  epsilon_xyz = state.bv_xyz()
  if state.sigma_current == float('inf'):
    state.sigma_current = sigma_obs
  else:
    num = state.sigma_current*sigma_obs
    den = sqrt(state.sigma_current**2 + sigma_obs**2)
    state.sigma_current = num/den
    state.cost += 1.0 - log10(epsilon_xyz)

  return state


def pick(state,piece_idx):
  """ As the result of this operator, the piece with index piece_idx is
  held by the Picker. """

  # Check precondition
  if state.robot_position['picker'] != 'neutral': return False
  if state.robot_position['catcher'] != 'neutral': return False
  if state.holding != None: return False
  if not state.on_table[piece_idx]: return False
  epsilon_xyz = state.bv_xyz()
  if epsilon_xyz > epsilon_maximum: return False

  # Update state
  state.holding = piece_idx
  state.on_table[piece_idx] = False
  state.robot_position['picker'] = 'over-table'
  epsilon_xyz = state.bv_xyz()
  state.cost += 1 - rho*log10(1-epsilon_xyz)
  state.nu_current = 1000

  return state

def show_blob(state,piece_idx):
  """ Performs a close observation of the currently held piece. """

  # Check that the picker is in show position and the the catcher is not
  # an obstacle.
  if state.robot_position['picker'] != 'show': return False
  if state.robot_position['catcher'] != 'neutral': return False
  if state.holding != piece_idx: return False

  # Update state
  epsilon_theta = state.bv_theta()
  if state.nu_current == float('inf'):
    state.nu_current = nu_obs
  else:
    num = state.nu_current*nu_obs
    den = sqrt(state.nu_current**2 + nu_obs**2)
    state.nu_current = num/den

  state.cost += 1 - log10(epsilon_theta)

  return state

def insert(state,piece_idx):
  """ Insert the currently held piece into the sphere. The sphere must
  be showing the most likely shape for piece piece_idx. """

  # Check precondition
  if state.robot_position['picker'] != 'neutral':

    return False
  ml_shape,_ = state.ml(piece_idx)
  if state.robot_position['catcher'] != ml_shape:
    return False
  if state.user_match[piece_idx] and state.user_match[piece_idx] != ml_shape:
    return False
  if state.user_match[piece_idx] != ml_shape and B[piece_idx,ml_shape] < b_min:
    return False
  if state.holding != piece_idx:
    return False
  epsilon_theta = state.bv_theta()
  if epsilon_theta > epsilon_maximum:
    return False

  # Update state
  state.cost += 1 + state.entropy(piece_idx,normalized=True)
  state.holding = None
  state.robot_position['picker'] = 'over-sphere'
  state.update_probabilities(piece_idx,ml_shape)

  return state

def receive_feedback(state,piece_idx,shape_ml):
  """ Receive feedback from human. Since the planner does not know before
  hand which is the shape the human will choose, the methods that include
  this operator in their subtasks list must perform an educated
  assumption (the most likely, for instance). """
  # Precondition
  if state.robot_position['picker'] != 'show': return False
  if state.holding == None: return False
  # Update
  state.cost += 1 + zeta - state.entropy(piece_idx,normalized=True)
  state.user_match[piece_idx] = shape_ml
  state.update_probabilities(piece_idx,shape_ml)

  return state

roshop.declare_operators(assume_position,observe_table,pick,show_blob,insert,
    receive_feedback)


def insert_all_method(state):
  """ This method returns a grab_and_insert subtask for each piece that is not
  inserted into the sphere. """
  pieces_idx = [(state.entropy(piece_idx),piece_idx) for piece_idx in xrange(n)]
  pieces_idx.sort()
  subtasks =  [('grab_and_insert',idx)
      for _,idx in pieces_idx if state.on_table[idx]]
  if state.holding != None:
    subtasks = [('insert_in_sphere',state.holding)] + subtasks
  return subtasks

roshop.declare_methods('insert_all',insert_all_method)

def grab_and_insert_method(state,piece_idx):
  subtasks = [('grab_piece',piece_idx),('insert_in_sphere',piece_idx)]
  return subtasks

roshop.declare_methods('grab_and_insert',grab_and_insert_method)

def set_all_positions_method(state,picker_position,catcher_position):
  subtasks = []
  if state.robot_position['picker'] != picker_position:
    subtasks.append(('assume_position','picker',picker_position))
  if state.robot_position['catcher'] != catcher_position:
    subtasks.append(('assume_position','catcher',catcher_position))
  return subtasks

roshop.declare_methods('set_all_positions',set_all_positions_method)

def grab_directly(state,piece_idx):
  if state.holding == piece_idx: return []
  return [('set_all_positions','neutral','neutral'),('pick',piece_idx)]

def observe_and_grab_piece(state,piece_idx):
  if state.holding == piece_idx: return []
  return [('set_all_positions','neutral','neutral'),('observe_table',),
      ('grab_piece',piece_idx)]

roshop.declare_methods('grab_piece', grab_directly, observe_and_grab_piece)

def assert_rotation_method(state,piece_idx):
  if state.bv_theta() < epsilon_maximum: return []
  subtasks = [('set_all_positions','show','neutral'),('show_blob',piece_idx),
      ('assert_rotation',piece_idx)]
  return subtasks

roshop.declare_methods('assert_rotation',assert_rotation_method)

def insert_in_sphere_directly(state,piece_idx):
  shape_ml,_ = state.ml(piece_idx)
  subtasks = [('assert_rotation',piece_idx),
      ('set_all_positions','neutral',shape_ml),('insert',piece_idx)]
  return subtasks

def insert_in_sphere_after_asking(state,piece_idx):
  if state.user_match[piece_idx] != None: return False # We have already asked
  shape_ml,_ = state.ml(piece_idx)
  subtasks = [('set_all_positions','show','neutral'),
      ('show_blob',piece_idx),('receive_feedback',piece_idx,shape_ml),
      ('insert_in_sphere',piece_idx)]
  return subtasks


roshop.declare_methods('insert_in_sphere',insert_in_sphere_after_asking,
    insert_in_sphere_directly)

def plan_for_state(state):
  return roshop.pyhop(state,[('insert_all',)],verbose=0,
      minimize_metric='cost')

# just some test

# B = np.array([
  # [ 0.50751933,  0.42213243,  0.,          0.        ],
  # [ 0.29524339,  0.73148106,  0.,          0.        ],
  # [ 0.,          0.,          0.4259793,   0.13801065],
  # [ 0.,          0.,          0.38044431,  0.34211679]])

# # b_min = 0.95

# n = 4

# state = State(
      # catcher_position="neutral",
      # picker_position="neutral",
      # on_table=[True]*4,
      # holding=None,
      # sigma_current=0.005,
      # nu_current=float('inf'),
      # user_match=[None]*4,
      # cost=0.0)

# print roshop.pyhop(state,[('insert_all',)],verbose=0,minimize_metric='cost')

