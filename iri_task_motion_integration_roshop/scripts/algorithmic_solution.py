#! /usr/bin/env python

#  - This example can be called after executing:
#    $ rosrun iri_wam_controller test_load_and_follow_joint_trajectory_in_a_csv_file.py -j poses_in_joint.csv -c poses_in_cartesian.csv
#
# This example shows how to send a QueryJointsMovement Service call,
# how to read multiple poses from a CSV file and how to ask for the
# Inverse Kinematics of the robot to achieve a joints movement

import roslib # ; roslib.load_manifest('iri_wam_controller')
import rospy
import actionlib_msgs
import actionlib
import sys
import getopt
import csv
import time
import tf

from math                        import cos, sin, pi, atan2
from geometry_msgs.msg           import Point,Quaternion,PoseStamped
from geometry_msgs.msg           import PointStamped,Vector3Stamped
from iri_common_drivers_msgs.srv import QueryJointsMovement, QueryJointsMovementRequest
from iri_common_drivers_msgs.srv import QueryInverseKinematics, QueryInverseKinematicsRequest
from iri_common_drivers_msgs.srv import QueryInverseKinematicsResponse
from control_msgs.msg            import FollowJointTrajectoryAction, FollowJointTrajectoryGoal
from trajectory_msgs.msg         import JointTrajectory, JointTrajectoryPoint
from estirabot_msgs.srv          import TransformPose, TransformPoseRequest
from tf                          import TransformListener
from tf.transformations          import quaternion_from_euler

from iri_perception_pipeline_msgs.msg import DetectPiecesGoal, DetectPiecesAction
from iri_world_interface_msgs.msg import PickOrPlacePieceGoal, PickOrPlacePieceAction
from iri_world_interface_msgs.msg import AdoptPositionGoal, AdoptPositionAction

robot = "iri_wam_picker"
base_frame = "{robot}_link_base".format(robot=robot)
kinect_frame = "/camera_rgb_optical_frame"
# kinect_frame_upwards = "/upwards_camera_rgb_tf"
joint_names = ["{robot}_joint_{joint}".format(robot=robot,joint=joint) for joint in range(1,8)]
neutral_position = [0.0,0.274,0.0,2.74,0.0,0.0,0.0]
joints_move_srv_name = "/{robot}/{robot}_controller/joints_move".format(robot=robot)
wam_ik_srv_name = "/{robot}/{robot}_gripper_ik/get_wam_ik".format(robot=robot)
action_name = "/perception_pipeline/DetectPieces"

# detectShapes_srv_name = "/shape_detector_alg_node/detectShapes"

available_shapes = ['triangle']

def detect_pieces_client(detection_type=0,available_shapes=available_shapes):
    client = actionlib.SimpleActionClient('~detect_pieces',DetectPiecesAction)
    client.wait_for_server()
    goal = DetectPiecesGoal(detection_type,available_shapes)
    client.send_goal_and_wait(goal)
    if client.get_state() != actionlib_msgs.msg.GoalStatus.SUCCEEDED:
        print "Call to action detect_pieces failed"
        assert False
    return client.get_result().detected_pieces

def pick_or_place_client(centroid,pick=True,most_likely_rotation=0):
    client = actionlib.SimpleActionClient('~pick_or_place_piece',PickOrPlacePieceAction)
    client.wait_for_server()
    goal = PickOrPlacePieceGoal(centroid,most_likely_rotation,pick,False)
    client.send_goal_and_wait(goal)
    if client.get_state() != actionlib_msgs.msg.GoalStatus.SUCCEEDED:
        print "Call to action pick_or_place_client failed"
        assert False
    return client.get_result()

def assume_position_client(robot,position):
    client = actionlib.SimpleActionClient('~{}_assume_position'.format(robot),
            AdoptPositionAction)
    client.wait_for_server()
    goal = AdoptPositionGoal(position)
    client.send_goal_and_wait(goal)
    if client.get_state() != actionlib_msgs.msg.GoalStatus.SUCCEEDED:
        print "Call to action assume_position_client failed"
        assert False
    return client.get_result()



# def get_joints_from_cartesian(cartesian_pose):
    # print ("########################################")
    # print ("Requesting Robot's Inverse Kinematics")

    # next_pose = QueryInverseKinematicsRequest()
    # next_pose.pose.header.frame_id = cartesian_pose.header.frame_id
    # next_pose.pose.pose = cartesian_pose.pose
    # try:
        # rospy.wait_for_service(wam_ik_srv_name)
        # set_object_pose_hnd = rospy.ServiceProxy(wam_ik_srv_name,
                # QueryInverseKinematics)
        # print next_pose
        # result = set_object_pose_hnd(next_pose)
        # print "SUCCESS!"
        # print "Joints: " + str(result.joints.position)
        # return result
    # except rospy.ServiceException, e:
        # print "Service call failed: %s"%e

# def get_rotation(piece):
    # best = piece.matches[0] if piece.matches else None
    # for match in piece.matches:
        # if match.similitude > best.similitude:
            # best = match
    # theta = best.optimum_angle*(pi/180) if best else 0
    # return theta
#     return Quaternion(x=0,y=0,z=sin(theta/2),w=cos(theta/2))
#     q = tf.transformations.quaternion_from_euler(0,pi,pi/2+theta) # new
#     return Quaternion(*q) # new


# def get_position(piece):
#     position = PointStamped(header=piecepoint=piece.centroid)
#     position.header.frame_id = kinect_frame
#     position.header.stamp = rospy.Time()
#     return position
#     return piece.centroid
#     centroid = piece.centroid
#     return Point(x=centroid.y,y=centroid.x,z=-centroid.z)

# def get_pose(piece):
#     pose_stmp = PoseStamped()
#     pose_stmp.pose.position = get_position(piece)
#     pose_stmp.pose.orientation = get_rotation(piece)
#     pose_stmp.header.frame_id = kinect_frame
#     pose_stmp.header.frame_id = kinect_frame_upwards
#     pose_stmp.header.stamp = rospy.Time()
#     return pose_stmp

# def recalculate_rotation(pose):
#     q = pose.pose.orientation
#     yaw = atan2(2*(q.w*q.z+q.x*q.y),1-2*(q.y**2+q.z**2))
#     pose.pose.orientation = Quaternion(x=0,y=0,z=-sin(yaw/2),w=cos(yaw/2))
#     q = tf.transformations.quaternion_from_euler(0,pi,-yaw) # new
#     pose.pose.orientation = Quaternion(*q) # new
#     return pose

# def obtain_grab_poses():
    # print ("#########################################")
    # print ("Trying to call %s" % action_name)
    # detect_pieces_client = actionlib.SimpleActionClient(action_name,
            # DetectPiecesAction)
    # print ("Waiting for server")
    # detect_pieces_client.wait_for_server()
    # goal = DetectPiecesGoal(available_shapes=['star5','trapezium','triangle'],detection_type=0)
# #     goal = DetectPiecesGoal(available_shapes=['star5','trapezium'],detection_type=0)
    # detect_pieces_client.send_goal(goal)
    # print ("Waiting for result")
    # detect_pieces_client.wait_for_result()

    # result = detect_pieces_client.get_result()
    # if result.status != 5:
        # print ("There's been a problem. Status code: {}".format(result.status))
        # return ()

    # points_kinect = [p.centroid for p in result.detected_pieces]
# #     points_kinect = map(get_position,result.detected_pieces)
    # points_robot = map(lambda point: tf_listener.transformPoint(base_frame,point),points_kinect)

    # idx = 0
    # poses_robot = []
    # for point in points_robot:
        # pose = PoseStamped(header=point.header)
        # pose.pose.position = point.point
        # theta = get_rotation(result.detected_pieces[idx])
        # q = tf.transformations.quaternion_from_euler(0,pi,theta)
        # pose.pose.orientation.x = q[0]
        # pose.pose.orientation.y = q[1]
        # pose.pose.orientation.z = q[2]
        # pose.pose.orientation.w = q[3]
        # print theta
        # poses_robot.append(pose)
        # idx += 1

    # return poses_robot

#     poses_kinect = map(get_pose,result.detected_pieces)
#
#     print "Waiting for tf between {kinect_frame} and {base_frame}".format(kinect_frame=kinect_frame,base_frame=base_frame)
#     tf_listener.waitForTransform(base_frame,kinect_frame,rospy.Time(),rospy.Duration(10))
#
#     grab_poses_0 = map(lambda pose: tf_listener.transformPose(base_frame,pose),poses_kinect)
#     grab_poses_1 = map(recalculate_rotation,grab_poses_0)
#
#     return grab_poses_1

# def obtain_show_pose():
#     print ("#########################################")
#     print ("Computing show pose")
#     pose_src = PoseStamped()
#
#     pose_src.header.frame_id = kinect_frame
#     pose_src.pose.position.x = 0.0
#     pose_src.pose.position.y = 0.0
#     pose_src.pose.position.z = 0.5
#     pose_src.pose.orientation.x = 0.0
#     pose_src.pose.orientation.y = 1.0
#     pose_src.pose.orientation.z = 0.0
#     pose_src.pose.orientation.w = 0.0
#
#     pose_dst = tf_listener.transformPose(base_frame, pose_src)
#     return pose_dst

# def assume_neutral_position():
    # goal = FollowJointTrajectoryGoal()
    # goal.trajectory.joint_names = joint_names
    # goal.trajectory.points.append(JointTrajectoryPoint())
    # goal.trajectory.points[0].time_from_start = rospy.Duration(5.0)
    # goal.trajectory.points[0].positions = [0.0, 0.274, 0.0, 2.74, 0.0, 0.0, 0.0]
    # goal.trajectory.points[0].velocities = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
    # goal.trajectory.points[0].accelerations = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
    # goal.trajectory.points[0].effort = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
    # execute_follow_joint_trajectory(goal)

# def iterate_centroids():
    # cartesian_poses = obtain_grab_poses()
    # joint_poses = map(get_joints_from_cartesian, cartesian_poses)
    # assume_position()
    # for j_pose in joint_poses:
        # assume_position(j_pose.joints.position)
    # assume_position()


# def assume_position(position=neutral_position):
    # """ Assumes the given position in joint space. By default, it assumes a
    # neutral position."""
    # joints_move = rospy.ServiceProxy(joints_move_srv_name,QueryJointsMovement)
    # joints_move(position,0.5,0.5)

################################

if __name__ == '__main__':
    rospy.init_node('move_to_centroid')
    tf_listener = TransformListener()
    tf_listener.waitForTransform(base_frame,kinect_frame,rospy.Time(),rospy.Duration(10))

    assume_position_client('catcher','neutral')
    assume_position_client('picker','neutral')

    pieces = detect_pieces_client()

    print "Detected pieces"
    print pieces

    assert len(pieces) == 1
    assert len(pieces[0].matches) == 1

    pick_or_place_client(pieces[0].centroid,True,pieces[0].matches[0].optimum_angle-3.33)

    assume_position_client('picker','show')

    # pieces = detect_pieces_client(2)

    # print "Detected pieces"
    # print pieces

    # assert len(pieces) == 1
    # assert len(pieces[0].matches) == 1

    angle2 = pieces[0].matches[0].optimum_angle

    assume_position_client('picker','neutral')
    assume_position_client('catcher','triangle')

    time.sleep(1)

    pieces = detect_pieces_client(1)

    print "Detected pieces"
    print pieces

    assert len(pieces) == 1
    assert len(pieces[0].matches) == 1

    cavity_robot = tf_listener.transformPoint(base_frame,pieces[0].centroid)

    cavity_robot.point.z += 0.02

    pick_or_place_client(cavity_robot,False,angle2-3.33)

    assume_position_client('picker','neutral')
    assume_position_client('catcher','neutral')

#     v = Vector3Stamped()
#     v.header.frame_id = kinect_frame
#     v.header.stamp = rospy.Time()
#     v.vector.x = 1
#     v_trans = tf_listener.transformVector3(base_frame,v)
#     print v_trans


    # while not rospy.is_shutdown():
        # iterate_centroids()

